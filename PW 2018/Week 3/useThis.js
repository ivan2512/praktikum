var lastClicked
var ctrzombie = 0
var youWin = true
//SUN DARI LANGIT
var ctrSunDariLangit = 0
function suns(x, y, index){
    this.life = 3000;
    this.x = x;
    this.y = y;
    this.index = index;
}
function kelasZombie(index){
	this.nyawa = 4;
	this.index = index;
}

function nubruk(A, B){
	if (A.position()==undefined || B.position()==undefined){
		return false;
	} else {
		return !(((A.position().top+A.height())< B.position().top) 
            || (A.position().top > (B.position().top+B.height())) 
            ||((A.position().left + A.width()) < B.position().left) ||
            (A.position().left > (B.position().left+B.width())))
	}
}

var timers = []
var arrSun = []
//SUN DARI SUNFLOWER
var bunga = []
var timerBunga = []
var sunBunga = 0
var timerPeas = []
var tanaman = [];
//sun on-hand
var mt
var counterTanaman = 0;
var zombies = []

function plantClicked(){
    lastClicked = this.id;
    lastClicked = lastClicked+".png";
    console.log(lastClicked);
}

function turunkan(i){
    if (parseInt($('#sun'+i).css('top'))-arrSun[i].y > 200 || parseInt($('#sun'+i).css('top'))+100 > 800){
        clearInterval(timers[i]);
        timers[i] = setTimeout(function(){
            $('#sun'+i).fadeOut();
        }, 3000);
    }
    else {
        $('#sun'+i).css('top', parseInt($('#sun'+i).css('top'))+5+"px");
    }
}

function spawnSun(i){
    var temp = '<div class="sunDariBunga" id="sunBunga'+sunBunga+'" style="top:'+(i.top-50)+'px; left:'+(i.left+10)+'px; opacity:0;"></div>';
    $('#palingBelakang').append(temp);
    bunga.push(new suns(i.top, (i.left-25), sunBunga));
    $('#sunBunga'+sunBunga).animate({top:i.top+"px", opacity:"1"})
    $('#sunBunga'+sunBunga).click(
        function(){
            $(this).fadeOut();
            mt = parseInt(document.getElementById('mthr').innerHTML);
            mt+=25;
            document.getElementById('mthr').innerHTML = mt;
        }
    )
    ++sunBunga;
}

function adaZombieDiPos(pos){
	for (let i = zombies.length - 1; i >= 0; i--) {
		if (!(zombies[i]==undefined)){
			var index = zombies[i].index
			var pasIni = $('#zombie'+index);
			if (parseInt(pos.top)==parseInt(pasIni.position().top) && parseInt(pasIni.position().left)>parseInt(pos.left)){
				return true;
			}
		}
	}
	return false;
}

var peas = []
function spawnPea(pos){
	if (adaZombieDiPos(pos)){
		var temp = '<div class="kacangIjo" style="top:'+(pos.top+10)+'px; left:'+(pos.left+50)+'px"></div>'
		$('#_1').append(temp);
	}
}

$(document).ready( function(){
    var Yatas = $('#kotak8').position();
    var Ytengah = $('#kotak16').position();
    var Ybawah = $('#kotak24').position();
    
    function genZombs(random){
    	var posZx, posZy;
    	if (random==0){
    		posZy = parseInt(Yatas.top);
    	} else if (random==1){
    		posZy = parseInt(Ytengah.top);
    	} else {
    		posZy = parseInt(Ybawah.top);
    	}
    	var zombs = "<div class='zombie' id='zombie"+ctrzombie+"'></div>"
    	$('#palingBelakang').append(zombs);
    	$('#zombie'+ctrzombie).css({'top': posZy+"px", 'right': '-25px'});
    	
    	zombies[ctrzombie] = (new kelasZombie(ctrzombie));
    	$('#zombie'+ctrzombie).click(
    		function(){
    			zombies[this.id.substr(6)].nyawa--;
    		}
    	)
    	++ctrzombie;
    }

    $('.plant').click(
        plantClicked
    );

    for (let i=1; i<4; ++i){
        $('.lane'+i).click(
            function(){
            	var berisi = !(this.id.includes(' '))
                if (lastClicked!=undefined && !(this.className.includes('disabled')) && berisi){
                    var plant = lastClicked.substr(0, lastClicked.indexOf('_')).toLowerCase();
                    
                    mt = parseInt(document.getElementById('mthr').innerHTML);
                    if (plant=='sunflower' && mt>=50){
                        mt-=50;
                        document.getElementById('mthr').innerHTML = mt;
                        $(this).css('background-image', 'url('+lastClicked+')');
	                    $(this).css('background-size', 'contain');
	                    $(this).attr('class',  $(this).attr('class')+" "+plant);
	                    this.id = this.id + ' '+ (plant+counterTanaman);
                        var pos = $(this).position();
                        timerBunga[counterTanaman] = setInterval(function() { spawnSun(pos); }, 3000)
                        tanaman.push($(this))
                        lastClicked = undefined;
                    } else if (mt>=100){
                        mt-=100;
                        document.getElementById('mthr').innerHTML = mt;
                        $(this).css('background-image', 'url('+lastClicked+')');
	                    $(this).css('background-size', 'contain');
	                    $(this).attr('class',  $(this).attr('class')+" "+plant);
	                    this.id = this.id + ' '+ (plant+counterTanaman);
                        var pos = $(this).position();
                        tanaman.push($(this))
                        timerPeas[counterTanaman]= setInterval(function() { spawnPea(pos); }, 1500)
                        lastClicked = undefined;
    				}
    				counterTanaman++;    
				}
		    }
	    );
	}
    
    var kemenangan = setTimeout( function() {
        if (youWin) alert("You win!")
        else {}
        location.reload();
    	}, 60000	);
    
    

    setInterval( function() {
        var x = Math.floor(Math.random()*650+50);
        var y = Math.floor(Math.random()*400+100);
        var matahari = '<div class="sunDariAtas" id=sun'+ctrSunDariLangit+ ' style="top:'+y+'px; left:'+x+'px;"></div>';
        arrSun.push(new suns(x, y));
        var passingIni = ctrSunDariLangit;
        timers[passingIni] = setInterval( function() {
            turunkan(passingIni)
        }, 100 ); 
        $('#palingBelakang').append(matahari);
        $('#sun'+ctrSunDariLangit).click(
            function(){
                $(this).fadeOut("slow");
                mt = parseInt(document.getElementById('mthr').innerHTML);
                mt+=25;
                document.getElementById('mthr').innerHTML = mt;
        }    )
        ++ctrSunDariLangit;
        genZombs(Math.floor(Math.random()*3));
        
    }, 5000    );

    setInterval(function(){
        if (bunga.length>0){
            for (let index = (bunga.length-1); index >= 0; index--) {
	            const element = bunga[index];
	            if (element.life<=0){
	                $('#sunBunga'+bunga[index].index).animate({left:"-1000px", opacity:"0"})
	            }
	            else {
	                element.life-=5;
	            }
	            if ($('#sunBunga'+bunga[index].index).css('opacity')==0 && element.life<=0 ){
	                $('#sunBunga'+bunga[index].index).remove();
	                bunga.splice(index, 1);
	    }    }   }    
	},4.5)

	var ini = setInterval(function(){
		if (zombies.length>0){
	    	for (let i = zombies.length - 1; i >= 0; i--) {
	    		if (zombies[i]!=undefined){
	    			var sementara = $('#zombie'+zombies[i].index)
		    		if (zombies[i].nyawa > 0){
		    			var baru = (parseFloat(sementara.css('right'))+1);
		    			sementara.css("right", baru+'px');
		    			if (sementara.position().left < 0){
		    				youWin = false;
		    				clearInterval(ini);
		    				break;
		    			}
		    			for (let i = tanaman.length - 1; i >= 0; i--) {
		    				if (nubruk(tanaman[i], sementara)){
		    					if ($(tanaman[i]).attr('class').includes('sunflower')){
		    						clearInterval(timerBunga[i]);
		    					} else if ($(tanaman[i]).attr('class').includes('peashooter')){
		    						clearInterval(timerPeas[i]);
		    					}
		    					var stringKelas = $(tanaman[i]).attr('class');
		    					stringKelas = stringKelas.substring(0, stringKelas.indexOf(' '));
		    					$(tanaman[i]).attr('class', stringKelas);
		    					stringKelas = $(tanaman[i]).attr('id');
		    					stringKelas = stringKelas.substring(0, stringKelas.indexOf(' '));
		    					$(tanaman[i]).attr('id', stringKelas);
		    					$(tanaman[i]).css('background-image', "");
		    					tanaman.splice(i,1);
		    				}
		    			}

		    		} else {
		    			if (sementara != undefined){
		    				sementara.remove();
		    			}
		    		}
	    		}
	    	}
	    	if (!youWin){
	    		clearTimeout(kemenangan);
		    	setTimeout(alert("You Lose"),20, location.reload());
		    }
	    }
	}, 50 )

	setInterval(function(){
		peas = $('.kacangIjo');
		for (var i = peas.length - 1; i >= 0; i--) {
			var x = parseInt($(peas[i]).position().left)
			x+=7; 
			if (x+50 > $('#palingBelakang').width()){
				$(peas[i]).remove();
			} else{
				x = x+'px';
				$(peas[i]).css('left', x)
				for (let j = zombies.length - 1; j >= 0; j--) {
					if (zombies[j]!=undefined){
						var index = zombies[j].index
						var pasIni = $('#zombie'+index)
						console.log(pasIni);
						if (nubruk(pasIni, $(peas[i]))){
							zombies[j].nyawa--;
							$(peas[i]).remove();
							peas.splice(i,1);
							if (zombies[j].nyawa <=0){
								$('#zombie'+index).remove();
								zombies.splice(j,1);
							}
						}
					}
				}
			}	
		}
	}, 200);

}   )
