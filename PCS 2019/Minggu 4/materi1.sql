create or replace trigger manager_trigger
before insert on manager
for each row
declare
    err exception;
    kode varchar2(6);
    angka number;
begin
    :new.nama_manager := initcap(:new.nama_manager);
    if (to_number(sysdate-:new.dob_manager)/365 < 30) then
        raise err;
    end if;
    kode := 'M'||substr(:new.nama_manager,0,2);
    select nvl(max(substr(id_manager, -3)),0)+1 into angka from manager where id_manager like kode||'%';
    kode := kode || lpad(angka,3,'0');
    :new.id_manager := upper(kode);
exception
    when err then
        raise_application_error(-20002, 'USIA TIDAK BOLEH KURANG DARI 30 TAHUN');
    when others then
        raise_application_error(sqlcode, sqlerrm);
end;
/
show err;

INSERT INTO MANAGER (NAMA_MANAGER, DOB_MANAGER, ID_NEGARA, ID_AGEN) VALUES('Lucas Moura',to_date('11-02-1978','dd-mm-yyyy'),'BEL','AGEN001');

INSERT INTO MANAGER (NAMA_MANAGER, DOB_MANAGER, ID_NEGARA, ID_AGEN) VALUES('ivan marcellino',to_date('11-02-1978','dd-mm-yyyy'),'BEL','AGEN001');