create or replace trigger club_trigger
before insert on club
for each row
declare
    err exception;
    tahun exception;
    kode varchar2(7);
    angka number;
    ada number;
begin
    select count(:new.id_liga) into ada from liga where id_liga=:new.id_liga;
    if (ada = 0) then 
        raise err;
    end if;
    if (to_number(to_char(:new.tahun_berdiri,'yyyy'))>= 2020) then raise tahun;
     end if;
    kode := 'C' || substr(:new.nama_club, -2) || substr(to_char(:new.tahun_berdiri, 'yyyy'),-1);
    select nvl(max(substr(id_club,-3)),0)+1 into angka from club where id_club like kode||'%';
    kode := kode || lpad(angka,2,'0');
    dbms_output.put_line(kode);
    :new.id_club := kode;
exception
    when err then
        raise_application_error(-20001, 'INVALID ID LIGA');
    when tahun then
        raise_application_error(-20002, 'tahun berdiri harus < 2020');
    when others then
        raise_application_error(sqlcode, sqlerrm);
end;
/
show err;

INSERT INTO CLUB (NAMA_CLUB,TAHUN_BERDIRI,ID_LIGA) VALUES('ABERDEEN',to_date('1994','YYYY'),'EN002');

INSERT INTO CLUB (NAMA_CLUB,TAHUN_BERDIRI,ID_LIGA) VALUES('ABERDEEN',to_date('2024','YYYY'),'EN002');

INSERT INTO CLUB (NAMA_CLUB,TAHUN_BERDIRI,ID_LIGA) VALUES('ABERDEEN',to_date('2024','YYYY'),'AM002');