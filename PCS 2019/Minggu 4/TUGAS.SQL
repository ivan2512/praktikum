drop trigger tugasnomor1;
drop trigger tugasnomor2;
drop trigger tugasnomor3;
drop trigger tugasnomor4;

/* TUGAS NOMOR 1a */
create or replace trigger tugasnomor1
before insert on club
for each row
declare
    kode varchar2(7);
    angka number;
    jumlahSpasi number;
    tempidliga varchar2(5);
    idStadium varchar2(6);
    namaStadium varchar2(20);
    idNegara varchar2(3);
    digitDua number;
    digitTiga number;
    panjangNama number;
begin
    :new.nama_club := initcap(:new.nama_club);
    if to_number(to_char(:new.tahun_berdiri, 'yyyy')) < 2000 then
        select id_liga into tempidliga from liga where nama_liga = 'Premier League';        
    else
        select id_liga into tempidliga from liga where nama_liga = 'EFL League One';
    end if;
    :new.id_liga := tempidliga;
    jumlahSpasi := instr(:new.nama_club, ' ');
    if (jumlahSpasi <> 0) then
        kode := substr(:new.nama_club, 0, 1) || substr(:new.nama_club, jumlahSpasi+1, 1);
    else 
        kode := upper(substr(:new.nama_club, 0, 2));
    end if;
    select nvl(max(substr(id_club, 3)), 0)+1 into angka from club where id_club like '%'||kode||'%';
    kode := kode || lpad(angka,3,'0');
    :new.id_club := kode;
end;
/
show err;

--query insert
insert into club (nama_club, tahun_berdiri) values ('nAmA kLuB', to_date('16-03-2018', 'dd-mm-yyyy'));
insert into club (nama_club, tahun_berdiri) values ('nAmAkLuB', to_date('16-03-1999', 'dd-mm-yyyy'));

--cek
select * from club where nama_club like '%ama%';
select * from club where id_club like '%NA%';

/* TUGAS NOMOR 2 */
create or replace trigger tugasnomor2
before insert on negara
for each row
declare
    err exception;
    jumlah number;
    kode varchar(3);
begin
    :new.nama_negara := upper(:new.nama_negara);
    select count(*) into jumlah from negara where nama_negara = upper(:new.nama_negara);
    if (jumlah<>0) then raise err; end if;
    kode := substr(:new.nama_negara,1,3);
    jumlah := -1;
    select count(*) into jumlah from negara where id_negara = kode;
    if (jumlah > 0) then
        kode := substr(kode, 1, 2);
        kode := kode || substr(:new.nama_negara, -2,1);
        :new.id_negara := kode;
        dbms_output.put_line(kode);
    else 
       :new.id_negara := kode;
        dbms_output.put_line(kode); 
    end if;
exception 
    when err then
        raise_application_error(-20001, 'NAMA NEGARA KEMBAR');
    when others then
        raise_application_error(sqlcode, sqlerrm);
end;
/
show err;
--test
insert into negara (nama_negara) values ('indonesia');
insert into negara (nama_negara) values ('INDONESIA');
insert into negara (nama_negara) values ('belanda');

select * from negara order by 2 asc;

/* TUGAS NOMOR 3*/
-- ERROR MUTATING TABLE KALAU TRIGGER TUGASNOMOR4 TIDAK DI DROP

drop trigger tugasnomor4;
create or replace trigger tugasnomor3 
before delete on h_transaksi 
for each row
declare 
    jumlah number; 
    kode varchar(3);
begin
    delete from d_transaksi where id_transaksi = :old.id_transaksi;
end;
/
show err;
--test
select * from h_transaksi where id_transaksi = 'TRANS001';
select * from d_transaksi where id_transaksi = 'TRANS001';
delete h_transaksi where id_transaksi = 'TRANS001';
select * from h_transaksi where id_transaksi = 'TRANS001';
select * from d_transaksi where id_transaksi = 'TRANS001';



/* TUGAS NOMOR 4 */
create or replace trigger tugasnomor4
before delete on d_transaksi
for each row
declare
begin
    update h_transaksi
        set biaya_transfer = biaya_transfer - :old.subtotal
        where id_transaksi = :old.id_transaksi;
end;
/

select * from h_transaksi where id_transaksi like '%035%';
select * from d_transaksi where id_transaksi like '%035%';

delete d_transaksi where id_transaksi like '%035%' and id_player = 'NE001';
select * from h_transaksi where id_transaksi like '%035%';
select * from d_transaksi where id_transaksi like '%035%';