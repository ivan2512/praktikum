create or replace trigger namaTrigger
before insert on PEGAWAI
for each row
declare
    temp number;
    kode varchar2(5);
    angka number;
begin
    if (:old.nip is null) then
        dbms_output.put_line('lagi insert'); end if;
    temp := instr(:new.nama_peg, ' ');
    if (temp != 0) then 
        kode := substr(:new.nama_peg,1,1)||substr(:new.nama_peg, temp+1,1);
    else 
        kode := substr(:new.nama_peg,1,2);
    end if;
    select nvl(max(substr(nip, -3)),0)+1 into angka from pegawai where nip like kode||'%';
    :new.nip := kode||lpad(angka,3,'0');
end;
/
show err;

insert into pegawai (nama_peg, menikah, jum_anak, kode_gol)values ('ivan marcellino'		,'s','0','gol02');
select * from pegawai;

create trigger trigger2 after delete on pegawai for each row
declare
    temp number;
    kode varchar2(5);
    angka number;
begin
    dbms_output.put_line('do something');
end;
/
show err;