create or replace trigger player_trigger
before insert on player
for each row
declare
err exception;
CLUBS EXCEPTION;
    kode varchar2(6);
    angka number;
    ada number;
begin
    if (length(:new.nama_player) <=4) then raise err; end if;
    SELECT COUNT(ID_CLUB) INTO ADA FROM CLUB WHERE ID_CLUB = :NEW.ID_CLUB;
    IF (ADA = 0) THEN RAISE CLUBS; END IF;
    IF (INSTR(:NEW.nama_player, ' ')<>0) THEN 
        kode := substr(:new.nama_player,0,1)||SUBSTR(:NEW.nama_player, INSTR(:NEW.nama_player, ' ')+1, 1);
    ELSE
        kode := substr(:new.nama_player,0,2);

    END IF;

    select nvl(max(substr(id_player, -3)),0) +1 into angka from player where id_player like kode||'%';

    :new.id_player := upper(kode) ||lpad(angka,3,'0');
    dbms_output.put_line(:new.id_player);
EXCEPTION
when err then
        raise_application_error(-20001, 'PANJANG NAMA HARUS LEBIH DARI 4');

When CLUBS then
        raise_application_error(-20002, '=INVALID ID CLUB');end;
/
show err;

insert into player values('','ivan marcellino',to_date('07-11-1990','DD-MM-YYYY'),'MU001','ESP','AGEN020');