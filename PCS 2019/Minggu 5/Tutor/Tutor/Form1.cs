﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tutor
{
    public partial class Form1 : Form
    {
        static OracleConnection ora = new OracleConnection(connectionString:"Data Source=orcl;User ID=N217116609; password=217116609");
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tampildata();
        }

        private void tampildata()
        {
            try
            {
                if (ora.State == ConnectionState.Open)
                {
                    ora.Close();
                }
                ora.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            OracleCommand cmd = new OracleCommand("select * from genre1", ora);
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            ora.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string perintah = "insert into genre1 values ";
            string id = textBox2.Text;
            string genre = textBox1.Text;
            perintah += "('" + id + "', '" + genre + "')";
            try
            {
                if (ora.State == ConnectionState.Open)
                {
                    ora.Close();
                }
                ora.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            OracleCommand cmd = new OracleCommand(perintah, ora);
            cmd.ExecuteNonQuery();
            ora.Close();
            tampildata();

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            

        }
        int rowIndex = -1;
        int colIndex = -1;

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            rowIndex = e.RowIndex;
            colIndex = e.ColumnIndex;
        }
    }
}
