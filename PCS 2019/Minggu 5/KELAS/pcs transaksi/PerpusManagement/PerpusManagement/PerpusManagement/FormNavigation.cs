﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PerpusManagement
{
    public partial class FormNavigation : Form
    {
        public FormNavigation()
        {
            InitializeComponent();
        }

        private void btnMasterBuku_Click(object sender, EventArgs e)
        {
            new MasterBuku().ShowDialog();
        }

        private void btnTransPinjam_Click(object sender, EventArgs e)
        {
            new TransPinjam().ShowDialog();
        }

        private void btnLapPinjam_Click(object sender, EventArgs e) {
            new FormLapPinjam().ShowDialog();
        }
    }
}
