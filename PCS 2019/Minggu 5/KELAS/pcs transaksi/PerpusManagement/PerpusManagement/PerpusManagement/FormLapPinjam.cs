﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace PerpusManagement {
    public partial class FormLapPinjam : Form {
        public FormLapPinjam() {
            InitializeComponent();
        }

        private void FormLapPinjam_Load(object sender, EventArgs e) {
            OracleCommand cmd = new OracleCommand {
                Connection = Form1.DBConnection,
                CommandText = "SELECT NRP, NAMA, NRP||' - '||NAMA as TEXT FROM MHS"
            };

            DataTable dt = new DataTable();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(dt);


            comboBox1.DataSource = dt;
            comboBox1.ValueMember = "NRP";
            comboBox1.DisplayMember = "TEXT";

        }

        private void button1_Click(object sender, EventArgs e) {
            LaporanPeminjaman lpp = new LaporanPeminjaman();
            lpp.SetParameterValue("nrp", comboBox1.SelectedValue);
            lpp.SetDatabaseLogon("n217116609", "217116609");
            crystalReportViewer1.ReportSource = lpp;
        }
    }
}
