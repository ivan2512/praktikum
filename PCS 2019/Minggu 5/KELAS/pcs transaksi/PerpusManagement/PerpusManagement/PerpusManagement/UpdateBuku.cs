﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace PerpusManagement
{
    public partial class UpdateBuku : Form
    {
        public UpdateBuku(DataRow selectedRow)
        {
            InitializeComponent();
            tbIDBuku.Text = selectedRow[0].ToString();
            tbJudul.Text = selectedRow[1].ToString();
            cbStatus.Checked = (selectedRow[2].ToString() == "1");
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand(
                "update buku set judul=:judul, status=:status where idbuku=:idbuku", 
                Form1.DBConnection);

            cmd.Parameters.Add(":judul", tbJudul.Text);
            cmd.Parameters.Add(":status", cbStatus.Checked ? "1" : "0");
            cmd.Parameters.Add(":idbuku", tbIDBuku.Text);

            int result = cmd.ExecuteNonQuery();
            if (result == 0)
            {
                MessageBox.Show("Update failed");
            } else
            {
                MessageBox.Show("Update sucessful");
                Close();
            }
        }
    }
}
