﻿namespace PerpusManagement
{
    partial class FormNavigation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMasterBuku = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnLapPinjam = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMasterBuku
            // 
            this.btnMasterBuku.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMasterBuku.Location = new System.Drawing.Point(12, 106);
            this.btnMasterBuku.Name = "btnMasterBuku";
            this.btnMasterBuku.Size = new System.Drawing.Size(260, 38);
            this.btnMasterBuku.TabIndex = 0;
            this.btnMasterBuku.Text = "Master Buku";
            this.btnMasterBuku.UseVisualStyleBackColor = true;
            this.btnMasterBuku.Click += new System.EventHandler(this.btnMasterBuku_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(260, 38);
            this.button1.TabIndex = 1;
            this.button1.Text = "Transaksi Peminjaman";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnTransPinjam_Click);
            // 
            // btnLapPinjam
            // 
            this.btnLapPinjam.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLapPinjam.Location = new System.Drawing.Point(12, 194);
            this.btnLapPinjam.Name = "btnLapPinjam";
            this.btnLapPinjam.Size = new System.Drawing.Size(260, 38);
            this.btnLapPinjam.TabIndex = 2;
            this.btnLapPinjam.Text = "Laporan Peminjaman";
            this.btnLapPinjam.UseVisualStyleBackColor = true;
            this.btnLapPinjam.Click += new System.EventHandler(this.btnLapPinjam_Click);
            // 
            // FormNavigation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 343);
            this.Controls.Add(this.btnLapPinjam);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnMasterBuku);
            this.Name = "FormNavigation";
            this.Text = "FormNavigation";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMasterBuku;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnLapPinjam;
    }
}