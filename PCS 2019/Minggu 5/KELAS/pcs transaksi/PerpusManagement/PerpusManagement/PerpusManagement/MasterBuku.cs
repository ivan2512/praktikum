﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace PerpusManagement
{
    public partial class MasterBuku : Form
    {
        DataTable dt;

        public MasterBuku()
        {
            InitializeComponent();
            RefreshDataGrid();
        }

        private void RefreshDataGrid()
        {
            OracleDataAdapter da = new OracleDataAdapter("select * from buku", Form1.DBConnection);
            dt = new DataTable();
            da.Fill(dt);

            dgvBuku.DataSource = dt;
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            new InsertBuku().ShowDialog();
            RefreshDataGrid();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dgvBuku.CurrentRow != null)
            {
                new UpdateBuku(dt.Rows[dgvBuku.CurrentRow.Index]).ShowDialog();
                RefreshDataGrid();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvBuku.CurrentRow != null)
            {
                OracleCommand cmd = new OracleCommand("delete from buku where idbuku=:idbuku",
                Form1.DBConnection);

                cmd.Parameters.Add(":idbuku", dt.Rows[dgvBuku.CurrentRow.Index][0]);
                cmd.ExecuteNonQuery();
                RefreshDataGrid();
            }
        }
    }
}
