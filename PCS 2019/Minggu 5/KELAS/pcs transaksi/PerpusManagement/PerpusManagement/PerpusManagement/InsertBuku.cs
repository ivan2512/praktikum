﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace PerpusManagement
{
    public partial class InsertBuku : Form
    {
        public InsertBuku()
        {
            InitializeComponent();

            OracleCommand cmd = new OracleCommand()
            {
                Connection = Form1.DBConnection,
                CommandText = "fAutoGenIDBuku",
                CommandType = CommandType.StoredProcedure
            };

            OracleParameter result = new OracleParameter()
            {
                Direction = ParameterDirection.ReturnValue,
                OracleDbType = OracleDbType.Varchar2,
                Size = 5
            };

            cmd.Parameters.Add(result);
            cmd.ExecuteNonQuery();

            tbIDBuku.Text = result.Value.ToString();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand(
                "insert into buku values(:idbuku,:judul,:status)", Form1.DBConnection);

            cmd.Parameters.Add(":idbuku", tbIDBuku.Text);
            cmd.Parameters.Add(":judul", tbJudul.Text);
            cmd.Parameters.Add(":status", cbStatus.Checked ? "1" : "0");

            int result = cmd.ExecuteNonQuery();
            if (result == 0)
            {
                MessageBox.Show("Insert failed");
            } else
            {
                MessageBox.Show("Insert sucessful");
                Close();
            }
        }
    }
}
