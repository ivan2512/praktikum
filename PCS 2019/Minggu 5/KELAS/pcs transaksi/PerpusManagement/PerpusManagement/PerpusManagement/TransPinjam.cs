﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace PerpusManagement
{
    public partial class TransPinjam : Form
    {
        OracleDataAdapter oda;
        DataTable tabelBuku;
        OracleCommandBuilder builder;

        public TransPinjam()
        {
            InitializeComponent();
        }

        private void TransPinjam_Load(object sender, EventArgs e)
        {
            dgvBuku.Font = label1.Font;
            using (OracleCommand cmd = new OracleCommand("SELECT * FROM MHS", Form1.DBConnection))
            {
                using (OracleDataReader dr = cmd.ExecuteReader())
                {
                    List<object> dt = new List<object>();
                    while (dr.Read())
                    {
                        dt.Add(new {
                            Nrp = dr.GetString(0),
                            Nama = dr.GetString(1),
                            Cetak = dr.GetString(1)+" - "+dr.GetString(0)
                        });
                    }
                    cbMhs.DataSource = dt;
                    cbMhs.DisplayMember = "Cetak";
                    cbMhs.ValueMember = "Nrp";
                }
            }

            using (OracleCommand cmd = new OracleCommand("SELECT * FROM BUKU where status=1", Form1.DBConnection))
            {
                using (OracleDataReader dr = cmd.ExecuteReader())
                {
                    List<object> databuku = new List<object>();
                    while (dr.Read())
                    {
                        databuku.Add(new
                        {
                            IdBuku = dr.GetString(0),
                            NamaBuku = dr.GetString(1),
                            Cetak = dr.GetString(1) + " - " + dr.GetString(0)
                        }
                        );
                    }
                    cbBuku.DataSource = databuku;
                    cbBuku.DisplayMember = "Cetak";
                    cbBuku.ValueMember = "IdBuku";
                }
            }

            oda = new OracleDataAdapter("SELECT * FROM DPINJAM WHERE 0=1", Form1.DBConnection);
            tabelBuku = new DataTable();
            oda.Fill(tabelBuku);
            builder = new OracleCommandBuilder(oda);
            
            dgvBuku.DataSource = tabelBuku;

            dgvBuku.Columns[0].Visible = false;
            dgvBuku.Columns[1].HeaderText = "Kode Buku";
            dgvBuku.Columns[2].HeaderText = "Tanggal Pinjam";
            dgvBuku.Columns[3].HeaderText = "Tanggal Kembali";
        }

        private void btnTambah_Click(object sender, EventArgs e)
        {
            DataRow dr = tabelBuku.NewRow();
            dr[1] = cbBuku.SelectedValue;
            dr[2] = dtpPinjam.Value;
            dr[3] = dtpKembali.Value;
            tabelBuku.Rows.Add(dr);
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            OracleTransaction oTrans = Form1.DBConnection.BeginTransaction();
            try
            {
                OracleParameter idPinjamBaru = new OracleParameter
                {
                    Direction = ParameterDirection.ReturnValue,
                    OracleDbType = OracleDbType.Varchar2,
                    Size = 10
                };
                OracleCommand cmd = new OracleCommand
                {
                    Connection = Form1.DBConnection,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "fAutoGenIDPinjam",
                    Parameters = { idPinjamBaru }
                };
                cmd.ExecuteNonQuery();

                OracleCommand insHpinjam = new OracleCommand
                {
                    CommandText = "INSERT INTO hpinjam values (:idpinjam, :nrp, :tanggalpinjam, :totalbuku)",
                    Connection = Form1.DBConnection
                };
                insHpinjam.Parameters.Add(":idpinjam", idPinjamBaru.Value);
                insHpinjam.Parameters.Add(":nrp", cbMhs.SelectedValue);
                insHpinjam.Parameters.Add(":tanggalpinjam", dtpPinjam.Value);
                insHpinjam.Parameters.Add(":totalbuku", dgvBuku.Rows.Count);

                insHpinjam.ExecuteNonQuery();

                foreach (DataRow item in tabelBuku.Rows)
                {
                    item[0] = idPinjamBaru.Value;
                }

                oda.Update(tabelBuku);

                oTrans.Commit();
                this.Close();
            }
            catch (Exception ex)
            {
                oTrans.Rollback();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnBatal_Click(object sender, EventArgs e)
        {
            TransPinjam_Load(null, null);
        }

        private void dgvBuku_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            tabelBuku.Rows[e.RowIndex].Delete();
        }
    }
}
