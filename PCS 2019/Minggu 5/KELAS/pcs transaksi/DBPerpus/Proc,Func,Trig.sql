create or replace function fAutoGenIDPinjam
return varchar2
is
  tID varchar2(10);
  tUrut number(4);
begin
  tID := to_char(sysdate,'DDMMYY');
  select nvl(substr(max(idpinjam),7,4),0)+1 into tUrut
  from hpinjam
  where substr(idpinjam,1,6) = tID;
  
  return tID||lpad(tUrut,4,'0');
end;
/
show err;

create or replace function fAutoGenIDBuku
return varchar2
is
    new_urutan number;
begin
    select nvl(max(substr(idbuku,2,4))+1,1) into new_urutan from buku;
    return 'B' || lpad(new_urutan,4,'0');
end;
/
show err;

create or replace procedure pAutoGenIDPinjam
(pID OUT varchar2)
is
  tID varchar2(10);
  tUrut number(4);
begin
  tID := to_char(sysdate,'DDMMYY');
  select nvl(substr(max(idpinjam),7,4),0)+1 into tUrut
  from hpinjam
  where substr(idpinjam,1,6) = tID;
  
  pID := tID||lpad(tUrut,4,'0');
end;
/
show err;

drop trigger tUpdateDPinjam;
create or replace trigger tUpdateDPinjam
before insert
on dpinjam
for each row
declare
begin
  :new.tglpinjam := sysdate;
  :new.tglkembali := sysdate+2;
  
  update buku
  set status = '0'
  where idbuku = :new.idbuku;
end;
/
show err;