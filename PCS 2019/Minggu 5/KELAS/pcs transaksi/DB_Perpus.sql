drop table mhs cascade constraint purge;
create table mhs (
  nrp varchar2(9) constraint pk_mhs primary key,
  nama varchar2(30) constraint nn0_nama_mhs not null,
  gender varchar2(1) constraint nn1_gender_mhs not null,
  constraint ch0_gender_mhs check (gender='M' or gender='m' or gender='F' or gender='f')
);

insert into mhs values ('210210182','INDRA MARYATI','F');
insert into mhs values ('217011665','JASON LEONARD','M');
insert into mhs values ('217011667','KEVIN CHRISTIAN MULIA','M');
insert into mhs values ('217011668','PATRICK SOEBIANTORO','M');
insert into mhs values ('217011669','YOHANES','M');

drop table buku cascade constraint purge;
create table buku (
  idbuku varchar2(5) constraint pk_buku primary key,
  judul varchar2(100) constraint nn0_judul_buku not null,
  status varchar2(1) constraint nn1_status_buku not null,
  constraint ch0_status_buku check (status='1' or status='0')
);

insert into buku values('B0001','Pemrograman Java','1');
insert into buku values('B0002','Pemrograman Client Server','1');
insert into buku values('B0003','Pemrograman Pascal','1');
insert into buku values('B0004','Matematika 1','1');
insert into buku values('B0006','Analisa Desain Berorientasi Objek','1');
insert into buku values('B0007','Pengantar Teknologi Informasi','0');
insert into buku values('B0008','Android Programming','1');
insert into buku values('B0009','Matematika 2','0');

drop table hpinjam cascade constraint purge;
create table hpinjam (
  idpinjam varchar2(10) constraint pk_hpinjam primary key,
  nrp varchar2(9) constraint fk0_nrp_hpinjam references mhs(nrp),
  tglpinjam date,
  totalbuku number(3)
);

drop table dpinjam cascade constraint purge;
create table dpinjam (
  idpinjam varchar2(10) constraint fk0_idpinjam_dpinjam references hpinjam(idpinjam) deferrable initially deferred,
  idbuku varchar2(5) constraint fk1_idbuku_dpinjam references buku(idbuku),
  tglpinjam date,
  tglkembali date,
  constraint pk_dpinjam primary key(idpinjam,idbuku)
);