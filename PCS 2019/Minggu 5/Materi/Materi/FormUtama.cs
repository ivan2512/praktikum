﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Materi
{
    public partial class FormUtama : Form
    {
        OracleConnection conn;
        DataTable dt;
        DataTable id_club;
        DataTable data_negara;
        DataTable data_agen;
        public FormUtama()
        {
            conn = new OracleConnection(
                connectionString: "Data Source=orcl;User ID=N217116609; password=217116609"
                );
            InitializeComponent();
        }

        private void FormUtama_Load(object sender, EventArgs e)
        {
            loadData();
            try
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            id_club = new DataTable();
            data_negara = new DataTable();
            data_agen = new DataTable();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "SELECT distinct id_club FROM PLAYER";
            cmd.Connection = conn;
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(id_club);
            idclub.Items.Clear();
            foreach (DataRow item in id_club.Rows)
            {
                idclub.Items.Add(item.ItemArray[0].ToString());
            }
            
            cmd = new OracleCommand();
            cmd.CommandText = "SELECT distinct id_negara FROM PLAYER";
            cmd.Connection = conn;
            da = new OracleDataAdapter(cmd);
            da.Fill(data_negara);
            negara.Items.Clear();
            foreach (DataRow item in data_negara.Rows)
            {
                negara.Items.Add(item.ItemArray[0].ToString());
            }

            cmd = new OracleCommand();
            cmd.CommandText = "SELECT distinct id_agen FROM PLAYER";
            cmd.Connection = conn;
            da = new OracleDataAdapter(cmd);
            da.Fill(data_agen);
            agen.Items.Clear();
            foreach (DataRow item in data_agen.Rows)
            {
                agen.Items.Add(item.ItemArray[0].ToString());
            }

            conn.Close();
        }
        private void loadData()
        {
            try
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                conn.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            dt = new DataTable();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "SELECT * FROM PLAYER";
            cmd.Connection = conn;
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(dt);
            dataGridView1.DataSource = dt;

            conn.Close();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            idclub.Text = "";
            negara.Text = "";
            agen.Text = "";
            var row = dataGridView1.Rows[e.RowIndex];
            idclub.Text = row.Cells[3].Value.ToString();
            idplayer.Text = row.Cells[0].Value.ToString();
            namaplayer.Text = row.Cells[1].Value.ToString();
            dob.Text = row.Cells[2].Value.ToString();
            negara.Text = row.Cells[4].Value.ToString();
            agen.Text = row.Cells[5].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
            string tanggal = dob.Value.Month + "/" + dob.Value.Day + "/" + dob.Value.Year;
            MessageBox.Show(tanggal);
            cmd.CommandText = "insert into player values('"+idplayer.Text+"', '"+namaplayer.Text+"', to_date('"+tanggal+"', 'MM/DD/YYYY'), '"+idclub.Text+"', '"+negara.Text+"', '"+agen.Text+"')";
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            loadData();
        }

        private void namaplayer_TextChanged(object sender, EventArgs e)
        {
            string kode = "";
            string urutan = "";
            if (namaplayer.Text.Contains(" "))
            {
                if (namaplayer.Text.Length > namaplayer.Text.IndexOf(' ')+1)
                {
                    kode = namaplayer.Text.Substring(0, 1);
                    kode += namaplayer.Text.Substring(namaplayer.Text.IndexOf(' ')+1, 1);
                    conn.Open();
                    OracleCommand cmd = new OracleCommand();
                    kode = kode.ToUpper();
                    cmd.CommandText = "select lpad(nvl(max(substr(id_player,3)),0)+1, 3, '0') from player where id_player like '%" + kode + "%'";
                    cmd.Connection = conn;
                    urutan = cmd.ExecuteScalar().ToString();
                    kode += urutan;
                    conn.Close();
                    idplayer.Text = kode;
                }
            } else
            {
                if (namaplayer.Text.Length >= 3)
                {
                    conn.Open();
                    OracleCommand cmd = new OracleCommand();
                    kode = namaplayer.Text.Substring(0, 2).ToUpper();
                    cmd.CommandText = "select lpad(nvl(max(substr(id_player,3)),0)+1, 3, '0') from player where id_player like '%" + kode + "%'";
                    cmd.Connection = conn;
                    urutan = cmd.ExecuteScalar().ToString();

                    kode = namaplayer.Text.Substring(0, 2).ToUpper() + urutan;
                    conn.Close();
                    idplayer.Text = kode;

                }
            }

        }
    }
}
