﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tugas6609 {
    public partial class Form1 : Form {
        public static OracleConnection dbConnection;

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            try {
                dbConnection = new OracleConnection(connectionString: "Data Source = orcl; User Id = n217116657; Password=217116657");
                if (dbConnection.State == ConnectionState.Open) dbConnection.Close();
                dbConnection.Open();
            }
            catch (Exception exx) {
                MessageBox.Show(exx.Message);
            }
        }

        private void PERSONNELToolStripMenuItem_Click(object sender, EventArgs e) {
            cLUBToolStripMenuItem.Enabled = true;
            pERSONNELToolStripMenuItem.Enabled = false;
            foreach (var item in MdiChildren) {
                item.Close();
            }
            Personnel p = new Personnel {
                MdiParent = this
            };
            p.Show();
        }

        private void CLUBToolStripMenuItem_Click(object sender, EventArgs e) {
            cLUBToolStripMenuItem.Enabled = false;
            pERSONNELToolStripMenuItem.Enabled = true;
            foreach (var item in MdiChildren) {
                item.Close();
            }
            Club c = new Club {
                MdiParent = this,
            };
            c.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            if (dbConnection.State==ConnectionState.Open) dbConnection.Close();
        }
    }
}
