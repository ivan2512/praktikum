﻿namespace Tugas6609 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pERSONNELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLUBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pERSONNELToolStripMenuItem,
            this.cLUBToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pERSONNELToolStripMenuItem
            // 
            this.pERSONNELToolStripMenuItem.Name = "pERSONNELToolStripMenuItem";
            this.pERSONNELToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.pERSONNELToolStripMenuItem.Text = "PERSONNEL";
            this.pERSONNELToolStripMenuItem.Click += new System.EventHandler(this.PERSONNELToolStripMenuItem_Click);
            // 
            // cLUBToolStripMenuItem
            // 
            this.cLUBToolStripMenuItem.Name = "cLUBToolStripMenuItem";
            this.cLUBToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.cLUBToolStripMenuItem.Text = "CLUB";
            this.cLUBToolStripMenuItem.Click += new System.EventHandler(this.CLUBToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pERSONNELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cLUBToolStripMenuItem;
    }
}

