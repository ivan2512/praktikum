drop view ligainggris;

create or replace view ligainggris as
select l.id_liga, l.nama_liga
from liga l, negara n 
where l.id_negara = n.id_negara 
and n.nama_negara = 'ENGLAND';

select l.nama_liga, count(c.id_liga) as "JUMLAH CLUB"
from ligainggris l, club c
where c.id_liga = l.id_liga
having count(c.id_liga) > 0
group by l.nama_liga
order by 1 asc;