drop view player_negara;

create or replace view player_negara as
select n.nama_negara, n.id_negara, count(p.id_negara) as banyakpemain
from negara n, player p 
where n.id_negara = p.id_negara
group by n.nama_negara, n.id_negara
having count(p.id_negara) > 2;

select 
    case when MOD(count(m.id_negara),2) = 1 then rpad(lpad(count(m.id_negara),10),20) else ' ' end
 as JUMLAH_MANAGER, pn.nama_negara
from manager m, player_negara pn 
where m.id_negara = pn.id_negara
group by pn.nama_negara
order by count(m.id_negara), pn.nama_negara;
