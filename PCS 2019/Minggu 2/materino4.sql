drop view agen_player;

create or replace view agen_player as
select a.nama_agen, a.id_agen, p.id_player, (sysdate - p.dob_player)/365 as UMUR
from agen a, player p 
where a.id_agen = p.id_agen;

select distinct ap.nama_agen, count(m.id_agen) as "Jumlah Client Manager"
from agen_player ap, manager m  
where ap.UMUR >= 31 and m.id_agen = ap.id_agen
having count(m.id_agen) > 0
group by ap.nama_agen
order by 2 desc, substr(ap.nama_agen,4,1);