drop view stadium_besar;

create or replace view stadium_besar as
select s.nama_stadium, s.id_club 
from stadium s 
where s.kapasitas > 50000;

select s.nama_stadium, m.nama_manager
from stadium_besar s, manager m, club_manager cm 
where s.id_club = cm.id_club and
m.id_manager = cm.id_manager
order by m.nama_manager, substr(s.nama_stadium, 4,1);