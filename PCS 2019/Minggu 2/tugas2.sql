drop view player_usia;
drop view club_negara;

create or replace view player_usia as
select p.id_negara, p.nama_player, trunc((sysdate-p.dob_player)/365) as usia
from player p;

create or replace view club_negara as
select count(c.id_club) as JUMLAH_KLUB, l.id_negara
from club c, liga l 
where c.id_liga = l.id_liga
having count(c.id_club)>4
group by l.id_negara;

select 
    case when length(p.nama_player) < 11 then rpad(p.nama_player, 20) else lpad(p.nama_player, 20) end
 as "Nama Player", p.usia as "Usia Player"
from player_usia p, club_negara cn 
where cn.id_negara = p.id_negara
order by cn.id_negara desc, p.usia;