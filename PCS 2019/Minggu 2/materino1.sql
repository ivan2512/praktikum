drop view diatas2015;

create or replace view diatas2015 as
select distinct c.id_club
from club c, h_transaksi ht 
where c.id_club = ht.club_asal
and ht.tgl_transfer >= to_date('01/01/2015', 'dd/mm/yyyy');

select m.nama_manager
from club_manager cm, diatas2015 view1, manager m
where cm.id_club = view1.id_club and
m.id_manager = cm.id_manager
order by substr(m.nama_manager,3,1) desc;