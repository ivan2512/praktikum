drop view diatas2015;
drop view ligainggris;
drop view stadium_besar;
drop view agen_player;
drop view player_usia;
drop view club_negara;
drop view player_negara;
drop synonym syn1;
drop synonym syn2;
drop synonym syn3;
drop synonym syn4;
drop synonym syn5;
drop synonym syn6;
drop synonym syn7;

--VIEW CREATION
create or replace view diatas2015 as
select distinct c.id_club
from club c, h_transaksi ht 
where c.id_club = ht.club_asal
and ht.tgl_transfer >= to_date('01/01/2015', 'dd/mm/yyyy');

create or replace view ligainggris as
select l.id_liga, l.nama_liga
from liga l, negara n 
where l.id_negara = n.id_negara 
and n.nama_negara = 'ENGLAND';

create or replace view stadium_besar as
select s.nama_stadium, s.id_club 
from stadium s 
where s.kapasitas > 50000;

create or replace view agen_player as
select a.nama_agen, a.id_agen, p.id_player, (sysdate - p.dob_player)/365 as UMUR
from agen a, player p 
where a.id_agen = p.id_agen;

create or replace view player_usia as
select p.id_negara, p.nama_player, trunc((sysdate-p.dob_player)/365) as usia
from player p;

create or replace view club_negara as
select count(c.id_club) as JUMLAH_KLUB, l.id_negara
from club c, liga l 
where c.id_liga = l.id_liga
having count(c.id_club)>4
group by l.id_negara;

create or replace view player_negara as
select n.nama_negara, n.id_negara, count(p.id_negara) as banyakpemain
from negara n, player p 
where n.id_negara = p.id_negara
group by n.nama_negara, n.id_negara
having count(p.id_negara) > 2;


--TUGAS 1 [MATERI]
    --1
    select m.nama_manager
    from club_manager cm, diatas2015 view1, manager m
    where cm.id_club = view1.id_club and
    m.id_manager = cm.id_manager
    order by substr(m.nama_manager,3,1) desc;

    --2
    select l.nama_liga, count(c.id_liga) as "JUMLAH CLUB"
    from ligainggris l, club c
    where c.id_liga = l.id_liga
    having count(c.id_liga) > 0
    group by l.nama_liga
    order by 1 asc;

    --3
    select s.nama_stadium, m.nama_manager
    from stadium_besar s, manager m, club_manager cm 
    where s.id_club = cm.id_club and
    m.id_manager = cm.id_manager
    order by m.nama_manager, substr(s.nama_stadium, 4,1);

    --4
    select distinct ap.nama_agen, count(m.id_agen) as "Jumlah Client Manager"
    from agen_player ap, manager m  
    where ap.UMUR >= 31 and m.id_agen = ap.id_agen
    having count(m.id_agen) > 0
    group by ap.nama_agen
    order by 2 desc, substr(ap.nama_agen,4,1);

--TUGAS 2
select 
    case when length(p.nama_player) < 11 then rpad(p.nama_player, 20) else lpad(p.nama_player, 20) end
 as "Nama Player", p.usia as "Usia Player"
from player_usia p, club_negara cn 
where cn.id_negara = p.id_negara
order by cn.id_negara desc, p.usia;

--TUGAS 3
select 
    case when MOD(count(m.id_negara),2) = 1 then rpad(lpad(count(m.id_negara),10),20) else ' ' end
 as JUMLAH_MANAGER, pn.nama_negara
from manager m, player_negara pn 
where m.id_negara = pn.id_negara
group by pn.nama_negara
order by count(m.id_negara), pn.nama_negara;

--TUGAS 4
create synonym syn1 for diatas2015;
create synonym syn2 for ligainggris;
create synonym syn3 for stadium_besar;
create synonym syn4 for agen_player;
create synonym syn5 for player_usia;
create synonym syn6 for club_negara;
create synonym syn7 for player_negara;
