
--Menampilkan Nama Pegawai dan No nota
select p.nama_peg as "Nama Pegawai", s.nonota as "Nota"
from pegawai p, (select h.nonota, h.nip 
				 from htrans h 
				 where to_number(to_char(h.tgl_sewa,'YYYY')) = 2017) s
where s.nip = p.nip;

--
create view view1 as
select h.nonota, h.nip 
from htrans h 
where to_number(to_char(h.tgl_sewa,'YYYY')) = 2017;

select p.nama_peg, v.nonota
from pegawai p, synv1 v
where v.nip = p.nip;

create synonym synv1 for view1;