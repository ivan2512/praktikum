﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tugas6609 {
    public partial class Club : Form {
        DataTable dataClub, dataLiga;
        bool updateMode;
        int rowIndex;

        public Club() {
            updateMode = false;
            InitializeComponent();
            rowIndex = -1;
        }

        private void Club_Load(object sender, EventArgs e) {
            label6.Text = "";
            Ganti();
            dataLiga = new DataTable();
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = "SELECT id_liga, id_liga ||' - '|| nama_liga as cetak from liga"
            };
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(dataLiga);
            leagues.DataSource = dataLiga;
            leagues.ValueMember = "ID_LIGA";
            leagues.DisplayMember = "cetak";
        }

        private void TabelClub_CellClick(object sender, DataGridViewCellEventArgs e) {
            updateMode = true;
            rowIndex = e.RowIndex;
            label6.Text = "update mode on";
            if (rowIndex > -1) {
                namaClub.Text = tabel["NAMA_CLUB", rowIndex].Value.ToString();
                idClub.Text = tabel["ID_CLUB", rowIndex].Value.ToString();
                leagues.SelectedValue = tabel["ID_LIGA", rowIndex].Value;
                tahunBerdiri.Text = tabel["TAHUN_BERDIRI", rowIndex].Value.ToString();
            }
        }

        private void Ganti() {
            dataClub = new DataTable();
            OracleCommand cmd = new OracleCommand {
                CommandText = "SELECT id_club, nama_club, tahun_berdiri, id_liga FROM CLUB",
                Connection = FormDasar.dbConnection
            };
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(dataClub);
            tabel.DataSource = dataClub;
        }

        private void NamaClub_TextChanged(object sender, EventArgs e) {
            if (!updateMode) {
                try {
                    string prefix = namaClub.Text.Substring(0, 2);
                    if (namaClub.Text.Contains(" ")) {
                        prefix = namaClub.Text[0].ToString();
                        for (int i = namaClub.Text.Length - 1; i >= 0; i--) {
                            if (namaClub.Text[i] == ' ') {
                                prefix += namaClub.Text[i + 1].ToString();
                                i = -1;
                            }
                        }
                    }
                    prefix = prefix.ToUpper();
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "SELECT lpad(nvl(max(substr(id_club, 3)),0)+1,3,'0') from club where id_club like '%" + prefix+"%'"
                    };
                    if (cmd.Connection.State == ConnectionState.Closed) 
                        cmd.Connection.Open();
                    prefix += cmd.ExecuteScalar().ToString();
                    idClub.Text = prefix;
                }
                catch (IndexOutOfRangeException) { /*silent*/ }
                catch (ArgumentOutOfRangeException) { }
                catch (Exception exc) {
                    MessageBox.Show("hayo van kok error\nMessage:"+exc.Message);
                }
            }
        }

        private void Insert_click(object sender, EventArgs e) {
            string str = "TO_DATE('" + (tahunBerdiri.Value.Year)+ "','YYYY')";
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = "INSERT INTO CLUB VALUES (:idclub, :namaclub, "+str+", :liga)"
            };
            cmd.Parameters.Add(":idclub", idClub.Text);
            cmd.Parameters.Add(":namaclub", namaClub.Text);
            cmd.Parameters.Add(":liga", leagues.SelectedValue);
            if (cmd.Connection.State == ConnectionState.Closed) 
                cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            Ganti();
        }

        private void Update_Click(object sender, EventArgs e) {
            string str = "TO_DATE('" + (tahunBerdiri.Value.Year) + "','YYYY')";
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = "UPDATE CLUB SET NAMA_CLUB = :namaclub, TAHUN_BERDIRI = " + str + ", ID_LIGA = :liga where ID_CLUB = :idclub"
            };
            cmd.Parameters.Add(":namaclub", namaClub.Text);
            cmd.Parameters.Add(":liga", leagues.SelectedValue);
            cmd.Parameters.Add(":idclub", idClub.Text);
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            Ganti();
        }

        private void Search(object sender, EventArgs e) {
            dataClub = new DataTable();
            string katakunci = "ID_CLUB";
            if (keyword.SelectedItem.ToString().Contains("ama"))
                katakunci = "NAMA_CLUB";
            OracleCommand cmd = new OracleCommand {
                CommandText = "SELECT id_club, nama_club, tahun_berdiri, id_liga FROM CLUB WHERE "+katakunci+" LIKE '%"+textBox3.Text+"%'",
                Connection = FormDasar.dbConnection
            };
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(dataClub);
            tabel.DataSource = dataClub;
        }

        private void Delete_Click(object sender, EventArgs e) {
            string chosenIDCLUB = idClub.Text;
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection
            };

            if (cmd.Connection.State == ConnectionState.Closed) 
                cmd.Connection.Open();
            cmd.CommandText = "DELETE FROM STADIUM WHERE ID_CLUB = '" + chosenIDCLUB + "'";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "DELETE FROM CLUB_MANAGER WHERE ID_CLUB = '" + chosenIDCLUB + "'";
            cmd.ExecuteNonQuery();

            //select semua transaksi dengan id tsb dulu
            OracleCommand selectHTRANS = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = "SELECT DISTINCT ID_TRANSAKSI FROM H_TRANSAKSI WHERE CLUB_ASAL = :idclub OR CLUB_BARU = :idclub"
            };
            selectHTRANS.Parameters.Add(":idclub", chosenIDCLUB);

            OracleDataReader dr = selectHTRANS.ExecuteReader();
            while (dr.Read()) {
                cmd.CommandText = "DELETE FROM D_TRANSAKSI WHERE ID_TRANSAKSI = '" + dr.GetString(0) + "'";
                cmd.ExecuteNonQuery();
            }
            cmd.CommandText = "DELETE FROM H_TRANSAKSI WHERE CLUB_ASAL = '" + chosenIDCLUB + "' or CLUB_BARU = '"+chosenIDCLUB+"'";
            cmd.ExecuteNonQuery();

            //select semua player yang mau didelete
            selectHTRANS.CommandText = "SELECT ID_PLAYER FROM PLAYER WHERE ID_CLUB=:idclub";
            dr = selectHTRANS.ExecuteReader();
            while (dr.Read()) {
                cmd.CommandText = "DELETE FROM D_TRANSAKSI WHERE ID_PLAYER = '" + dr.GetString(0) + "'";
                cmd.ExecuteNonQuery();
            }
            
            cmd.CommandText = "DELETE FROM PLAYER WHERE ID_CLUB = '" + chosenIDCLUB + "'";
            cmd.ExecuteNonQuery();
            
            cmd.CommandText = "DELETE FROM CLUB WHERE ID_CLUB = '" + chosenIDCLUB + "'";
            cmd.ExecuteNonQuery();
            Ganti();

            cmd.Connection.Close();
        }
    }
}