﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tugas6609 {
    public partial class Personnel : Form {
        DataTable dataAgen, dataNegara, dataClub;
        int rowIndex;
        bool updateMode = false;

        public Personnel() {
            dataAgen = new DataTable();
            dataNegara = new DataTable();
            dataClub = new DataTable();
            InitializeComponent();
            rowIndex = -1;
        }

        private void Personnel_Load(object sender, EventArgs e) {
            OracleConnection conn = FormDasar.dbConnection;
            OracleCommand cmd = new OracleCommand {
                Connection = conn,
                CommandText = "SELECT id_agen, nama_agen from agen"
            };
            if (conn.State == ConnectionState.Closed) conn.Open();
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(dataAgen);
            agen.DataSource = dataAgen;
            agen.DisplayMember = "NAMA_AGEN";
            agen.ValueMember = "ID_AGEN";

            cmd.CommandText = "SELECT id_negara, nama_negara from negara";
            oda = new OracleDataAdapter(cmd);
            oda.Fill(dataNegara);
            negara.DataSource = dataNegara;
            negara.DisplayMember = "NAMA_NEGARA";
            negara.ValueMember = "ID_NEGARA";

            cmd.CommandText = "SELECT id_club, nama_club from club";
            oda = new OracleDataAdapter(cmd);
            oda.Fill(dataClub);
            club.DataSource = dataClub;
            club.DisplayMember = "NAMA_CLUB";
            club.ValueMember = "ID_CLUB";
        }

        private void Ganti(object sender, EventArgs e) {
            Clearfields();
            DataTable dt = new DataTable();
            string namatabel = (isPlayer.Checked) ? (isPlayer.Text) : (radioButton2.Checked ? radioButton2.Text : "");
            OracleCommand cmd = new OracleCommand("SELECT * FROM " + namatabel, FormDasar.dbConnection);
            OracleDataAdapter oda = new OracleDataAdapter(cmd);
            oda.Fill(dt);
            tabel.DataSource = dt;
        }

        private void Clearfields() {
            label9.Text = "";
            updateMode = false;
            namaPersonel.Text = "";
            idPersonel.Text = "";
            negara.Text = "";
            club.Text = "";
            agen.Text = "";
            dob.Text = "";
        }

        private void DIKLIK(object sender, DataGridViewCellEventArgs e) {
            updateMode = true;
            label9.Text = "UPDATE MODE ON";
            rowIndex= e.RowIndex;
            if (rowIndex >= 0) {
                if (isPlayer.Checked) {
                    namaPersonel.Text = tabel["NAMA_PLAYER", rowIndex].Value.ToString();
                    dob.Text = tabel["DOB_PLAYER", rowIndex].Value.ToString();
                    club.SelectedValue = tabel["ID_CLUB", rowIndex].Value;
                    negara.SelectedValue = tabel["ID_NEGARA", rowIndex].Value;
                    agen.SelectedValue = tabel["ID_AGEN", rowIndex].Value;
                    idPersonel.Text = tabel["ID_PLAYER", rowIndex].Value.ToString();
                } 
                else {
                    namaPersonel.Text = tabel["NAMA_MANAGER", rowIndex].Value.ToString();
                    dob.Text = tabel["DOB_MANAGER", rowIndex].Value.ToString();
                    negara.SelectedValue = tabel["ID_NEGARA", rowIndex].Value;
                    agen.SelectedValue = tabel["ID_AGEN", rowIndex].Value;

                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "SELECT nvl(id_club,'') FROM CLUB_MANAGER WHERE ID_MANAGER = '" + tabel["ID_MANAGER", rowIndex].Value.ToString() + "'"
                    };
                    if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
                    try {
                        club.SelectedValue = cmd.ExecuteScalar().ToString();
                    }
                    catch (Exception exp) {
                        MessageBox.Show("Manager tidak memiliki club\nError message:"+exp.Message);
                        club.Text = "";
                    }
                    cmd.Connection.Close();
                    idPersonel.Text = tabel["ID_MANAGER", rowIndex].Value.ToString();
                }
            }
        }

        private void CARI(object sender, EventArgs e) {
            if (isPlayer.Checked) {
                string kondisi;
                if (keyword.SelectedItem.ToString().Contains("Nama"))
                    kondisi = "NAMA_PLAYER";
                else
                    kondisi = "ID_PLAYER";
                DataTable hasilSearch = new DataTable();
                OracleCommand cmd = new OracleCommand {
                    Connection = FormDasar.dbConnection,
                    CommandText = "SELECT * FROM PLAYER WHERE " + kondisi + " LIKE '%" + textBox3.Text+"%'"
                };
                try {
                    if (cmd.Connection.State == ConnectionState.Closed)
                        cmd.Connection.Open();
                    OracleDataAdapter oda = new OracleDataAdapter(cmd);
                    oda.Fill(hasilSearch);
                    tabel.DataSource = hasilSearch;
                    cmd.Connection.Close();
                }
                catch (Exception exp) {
                    MessageBox.Show(exp.Message); }
            }
            else {
                string kondisi;
                if (keyword.SelectedItem.ToString().Contains("Nama"))
                    kondisi = "NAMA_MANAGER";
                else
                    kondisi = "ID_MANAGER";
                DataTable hasilSearch = new DataTable();
                OracleCommand cmd = new OracleCommand {
                    Connection = FormDasar.dbConnection,
                    CommandText = "SELECT * FROM MANAGER WHERE " + kondisi + " LIKE '%" + textBox3.Text + "%'"
                };
                try {
                    if (cmd.Connection.State == ConnectionState.Closed)
                        cmd.Connection.Open();
                    OracleDataAdapter oda = new OracleDataAdapter(cmd);
                    oda.Fill(hasilSearch);
                    tabel.DataSource = hasilSearch;
                    cmd.Connection.Close();
                }
                catch (Exception exp) {
                    MessageBox.Show(exp.Message); }
            }
        }

        private void UPDATE(object sender, EventArgs e) {
            if (rowIndex >= 0) {
                if (isPlayer.Checked) {
                    string tanggal = dob.Value.Month + "/" + dob.Value.Day + "/" + dob.Value.Year;
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "update player set nama_player = initcap('" + namaPersonel.Text + "'), dob_player=to_date('" + tanggal + "', 'MM/DD/YYYY'), id_club=upper('" + club.SelectedValue.ToString() + "'), id_negara=upper('" + negara.SelectedValue.ToString() + "'), id_agen=upper('" + agen.SelectedValue.ToString() + "') where id_player = '" + tabel["ID_PLAYER", rowIndex].Value.ToString() + "'"
                    };
                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("UPDATE PLAYER GAGAL\nError: "+exp.Message); }
                } 
                else {
                    string tanggal = dob.Value.Month + "/" + dob.Value.Day + "/" + dob.Value.Year;
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "update manager set nama_manager = initcap('" + namaPersonel.Text + "'), dob_manager=to_date('" + tanggal + "', 'MM/DD/YYYY'), id_negara=upper('" + negara.SelectedValue.ToString() + "'), id_agen=upper('" + agen.SelectedValue.ToString() + "') where id_manager = '" + tabel["ID_MANAGER", rowIndex].Value.ToString() + "'"
                    };
                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("UPDATE MANAGER GAGAL\nError: " + exp.Message); }

                    try {
                        cmd.CommandText = "INSERT INTO CLUB_MANAGER VALUES ('" + club.SelectedValue.ToString() + "', '" + tabel["ID_MANAGER", rowIndex].Value.ToString() + "')";
                        Execnonquery(cmd);
                    }
                    catch (Exception exp) {
                        cmd.CommandText = "UPDATE CLUB_MANAGER SET ID_CLUB = '" + club.SelectedValue.ToString() + "' WHERE ID_MANAGER = '" + tabel["ID_MANAGER", rowIndex].Value.ToString() + "'";
                        try {
                            Execnonquery(cmd);
                        }
                        catch (Exception exc) {
                            MessageBox.Show("UPDATE CLUB_MANAGER GAGAL\nError 1: " + exp.Message+"\nError2: "+exc.Message);
                        }
                    }
                }
                Ganti(null, e);
            }
        }

        private void HAPUS(object sender, EventArgs e) {
            if (rowIndex > -1) {
                if (isPlayer.Checked) {
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "DELETE FROM D_TRANSAKSI WHERE ID_PLAYER = '"+idPersonel.Text+"'"
                    };

                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("DELETE DTRANS GAGAL\nError: " + exp.Message); }

                    cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "DELETE FROM PLAYER WHERE ID_PLAYER = '" + idPersonel.Text + "'"
                    };
                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("DELETE GAGAL\nError: " + exp.Message); }
                } 
                else {
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "DELETE FROM CLUB_MANAGER WHERE ID_MANAGER = '" + idPersonel.Text + "'"
                    };
                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("DELETE CLUB_MANAGER GAGAL\nError: " + exp.Message); }
                    cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection,
                        CommandText = "DELETE FROM MANAGER WHERE ID_MANAGER = '" + idPersonel.Text + "'"
                    };
                    try {
                        Execnonquery(cmd); }
                    catch (Exception exp) {
                        MessageBox.Show("DELETE MANAGER GAGAL\nError: " + exp.Message); }
                }
                Ganti(null, e);
            }
        }

        private void NamaChanged(object sender, EventArgs e) {
            OracleConnection conn = FormDasar.dbConnection;
            if (!updateMode) {
                if (isPlayer.Checked) {
                    string kode = "";
                    string urutan = "";
                    if (namaPersonel.Text.Contains(" ")) {
                        if (namaPersonel.Text.Length > namaPersonel.Text.IndexOf(' ') + 1) {
                            kode = namaPersonel.Text.Substring(0, 1);
                            kode += namaPersonel.Text.Substring(namaPersonel.Text.IndexOf(' ') + 1, 1);
                            if (conn.State == ConnectionState.Closed)
                                conn.Open();
                            OracleCommand cmd = new OracleCommand();
                            kode = kode.ToUpper();
                            cmd.CommandText = "select lpad(nvl(max(substr(id_player,3)),0)+1, 3, '0') from player where id_player like '%" + kode + "%'";
                            cmd.Connection = conn;
                            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
                            urutan = cmd.ExecuteScalar().ToString();
                            kode += urutan;
                            conn.Close();
                            idPersonel.Text = kode;
                        }
                    } else {
                        if (namaPersonel.Text.Length >= 3) {
                            if (conn.State == ConnectionState.Closed)
                                conn.Open();
                            OracleCommand cmd = new OracleCommand();
                            kode = namaPersonel.Text.Substring(0, 2).ToUpper();
                            cmd.CommandText = "select lpad(nvl(max(substr(id_player,3)),0)+1, 3, '0') from player where id_player like '%" + kode + "%'";
                            cmd.Connection = conn;
                            urutan = cmd.ExecuteScalar().ToString();

                            kode = namaPersonel.Text.Substring(0, 2).ToUpper() + urutan;
                            conn.Close();
                            idPersonel.Text = kode;
                            conn.Close();
                        }
                    }
                } else {
                    string kode = "MAN";
                    OracleCommand cmd = new OracleCommand {
                        CommandText = "SELECT lpad(nvl(max(substr(id_manager, 4)),0)+1,3,'0') from manager where id_manager like '%MAN%'",
                        Connection = conn
                    };
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    kode += cmd.ExecuteScalar().ToString();
                    idPersonel.Text = kode;
                    conn.Close();
                }
            }
        }

        private void INSERT(object sender, EventArgs e) {
            if (updateMode) Clearfields();
            else {
                if (isPlayer.Checked) {
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection
                    };
                    string tanggal = dob.Value.Month + "/" + dob.Value.Day + "/" + dob.Value.Year;
                    cmd.CommandText = "insert into player values(upper('" + idPersonel.Text + "'), initcap('" + namaPersonel.Text + "'), to_date('" + tanggal + "', 'MM/DD/YYYY'), '" + club.SelectedValue.ToString() + "', '" + negara.SelectedValue.ToString() + "', '" + agen.SelectedValue.ToString() + "')";
                    Execnonquery(cmd);
                } 
                else {
                    OracleCommand cmd = new OracleCommand {
                        Connection = FormDasar.dbConnection
                    };
                    string tanggal = dob.Value.Month + "/" + dob.Value.Day + "/" + dob.Value.Year;
                    cmd.CommandText = "insert into manager values('" + idPersonel.Text + "', initcap('" + namaPersonel.Text + "'), to_date('" + tanggal + "', 'MM/DD/YYYY'), '" + negara.SelectedValue.ToString() + "', '" + agen.SelectedValue.ToString() + "')";
                    Execnonquery(cmd);
                    cmd.CommandText = "insert into club_manager values ('" + club.SelectedValue.ToString() + "', '" + idPersonel.Text + "' )";
                    Execnonquery(cmd);
                }
                Ganti(null, e);
            }
        }

        private static void Execnonquery(OracleCommand cmd) {
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Connection.Close();
        }
    }
}
