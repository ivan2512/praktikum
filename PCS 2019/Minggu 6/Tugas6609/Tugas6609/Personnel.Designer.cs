﻿namespace Tugas6609 {
    partial class Personnel {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabel = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.agen = new System.Windows.Forms.ComboBox();
            this.negara = new System.Windows.Forms.ComboBox();
            this.club = new System.Windows.Forms.ComboBox();
            this.dob = new System.Windows.Forms.DateTimePicker();
            this.namaPersonel = new System.Windows.Forms.TextBox();
            this.idPersonel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.isPlayer = new System.Windows.Forms.RadioButton();
            this.SEARCH = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.keyword = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tabel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SEARCH.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabel
            // 
            this.tabel.AllowUserToAddRows = false;
            this.tabel.AllowUserToDeleteRows = false;
            this.tabel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tabel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabel.Location = new System.Drawing.Point(372, 31);
            this.tabel.Name = "tabel";
            this.tabel.ReadOnly = true;
            this.tabel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.tabel.Size = new System.Drawing.Size(800, 600);
            this.tabel.TabIndex = 0;
            this.tabel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DIKLIK);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.agen);
            this.groupBox1.Controls.Add(this.negara);
            this.groupBox1.Controls.Add(this.club);
            this.groupBox1.Controls.Add(this.dob);
            this.groupBox1.Controls.Add(this.namaPersonel);
            this.groupBox1.Controls.Add(this.idPersonel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.isPlayer);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(347, 379);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personnel";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(212, 313);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "DELETE";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.HAPUS);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(131, 313);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "UPDATE";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.UPDATE);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(50, 313);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "INSERT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.INSERT);
            // 
            // agen
            // 
            this.agen.FormattingEnabled = true;
            this.agen.Location = new System.Drawing.Point(6, 254);
            this.agen.Name = "agen";
            this.agen.Size = new System.Drawing.Size(200, 21);
            this.agen.TabIndex = 15;
            // 
            // negara
            // 
            this.negara.FormattingEnabled = true;
            this.negara.Location = new System.Drawing.Point(6, 214);
            this.negara.Name = "negara";
            this.negara.Size = new System.Drawing.Size(200, 21);
            this.negara.TabIndex = 14;
            // 
            // club
            // 
            this.club.FormattingEnabled = true;
            this.club.Location = new System.Drawing.Point(6, 174);
            this.club.Name = "club";
            this.club.Size = new System.Drawing.Size(200, 21);
            this.club.TabIndex = 13;
            // 
            // dob
            // 
            this.dob.Location = new System.Drawing.Point(6, 135);
            this.dob.Name = "dob";
            this.dob.Size = new System.Drawing.Size(200, 20);
            this.dob.TabIndex = 12;
            // 
            // namaPersonel
            // 
            this.namaPersonel.Location = new System.Drawing.Point(6, 96);
            this.namaPersonel.Name = "namaPersonel";
            this.namaPersonel.Size = new System.Drawing.Size(200, 20);
            this.namaPersonel.TabIndex = 11;
            this.namaPersonel.TextChanged += new System.EventHandler(this.NamaChanged);
            // 
            // idPersonel
            // 
            this.idPersonel.Location = new System.Drawing.Point(6, 57);
            this.idPersonel.Name = "idPersonel";
            this.idPersonel.ReadOnly = true;
            this.idPersonel.Size = new System.Drawing.Size(200, 20);
            this.idPersonel.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nama Agen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Kewarganegaraan";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Club";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "DOB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nama";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(276, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(67, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Manager";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.Ganti);
            // 
            // isPlayer
            // 
            this.isPlayer.AutoSize = true;
            this.isPlayer.Location = new System.Drawing.Point(194, 19);
            this.isPlayer.Name = "isPlayer";
            this.isPlayer.Size = new System.Drawing.Size(54, 17);
            this.isPlayer.TabIndex = 2;
            this.isPlayer.TabStop = true;
            this.isPlayer.Text = "Player";
            this.isPlayer.UseVisualStyleBackColor = true;
            this.isPlayer.CheckedChanged += new System.EventHandler(this.Ganti);
            // 
            // SEARCH
            // 
            this.SEARCH.Controls.Add(this.button4);
            this.SEARCH.Controls.Add(this.textBox3);
            this.SEARCH.Controls.Add(this.keyword);
            this.SEARCH.Controls.Add(this.label8);
            this.SEARCH.Controls.Add(this.label7);
            this.SEARCH.Location = new System.Drawing.Point(12, 397);
            this.SEARCH.Name = "SEARCH";
            this.SEARCH.Size = new System.Drawing.Size(343, 252);
            this.SEARCH.TabIndex = 16;
            this.SEARCH.TabStop = false;
            this.SEARCH.Text = "SEARCH";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(87, 141);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 19;
            this.button4.Text = "CARI";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.CARI);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(92, 94);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 16;
            // 
            // keyword
            // 
            this.keyword.FormattingEnabled = true;
            this.keyword.Items.AddRange(new object[] {
            "ID Personnel",
            "Nama Personnel"});
            this.keyword.Location = new System.Drawing.Point(92, 57);
            this.keyword.Name = "keyword";
            this.keyword.Size = new System.Drawing.Size(200, 21);
            this.keyword.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Key";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Field";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(436, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 17;
            // 
            // Personnel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.SEARCH);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabel);
            this.Name = "Personnel";
            this.Text = "Personnel";
            this.Load += new System.EventHandler(this.Personnel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.SEARCH.ResumeLayout(false);
            this.SEARCH.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView tabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox namaPersonel;
        private System.Windows.Forms.TextBox idPersonel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton isPlayer;
        private System.Windows.Forms.ComboBox agen;
        private System.Windows.Forms.ComboBox negara;
        private System.Windows.Forms.ComboBox club;
        private System.Windows.Forms.DateTimePicker dob;
        private System.Windows.Forms.GroupBox SEARCH;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox keyword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
    }
}