﻿namespace Tugas6609
{
    partial class Transaksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.modeBeli = new System.Windows.Forms.RadioButton();
            this.modeJual = new System.Windows.Forms.RadioButton();
            this.Aktif = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.berita = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.subtotal = new System.Windows.Forms.Label();
            this.harga = new System.Windows.Forms.NumericUpDown();
            this.finbutton = new System.Windows.Forms.Button();
            this.delbutton = new System.Windows.Forms.Button();
            this.addbutton = new System.Windows.Forms.Button();
            this.tempList = new System.Windows.Forms.ListBox();
            this.targetClub = new System.Windows.Forms.ComboBox();
            this.pemainClub = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.resetButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.harga)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(899, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "CLUB";
            // 
            // modeBeli
            // 
            this.modeBeli.AutoSize = true;
            this.modeBeli.Location = new System.Drawing.Point(462, 9);
            this.modeBeli.Name = "modeBeli";
            this.modeBeli.Size = new System.Drawing.Size(48, 17);
            this.modeBeli.TabIndex = 4;
            this.modeBeli.TabStop = true;
            this.modeBeli.Text = "BELI";
            this.modeBeli.UseVisualStyleBackColor = true;
            this.modeBeli.CheckedChanged += new System.EventHandler(this.Radio_CheckedChanged);
            // 
            // modeJual
            // 
            this.modeJual.AutoSize = true;
            this.modeJual.Location = new System.Drawing.Point(533, 9);
            this.modeJual.Name = "modeJual";
            this.modeJual.Size = new System.Drawing.Size(51, 17);
            this.modeJual.TabIndex = 5;
            this.modeJual.TabStop = true;
            this.modeJual.Text = "JUAL";
            this.modeJual.UseVisualStyleBackColor = true;
            this.modeJual.CheckedChanged += new System.EventHandler(this.Radio_CheckedChanged);
            // 
            // Aktif
            // 
            this.Aktif.FormattingEnabled = true;
            this.Aktif.Location = new System.Drawing.Point(953, 11);
            this.Aktif.Name = "Aktif";
            this.Aktif.Size = new System.Drawing.Size(121, 21);
            this.Aktif.TabIndex = 6;
            this.Aktif.Text = "CLUB";
            this.Aktif.SelectedValueChanged += new System.EventHandler(this.Aktif_SelectedValueChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(462, 41);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(613, 563);
            this.dataGridView1.TabIndex = 14;
            // 
            // berita
            // 
            this.berita.FormattingEnabled = true;
            this.berita.Location = new System.Drawing.Point(10, 301);
            this.berita.Name = "berita";
            this.berita.Size = new System.Drawing.Size(429, 303);
            this.berita.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.subtotal);
            this.groupBox1.Controls.Add(this.harga);
            this.groupBox1.Controls.Add(this.finbutton);
            this.groupBox1.Controls.Add(this.delbutton);
            this.groupBox1.Controls.Add(this.addbutton);
            this.groupBox1.Controls.Add(this.tempList);
            this.groupBox1.Controls.Add(this.targetClub);
            this.groupBox1.Controls.Add(this.pemainClub);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 286);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Transaksi";
            // 
            // subtotal
            // 
            this.subtotal.AutoSize = true;
            this.subtotal.Location = new System.Drawing.Point(26, 163);
            this.subtotal.Name = "subtotal";
            this.subtotal.Size = new System.Drawing.Size(0, 13);
            this.subtotal.TabIndex = 14;
            // 
            // harga
            // 
            this.harga.Location = new System.Drawing.Point(68, 116);
            this.harga.Name = "harga";
            this.harga.Size = new System.Drawing.Size(120, 20);
            this.harga.TabIndex = 13;
            // 
            // finbutton
            // 
            this.finbutton.Location = new System.Drawing.Point(328, 234);
            this.finbutton.Name = "finbutton";
            this.finbutton.Size = new System.Drawing.Size(75, 23);
            this.finbutton.TabIndex = 12;
            this.finbutton.Text = "FINISH";
            this.finbutton.UseVisualStyleBackColor = true;
            this.finbutton.Click += new System.EventHandler(this.Finbutton_Click);
            // 
            // delbutton
            // 
            this.delbutton.Location = new System.Drawing.Point(129, 234);
            this.delbutton.Name = "delbutton";
            this.delbutton.Size = new System.Drawing.Size(75, 23);
            this.delbutton.TabIndex = 11;
            this.delbutton.Text = "DELETE";
            this.delbutton.UseVisualStyleBackColor = true;
            this.delbutton.Click += new System.EventHandler(this.Delbutton_Click);
            // 
            // addbutton
            // 
            this.addbutton.Location = new System.Drawing.Point(48, 234);
            this.addbutton.Name = "addbutton";
            this.addbutton.Size = new System.Drawing.Size(75, 23);
            this.addbutton.TabIndex = 10;
            this.addbutton.Text = "ADD";
            this.addbutton.UseVisualStyleBackColor = true;
            this.addbutton.Click += new System.EventHandler(this.Addbutton_Click);
            // 
            // tempList
            // 
            this.tempList.FormattingEnabled = true;
            this.tempList.Location = new System.Drawing.Point(194, 19);
            this.tempList.Name = "tempList";
            this.tempList.Size = new System.Drawing.Size(235, 199);
            this.tempList.TabIndex = 9;
            // 
            // targetClub
            // 
            this.targetClub.FormattingEnabled = true;
            this.targetClub.Location = new System.Drawing.Point(67, 32);
            this.targetClub.Name = "targetClub";
            this.targetClub.Size = new System.Drawing.Size(121, 21);
            this.targetClub.TabIndex = 7;
            this.targetClub.SelectedValueChanged += new System.EventHandler(this.TargetClub_SelectedValueChanged);
            // 
            // pemainClub
            // 
            this.pemainClub.FormattingEnabled = true;
            this.pemainClub.Location = new System.Drawing.Point(67, 72);
            this.pemainClub.Name = "pemainClub";
            this.pemainClub.Size = new System.Drawing.Size(121, 21);
            this.pemainClub.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "HARGA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "PEMAIN";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TARGET";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.berita);
            this.panel1.Controls.Add(this.modeJual);
            this.panel1.Controls.Add(this.modeBeli);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(12, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1094, 624);
            this.panel1.TabIndex = 15;
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(12, 9);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(96, 23);
            this.resetButton.TabIndex = 16;
            this.resetButton.Text = "RESET FORM";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // Transaksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 680);
            this.Controls.Add(this.resetButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Aktif);
            this.Controls.Add(this.label4);
            this.Name = "Transaksi";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Transaksi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.harga)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton modeBeli;
        private System.Windows.Forms.RadioButton modeJual;
        private System.Windows.Forms.ComboBox Aktif;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ListBox berita;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown harga;
        private System.Windows.Forms.Button finbutton;
        private System.Windows.Forms.Button delbutton;
        private System.Windows.Forms.Button addbutton;
        private System.Windows.Forms.ListBox tempList;
        private System.Windows.Forms.ComboBox targetClub;
        private System.Windows.Forms.ComboBox pemainClub;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label subtotal;
        private System.Windows.Forms.Button resetButton;
    }
}

