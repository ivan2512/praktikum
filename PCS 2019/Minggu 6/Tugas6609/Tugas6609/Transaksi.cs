﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tugas6609
{
    public partial class Transaksi : Form
    {
        DataTable sumberPlayer;
        List<HasilQuery> dataTransaksi;
        decimal total;
        public Transaksi()
        {
            InitializeComponent();
            harga.Maximum = decimal.MaxValue;
            harga.Minimum = 0;
            dataTransaksi = new List<HasilQuery>();
            total = 0;
        }

        private void Transaksi_Load(object sender, EventArgs e) {
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = "SELECT ID_CLUB, NAMA_CLUB FROM CLUB"
            };
            if (cmd.Connection.State != ConnectionState.Closed) 
                cmd.Connection.Close();
            cmd.Connection.Open();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataTable temp = new DataTable();
            da.Fill(temp);
            Aktif.DataSource = temp;
            Aktif.ValueMember = "ID_CLUB";
            Aktif.DisplayMember = "NAMA_CLUB";
            cmd.Connection.Close();
        }

        private void Aktif_SelectedValueChanged(object sender, EventArgs e) {
            tempList.DataSource = null;
            dataTransaksi = new List<HasilQuery>();
            panel1.Enabled = true;
            OracleCommand cmd = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText= "SELECT ID_CLUB, NAMA_CLUB FROM CLUB WHERE ID_CLUB != '"+Aktif.SelectedValue+"'"
            };
            if (cmd.Connection.State != ConnectionState.Closed) 
                cmd.Connection.Close();
            cmd.Connection.Open();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            DataTable temp = new DataTable();
            da.Fill(temp);
            targetClub.DataSource = temp;
            targetClub.ValueMember = "ID_CLUB";
            targetClub.DisplayMember = "NAMA_CLUB";

            cmd.CommandText = "SELECT * FROM PLAYER WHERE ID_CLUB = '"+Aktif.SelectedValue+"'";
            da = new OracleDataAdapter(cmd);
            temp = new DataTable();
            da.Fill(temp);
            dataGridView1.DataSource = temp;

            cmd.Connection.Close();
        }

        private void Radio_CheckedChanged(object sender, EventArgs e) {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = FormDasar.dbConnection;
            if (modeBeli.Checked) {
                cmd.CommandText = "SELECT * FROM PLAYER WHERE ID_CLUB = '" + targetClub.SelectedValue + "'";
            }
            else if (modeJual.Checked){
                cmd.CommandText = "SELECT * FROM PLAYER WHERE ID_CLUB = '" + Aktif.SelectedValue + "'";
            }
            if (cmd.CommandText.Contains("SELECT")) {
                if (cmd.Connection.State != ConnectionState.Closed) cmd.Connection.Close();
                cmd.Connection.Open();
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                sumberPlayer = new DataTable();
                da.Fill(sumberPlayer);
                Isicbplayer();
            }
        }

        private void Addbutton_Click(object sender, EventArgs e) {
            Aktif.Enabled = false;
            targetClub.Enabled = false;
            if (pemainClub.SelectedIndex > -1) {
                dataTransaksi.Add(new HasilQuery {
                    Teks = targetClub.Text + " - " + pemainClub.Text + " - " + harga.Value,
                    Idclub = targetClub.SelectedValue.ToString(),
                    Idplayer = pemainClub.SelectedValue.ToString(),
                    Harga = harga.Value,
                    Berita = pemainClub.Text
                });
                tempList.DataSource = null;
                tempList.DataSource = dataTransaksi;
                tempList.DisplayMember = "teks";
                sumberPlayer.Rows.RemoveAt(targetClub.SelectedIndex);
                Isicbplayer();
                total += harga.Value;
                subtotal.Text = total.ToString();
            }
        }

        private void Isicbplayer() {
            pemainClub.DataSource = null;
            pemainClub.DataSource = sumberPlayer;
            pemainClub.ValueMember = "ID_PLAYER";
            pemainClub.DisplayMember = "NAMA_PLAYER";
        }

        private void Delbutton_Click(object sender, EventArgs e) {
            if (tempList.SelectedIndex > -1) {
                dataTransaksi.RemoveAt(tempList.SelectedIndex);
                tempList.DataSource = null;
                tempList.DataSource = dataTransaksi;
                tempList.DisplayMember = "teks";
            }
        }

        private void Finbutton_Click(object sender, EventArgs e) {
            OracleTransaction t;
            OracleDataAdapter header, data;
            OracleCommand selectHT, selectDT;
            selectHT = new OracleCommand("SELECT * FROM H_TRANSAKSI WHERE 1=0", FormDasar.dbConnection);
            selectDT = new OracleCommand("SELECT * FROM D_TRANSAKSI WHERE 1=0", FormDasar.dbConnection);
            if (selectHT.Connection.State != ConnectionState.Closed) selectHT.Connection.Close();
            selectHT.Connection.Open();
            header = new OracleDataAdapter(selectHT);
            OracleCommandBuilder builderHT = new OracleCommandBuilder(header);
            DataTable header_transaksi = new DataTable();
            header.Fill(header_transaksi);
            DataRow baris = header_transaksi.NewRow();
            string kodeTransaksi, klubasal, klubbaru;
            OracleCommand ambilKode = new OracleCommand {
                Connection = FormDasar.dbConnection,
                CommandText = " select 'TRANS'||lpad( nvl( max( substr(id_transaksi,-3) ),0)+1,3,'0' ) from h_transaksi"
            };
            kodeTransaksi = ambilKode.ExecuteScalar().ToString();
            baris["ID_TRANSAKSI"] = kodeTransaksi;
            klubasal = Aktif.SelectedValue.ToString();
            klubbaru = targetClub.SelectedValue.ToString();
            if (modeBeli.Checked) {
                string temp=klubasal;
                klubasal = klubbaru;
                klubbaru = temp;
            }
            baris["CLUB_ASAL"] = klubasal;
            baris["CLUB_BARU"] = klubbaru;
            baris["BIAYA_TRANSFER"] = long.Parse(subtotal.Text);
            header_transaksi.Rows.Add(baris);
            if (selectDT.Connection.State != ConnectionState.Closed) selectDT.Connection.Close();
            selectDT.Connection.Open();
            data = new OracleDataAdapter(selectDT);
            OracleCommandBuilder builderDT = new OracleCommandBuilder(data);
            DataTable detail_transaksi = new DataTable();
            data.Fill(detail_transaksi);
            DataRow detailBaris = detail_transaksi.NewRow();
            foreach (var item in dataTransaksi) {
                detailBaris = detail_transaksi.NewRow();
                detailBaris["ID_TRANSAKSI"] = kodeTransaksi;
                detailBaris["ID_PLAYER"] = item.Idplayer;
                detailBaris["SUBTOTAL"] = item.Harga;
                detail_transaksi.Rows.Add(detailBaris);
            }

            t = FormDasar.dbConnection.BeginTransaction();
            try {
                header.Update(header_transaksi);
                data.Update(detail_transaksi);
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = FormDasar.dbConnection;
                foreach (DataRow item in detail_transaksi.Rows) {
                    cmd.CommandText = "UPDATE PLAYER SET ID_CLUB = '" + klubbaru + "' WHERE ID_PLAYER = '" + item["ID_PLAYER"] + "'";
                    cmd.ExecuteNonQuery();
                }
                string namaklubasal = Aktif.Text;
                string namaklubbaru = targetClub.Text;
                if (modeBeli.Checked) {
                    string temp = namaklubasal;
                    namaklubasal = namaklubbaru;
                    namaklubbaru = temp;
                }
                foreach (var item in dataTransaksi) {
                    string s = item.Berita + ":" + namaklubasal + "->" + namaklubbaru + "(EUR "+item.Harga+")";
                    berita.Items.Add(s);
                }
                total = 0;
                subtotal.Text = total.ToString();
                t.Commit();
                dataGridView1.DataSource = null;
                Aktif_SelectedValueChanged(sender, e);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                t.Rollback();
                total = 0;
                ResetButton_Click(sender, e);
            }
        }

        private void TargetClub_SelectedValueChanged(object sender, EventArgs e) {
            Radio_CheckedChanged(sender, e);
        }

        private void ResetButton_Click(object sender, EventArgs e) {
            Aktif.Enabled = true;
            targetClub.Enabled = true;
            panel1.Enabled = false;
        }
    }

    class HasilQuery {
        public string Idclub { get; set; }
        public decimal Harga { get; set; }
        public string Idplayer { get; set; }
        public string Teks { get; set; }
        public string Berita { get; set; }
    }
}
