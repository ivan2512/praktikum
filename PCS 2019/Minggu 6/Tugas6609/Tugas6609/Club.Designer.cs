﻿namespace Tugas6609 {
    partial class Club {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tabel = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.delete = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.Button();
            this.insert = new System.Windows.Forms.Button();
            this.leagues = new System.Windows.Forms.ComboBox();
            this.tahunBerdiri = new System.Windows.Forms.DateTimePicker();
            this.namaClub = new System.Windows.Forms.TextBox();
            this.idClub = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SEARCH = new System.Windows.Forms.GroupBox();
            this.searchClub = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.keyword = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tabel)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SEARCH.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabel
            // 
            this.tabel.AllowUserToAddRows = false;
            this.tabel.AllowUserToDeleteRows = false;
            this.tabel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tabel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabel.Location = new System.Drawing.Point(372, 31);
            this.tabel.Name = "tabel";
            this.tabel.ReadOnly = true;
            this.tabel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.tabel.Size = new System.Drawing.Size(800, 600);
            this.tabel.TabIndex = 0;
            this.tabel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelClub_CellClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.delete);
            this.groupBox1.Controls.Add(this.update);
            this.groupBox1.Controls.Add(this.insert);
            this.groupBox1.Controls.Add(this.leagues);
            this.groupBox1.Controls.Add(this.tahunBerdiri);
            this.groupBox1.Controls.Add(this.namaClub);
            this.groupBox1.Controls.Add(this.idClub);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(347, 379);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Club";
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(212, 291);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(75, 23);
            this.delete.TabIndex = 18;
            this.delete.Text = "DELETE";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(131, 291);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(75, 23);
            this.update.TabIndex = 17;
            this.update.Text = "UPDATE";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.Update_Click);
            // 
            // insert
            // 
            this.insert.Location = new System.Drawing.Point(50, 291);
            this.insert.Name = "insert";
            this.insert.Size = new System.Drawing.Size(75, 23);
            this.insert.TabIndex = 16;
            this.insert.Text = "INSERT";
            this.insert.UseVisualStyleBackColor = true;
            this.insert.Click += new System.EventHandler(this.Insert_click);
            // 
            // leagues
            // 
            this.leagues.FormattingEnabled = true;
            this.leagues.Location = new System.Drawing.Point(6, 214);
            this.leagues.Name = "leagues";
            this.leagues.Size = new System.Drawing.Size(200, 21);
            this.leagues.TabIndex = 14;
            // 
            // tahunBerdiri
            // 
            this.tahunBerdiri.Location = new System.Drawing.Point(6, 135);
            this.tahunBerdiri.Name = "tahunBerdiri";
            this.tahunBerdiri.Size = new System.Drawing.Size(200, 20);
            this.tahunBerdiri.TabIndex = 12;
            // 
            // namaClub
            // 
            this.namaClub.Location = new System.Drawing.Point(6, 96);
            this.namaClub.Name = "namaClub";
            this.namaClub.Size = new System.Drawing.Size(200, 20);
            this.namaClub.TabIndex = 11;
            this.namaClub.TextChanged += new System.EventHandler(this.NamaClub_TextChanged);
            // 
            // idClub
            // 
            this.idClub.Location = new System.Drawing.Point(6, 57);
            this.idClub.Name = "idClub";
            this.idClub.ReadOnly = true;
            this.idClub.Size = new System.Drawing.Size(200, 20);
            this.idClub.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "ID Liga";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Club";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tahun Berdiri";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nama Club";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID Club";
            // 
            // SEARCH
            // 
            this.SEARCH.Controls.Add(this.searchClub);
            this.SEARCH.Controls.Add(this.textBox3);
            this.SEARCH.Controls.Add(this.keyword);
            this.SEARCH.Controls.Add(this.label8);
            this.SEARCH.Controls.Add(this.label7);
            this.SEARCH.Location = new System.Drawing.Point(12, 397);
            this.SEARCH.Name = "SEARCH";
            this.SEARCH.Size = new System.Drawing.Size(343, 252);
            this.SEARCH.TabIndex = 16;
            this.SEARCH.TabStop = false;
            this.SEARCH.Text = "SEARCH";
            // 
            // searchClub
            // 
            this.searchClub.Location = new System.Drawing.Point(87, 141);
            this.searchClub.Name = "searchClub";
            this.searchClub.Size = new System.Drawing.Size(75, 23);
            this.searchClub.TabIndex = 19;
            this.searchClub.Text = "CARI";
            this.searchClub.UseVisualStyleBackColor = true;
            this.searchClub.Click += new System.EventHandler(this.Search);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(92, 94);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 16;
            // 
            // keyword
            // 
            this.keyword.FormattingEnabled = true;
            this.keyword.Items.AddRange(new object[] {
            "ID Club",
            "Nama Club"});
            this.keyword.Location = new System.Drawing.Point(92, 57);
            this.keyword.Name = "keyword";
            this.keyword.Size = new System.Drawing.Size(200, 21);
            this.keyword.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Key";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(37, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Field";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(383, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "label6";
            // 
            // Club
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.SEARCH);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabel);
            this.Name = "Club";
            this.Text = "Club";
            this.Load += new System.EventHandler(this.Club_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tabel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.SEARCH.ResumeLayout(false);
            this.SEARCH.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView tabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox namaClub;
        private System.Windows.Forms.TextBox idClub;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox leagues;
        private System.Windows.Forms.DateTimePicker tahunBerdiri;
        private System.Windows.Forms.GroupBox SEARCH;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox keyword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button insert;
        private System.Windows.Forms.Button searchClub;
        private System.Windows.Forms.Label label6;
    }
}