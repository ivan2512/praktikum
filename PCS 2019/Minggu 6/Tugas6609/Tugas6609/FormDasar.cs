﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Tugas6609 {
    public partial class FormDasar : Form {
        public static OracleConnection dbConnection;

        public FormDasar() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            try {
                dbConnection = new OracleConnection(connectionString: "Data Source = ORCL; User Id = n217116657; Password=217116657");
                if (dbConnection.State == ConnectionState.Open) dbConnection.Close();
                dbConnection.Open();
            }
            catch (Exception exx) { MessageBox.Show(exx.Message); }
        }

        private void PERSONNELToolStripMenuItem_Click(object sender, EventArgs e) {
            cLUBToolStripMenuItem.Enabled = true;
            pERSONNELToolStripMenuItem.Enabled = false;
            tRANSAKSIToolStripMenuItem.Enabled = true;
            Tutup();
            Personnel p = new Personnel {
                MdiParent = this };
            p.Show();
        }

        private void CLUBToolStripMenuItem_Click(object sender, EventArgs e) {
            cLUBToolStripMenuItem.Enabled = false;
            tRANSAKSIToolStripMenuItem.Enabled = true;
            pERSONNELToolStripMenuItem.Enabled = true;
            Tutup();
            Club c = new Club {
                MdiParent = this };
            c.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            if (dbConnection.State==ConnectionState.Open) dbConnection.Close();
        }

        private void TRANSAKSIToolStripMenuItem_Click(object sender, EventArgs e) {
            tRANSAKSIToolStripMenuItem.Enabled = false;
            pERSONNELToolStripMenuItem.Enabled = true;
            cLUBToolStripMenuItem.Enabled = true;
            Tutup();
            Transaksi t = new Transaksi {
                MdiParent = this };
            t.Show();
        }

        private void Tutup() {
            foreach (var item in MdiChildren) {
                item.Close(); }
        }
    }
}
