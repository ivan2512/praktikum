create or replace procedure no3
is
    itung number;
begin
    dbms_output.put_line('===================================================');
    dbms_output.put_line('|AGEN               |CLIENT PLAYER |CLIENT MANAGER|');
    dbms_output.put_line('===================================================');
    for i in (
        select a.id_agen, a.nama_agen as AGEN, count(p.id_agen) as CLIENT_PLAYER
        from agen a, player p 
        where a.id_agen = p.id_agen 
        group by a.nama_agen, a.id_agen
        order by substr(AGEN, 0,1)asc, CLIENT_PLAYER asc) loop

    select count(m.id_agen) into itung from MANAGER m where m.id_agen=i.id_agen;
    if itung >0 then
    dbms_output.put_line('|' || rpad(i.agen, 19, ' ')||'|'||rpad(i.CLIENT_PLAYER, 14, ' ')||'|'||rpad(itung, 14, ' ')||'|');
    end if;
    end loop;
    dbms_output.put_line('===================================================');
end;
/
show err;

exec no3();
