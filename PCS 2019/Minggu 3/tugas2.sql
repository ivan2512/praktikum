CREATE OR REPLACE FUNCTION cek_umur_player
(
    nama varchar2
)
return varchar2
is
    namaKlub varchar2(20);
    jumlahPemain number;
    idClub varchar2(7);
    usiaPalingTua number;
    usiaKu number;
    usiaPalingMuda number;
begin
    jumlahPemain := 0;
    select c.nama_club, c.id_club into namaKlub, idClub from club c, player p where p.nama_player = nama and c.id_club=p.id_club;
    if SQL%NOTFOUND then
        return 'Data not found';
    else 
        select count(p.nama_player) into jumlahPemain from player p where p.id_club = idClub;
        if jumlahPemain=1 then
            return upper(nama||' adalah satu-satunya pemain yang terdaftar pada '||namaKlub);
        else
            select (max(sysdate-p.dob_player)) into usiaPalingTua
            from player p where p.id_club = idClub;
            select (min(sysdate-p.dob_player)) into usiaPalingMuda
            from player p where p.id_club = idClub;

            select sysdate-p.dob_player into usiaKu from player p where p.nama_player = nama;
            if (usiaKu=usiaPalingMuda) then
                return upper(nama||' ADALAH PEMAIN TERMUDA DI CLUB '||namaKlub);
            elsif usiaPalingTua=usiaKu then
                return upper(nama||' ADALAH PEMAIN TERTUA DI CLUB '||namaKlub);
            else
                return upper(nama||' ADALAH PEMAIN BIASA DI KLUB '||namaKlub);
            end if;
        end if;
    end if;
end;
/


DECLARE
    X VARCHAR2(200);
 BEGIN
    X := CEK_UMUR_PLAYER('Javier Pastore');
    DBMS_OUTPUT.PUT_LINE(X);
 END;
 /
