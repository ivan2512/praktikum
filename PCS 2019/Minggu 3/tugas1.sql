--1
create or replace function auto_gen_id_player
(
    nama in varchar2
)
return varchar2
is
    kodebaru varchar2(6);
    kodelamamaks number;
    ctr_spasi number;
begin
    ctr_spasi := 0;

    for i in 0..length(nama) loop
        if (substr(nama,i,1) = ' ') then ctr_spasi := ctr_spasi+1;
        end if;
    end loop;
    if (ctr_spasi=0) then 
        kodebaru := upper(substr(nama, 0,2));
    else 
        kodebaru := substr(nama, 0, 1) || substr(nama, instr(nama, ' ', -1)+1, 1);
    end if;

    select nvl(max(substr(id_player,3)),0)+1 into kodelamamaks from player where id_player like '%'||kodebaru||'%';

    return kodebaru||lpad(kodelamamaks,3,'0');
end;
/

--2
create or replace function CHECK_PEMBELIAN
(
    nama_klub varchar2
)
return varchar2
is
    jumlahTransaksi number;
    cetak varchar2(10000);
    ctr number;
begin
    select count(id_transaksi) into jumlahTransaksi from h_transaksi where
    club_baru = (select id_club from club where nama_club = nama_klub);
    if (jumlahTransaksi=0) then
        cetak := 'CLUB '||upper(nama_klub)||' TIDAK PERNAH MEMBELI PEMAIN SIAPAPUN';
        return cetak;
    else
        cetak := 'CLUB '||upper(nama_klub)||' ';
        ctr := 0;
        for temp in (
            select upper(p.nama_player) as namaplayer, to_char(ht.tgl_transfer, 'dd-mm-yyyy') as tgl
            from h_transaksi ht, d_transaksi dt, player p
            where p.id_player = dt.id_player and ht.id_transaksi = dt.id_transaksi and ht.club_baru = (select id_club from club where club.nama_club = nama_klub)
            order by ht.tgl_transfer asc
        ) loop 
            if ctr=0 then
                cetak := cetak || 'PERNAH MEMBELI PLAYER '||temp.namaplayer||' PADA TANGGAL '||temp.tgl||' ';
            else
                cetak := cetak || 'DAN PERNAH MEMBELI PLAYER '||temp.namaplayer||' JUGA PADA TANGGAL '||temp.tgl||' ';
            end if;
            ctr := ctr +1;
        end loop;
        return cetak;
    end if;
end;
/

--3
create or replace procedure no3
is
    itung number;
begin
    dbms_output.put_line('===================================================');
    dbms_output.put_line('|AGEN               |CLIENT PLAYER |CLIENT MANAGER|');
    dbms_output.put_line('===================================================');
    for i in (
        select a.id_agen, a.nama_agen as AGEN, count(p.id_agen) as CLIENT_PLAYER
        from agen a, player p 
        where a.id_agen = p.id_agen 
        group by a.nama_agen, a.id_agen
        order by substr(AGEN, 0,1)asc, CLIENT_PLAYER asc) loop

    select count(m.id_agen) into itung from MANAGER m where m.id_agen=i.id_agen;
    if itung >0 then
    dbms_output.put_line('|' || rpad(i.agen, 19, ' ')||'|'||rpad(i.CLIENT_PLAYER, 14, ' ')||'|'||rpad(itung, 14, ' ')||'|');
    end if;
    end loop;
    dbms_output.put_line('===================================================');
end;
/
exec no3();

--4
create or replace procedure no4
is
begin
    for i in (
        select  ht.id_transaksi, p.nama_player, 
                to_char(ht.tgl_transfer, 'dd-MONyyyy') as tgltransfer, 
                c1.nama_club as clubasal, c2.nama_club as clubbaru, 
                to_char(ht.tgl_transfer, 'dd') as tanggal, 
                to_char(ht.tgl_transfer, 'MON') as bulan, 
                to_char(ht.tgl_transfer, 'yy') as "2tahun"
        from h_transaksi ht, d_transaksi dt, player p, club c1, club c2
        where ht.id_transaksi = dt.id_transaksi
        and p.id_player = dt.id_player
        and c1.id_club = ht.club_asal and c2.id_club = ht.club_baru
        order by tanggal asc, substr(bulan, 0, 1) asc, substr(bulan, 1, 2) asc, "2tahun" asc, p.nama_player asc
    ) loop
    dbms_output.put_line('=======================================');
    dbms_output.put_line('|            INFO TRANSFER            |');
    dbms_output.put_line('=======================================');
    dbms_output.put('|NAMA PLAYER      :');
    dbms_output.put_line(LPAD(i.nama_player, 19, ' ')||'|' );
    dbms_output.put('|TANGGAL TRANSFER :');
    dbms_output.put_line(LPAD(i.tgltransfer, 19, ' ')||'|' );
    dbms_output.put('|CLUB ASAL        :');
    dbms_output.put_line(LPAD(i.clubasal, 19, ' ')||'|' );
    dbms_output.put('|CLUB BARU        :');
    dbms_output.put_line(LPAD(i.clubbaru, 19, ' ')||'|' );
    dbms_output.put_line('=======================================');
    end loop;
end;
/

exec no4();
