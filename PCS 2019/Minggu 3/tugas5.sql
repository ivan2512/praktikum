create or replace procedure tugas5
is
begin
    dbms_output.put_line('+==========================================================+');
    dbms_output.put_line('|AGEN               |STADIUM            |KAPASITAS         |');
    dbms_output.put_line('+==========================================================+');
    for rows in (
        select rpad(temp.NEGARA,19) as agen, rpad(s.NAMA_STADIUM,19) as STADIUM, rpad(avg.CAP,18) as KAPASITAS
        from 
        (   select n.NAMA_NEGARA as NEGARA, max(s.KAPASITAS) as CAP
            from STADIUM s, NEGARA n
            where n.ID_NEGARA = s.ID_NEGARA
            group by n.NAMA_NEGARA
        ) temp, 
            NEGARA n, STADIUM s,
        (   select n.NAMA_NEGARA, round(max(s.KAPASITAS)) || ' KURSI' as CAP
            from STADIUM s, NEGARA n
            where s.ID_NEGARA = n.ID_NEGARA
            group by n.NAMA_NEGARA
        ) avg
        where temp.negara = n.nama_negara and n.id_negara = s.id_negara and s.kapasitas = temp.cap and temp.negara = avg.nama_negara
        order by substr(s.NAMA_STADIUM, 2,1), KAPASITAS desc
    ) loop

       dbms_output.put_line('|'||rows.agen||'|'||rows.STADIUM||'|'||rows.KAPASITAS||'|');
    end loop;
    dbms_output.put_line('+==========================================================+');
end;
/
show err;


exec tugas5();