create or replace procedure no4
is
begin
    for i in (
        select  ht.id_transaksi, p.nama_player, 
                to_char(ht.tgl_transfer, 'dd-MONyyyy') as tgltransfer, 
                c1.nama_club as clubasal, c2.nama_club as clubbaru, 
                to_char(ht.tgl_transfer, 'dd') as tanggal, 
                to_char(ht.tgl_transfer, 'MON') as bulan, 
                to_char(ht.tgl_transfer, 'yy') as "2tahun"
        from h_transaksi ht, d_transaksi dt, player p, club c1, club c2
        where ht.id_transaksi = dt.id_transaksi
        and p.id_player = dt.id_player
        and c1.id_club = ht.club_asal and c2.id_club = ht.club_baru
        order by tanggal asc, substr(bulan, 0, 1) asc, substr(bulan, 1, 2) asc, "2tahun" asc, p.nama_player asc
    ) loop
    dbms_output.put_line('=======================================');
    dbms_output.put_line('|            INFO TRANSFER            |');
    dbms_output.put_line('=======================================');
    dbms_output.put('|NAMA PLAYER      :');
    dbms_output.put_line(LPAD(i.nama_player, 19, ' ')||'|' );
    dbms_output.put('|TANGGAL TRANSFER :');
    dbms_output.put_line(LPAD(i.tgltransfer, 19, ' ')||'|' );
    dbms_output.put('|CLUB ASAL        :');
    dbms_output.put_line(LPAD(i.clubasal, 19, ' ')||'|' );
    dbms_output.put('|CLUB BARU        :');
    dbms_output.put_line(LPAD(i.clubbaru, 19, ' ')||'|' );
    dbms_output.put_line('=======================================');
    end loop;
end;
/

exec no4();