create or replace function auto_gen_person(
    nama varchar2,
    jabatan varchar2
)
return varchar2
is
    salah_jabatan exception;
    angkanya number;
    inisial varchar2(2);
    kodeHuruf varchar2(3);
    kodeBaru varchar2(6);
    ctrSpasi number;
begin
    if upper(jabatan) <> 'PLAYER' AND upper(jabatan)<> 'MANAGER'
    then raise salah_jabatan;
    end if;
    ctrSpasi := 0;
    for i in 0..length(nama) loop
        if (substr(nama,i,1) = ' ') then ctrSpasi := ctrSpasi+1;
        end if;
    end loop;
    if (ctrSpasi=0) then 
        kodeBaru := upper(substr(nama, 0,2));
        inisial := kodeBaru;
        kodeHuruf := substr(upper(jabatan),0,1)||inisial;
    else 
        kodeBaru := substr(nama, 0, 1) || substr(nama, instr(nama, ' ', -1)+1, 1);
        inisial := kodeBaru;
        kodeHuruf := substr(upper(jabatan),0,1)||inisial;
    end if;

    if (upper(jabatan) = 'PLAYER') then
        select nvl(max(substr(id_player,3)),0)+1 into angkanya from player where id_player like '%'||inisial||'%';
        kodeBaru:= kodeHuruf||lpad(angkanya,3,'0');
    else 
        select nvl(max(substr(id_manager,4)),0)+1 into angkanya from manager where id_manager like '%'||kodeHuruf||'%';
        kodeBaru:= kodeHuruf||lpad(angkanya,3, '0');
    end if;
    return kodeBaru;
end;
/

show err;

declare
begin
    dbms_output.put_line(auto_gen_person('Andika', 'Manager'));
end;
/
show err;