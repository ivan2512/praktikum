create or replace function auto_gen_id_player
(
    nama in varchar2
)
return varchar2
is
    kodebaru varchar2(6);
    kodelamamaks number;
    ctr_spasi number;
begin
    ctr_spasi := 0;

    for i in 0..length(nama) loop
        if (substr(nama,i,1) = ' ') then ctr_spasi := ctr_spasi+1;
        end if;
    end loop;
    if (ctr_spasi=0) then 
        kodebaru := upper(substr(nama, 0,2));
    else 
        kodebaru := substr(nama, 0, 1) || substr(nama, instr(nama, ' ', -1)+1, 1);
    end if;

    select nvl(max(substr(id_player,3)),0)+1 into kodelamamaks from player where id_player like '%'||kodebaru||'%';

    return kodebaru||lpad(kodelamamaks,3,'0');
end;
/

declare
begin
    dbms_output.put_line(auto_gen_id_player('Antoine Griezmann'));
    dbms_output.put_line(auto_gen_id_player('Misaki Tsubasa A'));
    dbms_output.put_line(auto_gen_id_player('Nacho'));
end;
/
