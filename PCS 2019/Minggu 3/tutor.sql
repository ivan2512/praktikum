
--PROCEDURE
create or replace procedure prod1
(
    nama IN varchar2
)
is
    temp varchar2(100);
begin
    temp := 'Hello '||nama;
    dbms_output.put('Hello ');
    dbms_output.put(nama);
    dbms_output.put_line('');
end;
/
show err;

create or replace procedure ambilGolongan
is
begin
    FOR i in (SELECT * FROM GOLONGAN) LOOP
        dbms_output.put_line(i.kode_gol);
        FOR x in (SELECT nama_peg FROM pegawai where kode_gol = i.kode_gol) LOOP
            dbms_output.put(x.nama_peg || ' ');
        END LOOP;
        dbms_output.put_line('');
    END LOOP;
    dbms_output.put_line('');
end;
/
show err;

create or replace function autogen
return golongan.kode_gol%type
is
    num number;
begin
    select max(substr(kode_gol,4,2))+1 into num from golongan;
    dbms_output.put_line('gol'||lpad(num,2,'0'));
    return 'gol'||lpad(num,2,'0');
end;
/


declare
    err exception;
begin
    dbms_output.put_line(5 mod 2);
    dbms_output.put_line(5/2);
    dbms_output.put_line(5 / 2);
    for i in reverse 1..5
    LOOP
        dbms_output.put_line(i);
        if (i mod 2 = 0) then 
            raise err;
        else dbms_output.put_line('ganjil');
        end if;
    end loop;
exception 
    when err then raise_application_error(-20001, 'Angka genap');
end;
/

