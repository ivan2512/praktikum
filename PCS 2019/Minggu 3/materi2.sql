create or replace function CHECK_PEMBELIAN
(
    nama_klub varchar2
)
return varchar2
is
    jumlahTransaksi number;
    cetak varchar2(10000);
    ctr number;
begin
    select count(id_transaksi) into jumlahTransaksi from h_transaksi where
    club_baru = (select id_club from club where nama_club = nama_klub);
    if (jumlahTransaksi=0) then
        cetak := 'CLUB '||upper(nama_klub)||' TIDAK PERNAH MEMBELI PEMAIN SIAPAPUN';
        return cetak;
    else
        cetak := 'CLUB '||upper(nama_klub)||' ';
        ctr := 0;
        for temp in (
            select upper(p.nama_player) as namaplayer, to_char(ht.tgl_transfer, 'dd-mm-yyyy') as tgl
            from h_transaksi ht, d_transaksi dt, player p
            where p.id_player = dt.id_player and ht.id_transaksi = dt.id_transaksi and ht.club_baru = (select id_club from club where club.nama_club = nama_klub)
            order by ht.tgl_transfer asc
        ) loop 
            if ctr=0 then
                cetak := cetak || 'PERNAH MEMBELI PLAYER '||temp.namaplayer||' PADA TANGGAL '||temp.tgl||' ';
            else
                cetak := cetak || 'DAN PERNAH MEMBELI PLAYER '||temp.namaplayer||' JUGA PADA TANGGAL '||temp.tgl||' ';
            end if;
            ctr := ctr +1;
        end loop;
        return cetak;
    end if;
end;
/
show err

