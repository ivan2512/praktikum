create or replace procedure tugas4
is
begin
    for rows in (
        select c.nama_club as nc, to_char(c.tahun_berdiri, 'yyyy') as year, l.nama_liga as liga, m.nama_manager as manager, s.nama_stadium as stadium
        from club c, liga l, club_manager cm, manager m, stadium s
        where l.id_liga=c.id_liga and cm.id_club = c.id_club and m.id_manager = cm.id_manager and s.id_club = c.id_club
        order by 1, 2 asc
    ) loop 
        dbms_output.put_line('#######################################');
        dbms_output.put_line('|            INFO CLUB                |');
        dbms_output.put_line('#######################################');
        dbms_output.put('|'||rpad('NAMA CLUB', 17, ' '));
        dbms_output.put_line(':'||rpad(lpad(rows.nc, 19, ' '), 20, '|'));
        dbms_output.put('|'||rpad('TAHUN BERDIRI', 17, ' '));
        dbms_output.put_line(':'||rpad(lpad(rows.year, 19, ' '), 20, '|'));
        dbms_output.put('|'||rpad('LIGA', 17, ' '));
        dbms_output.put_line(':'||rpad(lpad(rows.liga, 19, ' '), 20, '|'));
        dbms_output.put('|'||rpad('MANAGER CLUB', 17, ' '));
        --supaya persis hasil soal
        if (length(rows.manager)<=19) then
            dbms_output.put_line(':'||lpad(rows.manager, 19, ' ')||'|');
        else 
            dbms_output.put_line(':'||rows.manager);
        end if;
        dbms_output.put('|'||rpad('STADIUM', 17, ' '));
        dbms_output.put_line(':'||rpad(lpad(rows.stadium, 19, ' '), 20, '|'));
        dbms_output.put_line('#######################################');
    end loop;
end;
/
show err;

exec tugas4();

