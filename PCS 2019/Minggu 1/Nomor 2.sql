select temp.NEGARA as "Nama Negara", s.NAMA_STADIUM as "Stadion Terbesar", lpad(rpad(avg.CAP,20,' '),27,' ') as "Kapasitas Stadion Rata2"
from 
(	select n.NAMA_NEGARA as NEGARA, max(s.KAPASITAS) as CAP
	from STADIUM s, NEGARA n
	where n.ID_NEGARA = s.ID_NEGARA
	group by n.NAMA_NEGARA
) temp, 
	NEGARA n, STADIUM s,
(	select n.NAMA_NEGARA, round(avg(s.KAPASITAS)) || ' Orang' as CAP
	from STADIUM s, NEGARA n
	where s.ID_NEGARA = n.ID_NEGARA
	group by n.NAMA_NEGARA
	order by 1 desc
) avg
where temp.negara = n.nama_negara and n.id_negara = s.id_negara and s.kapasitas = temp.cap and temp.negara = avg.nama_negara
order by 3 desc;