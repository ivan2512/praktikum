SQL> disc
Disconnected from Oracle Database 11g Enterprise Edition Release 11.2.0.1.0 - Production
With the Partitioning, OLAP, Data Mining and Real Application Testing options
SQL> conn ivan/password
Connected.
SQL> @"E:\Ivan Marcellino\Uni\Semester 4\Proyek PCS\prak e4\Minggu 1\PCSM1_6615.sql"

Club Aktif Transfer 3 Tahun
------------------------------
Arsenal
Chelsea
Real Madrid
Barcelona
Manchester United
Manchester City

6 rows selected.


Nama Negara     Stadion Terbesar     Kapasitas Stadion Rata2
--------------- -------------------- ---------------------------
SPAIN           Camp Nou                    68356 Orang
ITALY           Stadio Olimpico             49629 Orang
FRANCE          Parc des Princes            47900 Orang
JAPAN           Nissan Stadium              46040 Orang
ENGLAND         Gavea Stadium               42278 Orang
BELGIUM         Jan Breydel                 24760 Orang

6 rows selected.


NAMA_MANAGER         NAMA_CLUB
-------------------- --------------------
Alex Ferguson        Manchester United
Arsene Wenger        Arsenal
Josep Guardiola      Manchester City
Maurizio Sarri       Chelsea
Christophe Galtier   Liverpool
Jurgen Klopp         Liverpool
Mauricio Pochettino  Tottenham
Ernesto Valverde     Barcelona
Santiago Solari      Real Madrid
Diego Simeone        Atletico Madrid
Pablo Machin         Sevilla

NAMA_MANAGER         NAMA_CLUB
-------------------- --------------------
Marcelino Toral      Valencia
Massimiliano Allegri Juventus
Gennaro Gattuso      AC Milan
Luciano Spalletti    Inter Milan
Carlo Ancelotti      Napoli
Eusebio Di Francesco AS Roma
Thomas Tuchel        Paris Saint Germain
Leonardo Jardim      AS Monaco
Bruno Genesio        Lyon
Rudi Garcia          Marseille
Fernando Santos      Santos

NAMA_MANAGER         NAMA_CLUB
-------------------- --------------------
Carlos Parreira      Sao Paulo
Abel Braga           Flamengo
Renato Portaluppi
Jose Mourinho
Kenny Dalglish
Zinedine Zidane
Tsuneyasu Miyamoto
Marcelo Gallardo
Paolo Maldini
Greg Abbot
Harry Redknapp

NAMA_MANAGER         NAMA_CLUB
-------------------- --------------------
Gustavo Alfaro

34 rows selected.


NAMA_PLAYER          TOTAL_PENJUALAN NEGARA_ASAL
-------------------- --------------- ---------------
       Romelu Lukaku        99000000 BELGIUM
              Neymar       150000000 BRAZIL
Cristiano Ronaldo          160000000 PORTUGAL
Angel Di Maria             100000000 ARGENTINA
Paul Pogba                  73500000 FRANCE
Diego Costa                 85000000 SPAIN
  Philippe Countinho        99000000 BRAZIL

7 rows selected.