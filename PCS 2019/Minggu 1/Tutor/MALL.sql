drop table customer cascade constraint purge;
drop table dtrans cascade constraints purge;
drop table jenis_ruang cascade constraint purge;
drop table ruang cascade constraint purge;
drop table golongan cascade constraint purge;
drop table htrans cascade constraint purge;
drop table keluarga cascade constraint purge;
drop table pegawai cascade constraint purge;
drop table penggajian cascade constraint purge;                       

create table jenis_ruang
(
 kode_jenis  char(5),
 nama_jenis  varchar2(15) not null,
 harga_sewa  number(9) not null,
 constraint pk_kode_jenis primary key(kode_jenis),
 constraint ck_harga_sewa check(harga_sewa > 100000)
);

create table golongan
(
 kode_gol   char(5),
 g_pokok    number(8) not null,
 g_tambah   number(8),
 tunjangan  number(8),
 constraint pk_kode_gol primary key(kode_gol),
 constraint ck_tunjangan check (tunjangan > 0)
);

create table customer
(
 kode_cust  char(8),
 nama_cust  varchar2(20) not null,
 alamat_cust  varchar2(25) not null,
 telp_cust  varchar2(12) not null,
 constraint pk_kode_cust primary key(kode_cust)
);

create table ruang
(
 nir         char(7),
 kondisi     char(1),
 kode_jenis  char(5),
 constraint pk_nir primary key(nir),
 constraint ck_nir check(kondisi = 't' or kondisi = 'k'),
 constraint fk_kode_jenis foreign key(kode_jenis) 
 references jenis_ruang(kode_jenis)  
);

create table pegawai
(
 nip       char(5),
 nama_peg  varchar2(20) not null,
 menikah   char(1),
 jum_anak  number(2),
 kode_gol  char(5),
 constraint pk_nip primary key(nip),
 constraint ck_menikah check(menikah = 's' or menikah = 'm'),
 constraint fk_kode_gol foreign key(kode_gol) 
 references golongan(kode_gol)
);

create table keluarga
(
 nip        char(5),
 nama_kel   varchar2(20),
 status     char(1),
 constraint fk_nip foreign key(nip) 
 references pegawai(nip)
);

create table htrans
(
 nonota     char(9),
 nip        char(5),
 kode_cust  char(8),
 tgl_sewa   date,
 constraint pk_nonota primary key(nonota),
 constraint fk_nip2 foreign key(nip) 
 references pegawai(nip),
 constraint fk_kode_cust foreign key(kode_cust) 
 references customer(kode_cust)
);

create table dtrans
(
 nonota     char(9),
 nir        char(7),
 lama_sewa  number(6),
 constraint fk_nonota foreign key(nonota) 
 references htrans(nonota),
 constraint fk_nir foreign key(nir) 
 references ruang(nir),
 constraint pk_nir_nonota primary key(nonota,nir)
);

create table penggajian
(
 id_gaji    varchar2(11),
 tunj       char(1),
 jum_gaji   number(7),
 constraint pk_id_gaji primary key(id_gaji),
 constraint ck_tunj check(tunj = 'd' or tunj = 't'),
 constraint ck_jum_gaji check(jum_gaji > 100000)
);

insert into jenis_ruang values ('jr001','stan besar','5000000');
insert into jenis_ruang values ('jr002','stan sedang','3000000');
insert into jenis_ruang values ('jr003','stan kecil','1000000');
insert into jenis_ruang values ('jr004','stan super','9000000');

insert into golongan values ('gol01',1200000,25000,250000);
insert into golongan values ('gol02',1500000,35000,300000);
insert into golongan values ('gol03',800000 ,45000,200000);
insert into golongan values ('gol04',600000 ,25000,220000);

insert into customer values ('0813ja01','jajang suherman'	,'gembong ii/2'			,'081323242089');
insert into customer values ('0879su01','sutiyono'			,'embong kenongo 150'	,'08793287610');
insert into customer values ('0317ke01','ketut ismail'		,'pemuda 187'			,'03170070770');
insert into customer values ('0316ii01','iin'				,'ngagel jaya 54'		,'03160600606');
insert into customer values ('0317su02','supono'			,'ngagel tama ii/43'	,'0317325638');
insert into customer values ('0888en01','entis sutrisna'	,'raya darmo 212'		,'088880829101');

insert into ruang values ('rua0201','t','jr002');
insert into ruang values ('rua0101','t','jr001');
insert into ruang values ('rua0301','k','jr003');
insert into ruang values ('rua0302','k','jr003');
insert into ruang values ('rua0202','t','jr002');
insert into ruang values ('rua0102','k','jr001');
insert into ruang values ('rua0401','k','jr004');
insert into ruang values ('rua0402','k','jr004');

insert into pegawai values ('dw001','dwianto'			,'m','0','gol01');
insert into pegawai values ('dw002','dimas warsito'		,'s','0','gol02');
insert into pegawai values ('jp001','johan efrat pohan'	,'m','1','gol02');
insert into pegawai values ('pi001','pohan indri'		,'s','0','gol03');
insert into pegawai values ('es001','edi sudarsono'		,'m','0','gol04');
insert into pegawai values ('rl001','rojali lin'		,'m','2','gol01');
insert into pegawai values ('nb001','norman buto'		,'s','0','gol03');

insert into keluarga values ('es001','maria antonia'   ,'i');
insert into keluarga values ('dw001','mandy lantaroz'  ,'i');
insert into keluarga values ('dw001','lucky rosalie'   ,'a');
insert into keluarga values ('jp001','kent eleazar'    ,'s');
insert into keluarga values ('jp001','merry ongko'     ,'a');
insert into keluarga values ('rl001','silviana'        ,'i');
insert into keluarga values ('rl001','margaretha'      ,'a');
insert into keluarga values ('rl001','rosales antagues','a');

insert into htrans values ('sw1003001','dw001','0813ja01',to_date('10/03/2017','dd-mm-yyyy'));
insert into htrans values ('sw0110001','dw002','0879su01',to_date('01/10/2016','dd-mm-yyyy'));
insert into htrans values ('sw3009001','jp001','0813ja01',to_date('30/09/2016','dd-mm-yyyy'));
insert into htrans values ('sw2509001','dw001','0317ke01',to_date('25/09/2016','dd-mm-yyyy'));
insert into htrans values ('sw1012001','pi001','0888en01',to_date('10/12/2016','dd-mm-yyyy'));
insert into htrans values ('sw3112001','pi001','0813ja01',to_date('31/12/2016','dd-mm-yyyy'));
insert into htrans values ('sw2512001','jp001','0317ke01',to_date('25/12/2016','dd-mm-yyyy'));
insert into htrans values ('sw0101001','pi001','0317ke01',to_date('01/01/2017','dd-mm-yyyy'));

insert into dtrans values ('sw1003001','rua0201','90');
insert into dtrans values ('sw0110001','rua0101','120');
insert into dtrans values ('sw3009001','rua0301','1825');
insert into dtrans values ('sw2509001','rua0302','30');
insert into dtrans values ('sw1012001','rua0202','30');
insert into dtrans values ('sw3112001','rua0102','14');
insert into dtrans values ('sw2512001','rua0102','2');
insert into dtrans values ('sw0101001','rua0402','2');
insert into dtrans values ('sw2509001','rua0402','2');

insert into penggajian values ('210217dw001','d','1250000');
insert into penggajian values ('220217dw002','t','1535000');
insert into penggajian values ('210217jp001','d','1835000');
insert into penggajian values ('230217pi001','t','845000');
insert into penggajian values ('230217es001','t','625000');
                                     
commit;     
