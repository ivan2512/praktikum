select DISTINCT rpad(c.NAMA_CLUB,30,' ') as "Club Aktif Transfer 3 Tahun"
from CLUB c, H_TRANSAKSI ht1, H_TRANSAKSI ht2, H_TRANSAKSI ht3
where 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2017'))
	or
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2018') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2017'))
	or 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
	or
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2013') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
	or 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2012') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2013') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
order by substr("Club Aktif Transfer 3 Tahun", 2) desc;