Advanced Query - Yohanes Wijoyo

|---------------------------------------------|
| !!! COMPARISON EQUAL PAKAI 1 TANDA SAJA !!! |
|---------------------------------------------|

Left Join : menampilkan semua data yang ada di kiri
Right Join : menampilkan semua data yang ada di kanan
Inner Join : from where

SELECT p.nama_peg as "Nama Pegawai", k.nama_kel as "Keluarga"
from pegawai p 
left outer join keluarga k on
p.nip = k.nip
;

sama dengan

SELECT initcap(p.nama_peg) as "Nama Pegawai", k.nama_kel as "Keluarga"
from keluarga k
right outer join pegawai p on
p.nip = k.nip
;

------------------------

select 
d.nonota as "No Nota", 
jr.nama_jenis as "Nama Jenis"
from 
jenis_ruang jr 
inner join ruang r 
on jr.kode_jenis = r.kode_jenis
inner join dtrans d 
on r.nir = d.nir;

sama dengan

select 
d.nonota as "No Nota", 
jr.nama_jenis as "Nama Jenis"
from 
jenis_ruang jr, ruang r, dtrans d
where 
jr.kode_jenis = r.kode_jenis and 
r.nir = d.nir;

------------------------------
SUBQUERY

select h.nonota as "No Nota", tabel_sementara.nip
from htrans h, (
	select nonota, nip from htrans where to_char(tgl_sewa, 'YYYY') = '2017'
) tabel_sementara
where h.nonota = tabel_sementara.nonota;

------------------------------

/--HAVING

select nonota as "No Nota" 
from dtrans
having sum(lama_sewa) = (
	select max(sum(lama_sewa))
	from dtrans
	group by nonota
	)
group by nonota;



Asisten Lab E4 : 
Kenny Vincent
Yohanes Wijoyo
Natanael Simogiarto

Kasub PCS : Natanael Simogiarto
Koordinator Perkuliahan : Riandika Lumaris

