--1
select tabelManager.NAMA_AGEN as "NAMA_AGEN", tabelPlayer."Client Player", tabelManager."Client Manager"
from 
	(select a.nama_agen as "NAMA_AGEN", count(p.id_player) as "Client Player"
	from player p inner join agen a on
	a.id_agen = p.id_agen
	group by a.nama_agen) tabelPlayer, 
	(select a.nama_agen as "NAMA_AGEN", count(m.id_manager) as "Client Manager"
	from manager m inner join agen a on
	a.id_agen = m.id_agen
	group by a.nama_agen) tabelManager,
	agen a
where tabelManager.NAMA_AGEN = tabelPlayer.NAMA_AGEN
and a.nama_agen = tabelManager.nama_agen

--2
select m.nama_manager, nvl(a.nama_agen, 'Tidak Memiliki Agen') as "Nama Agen"
from manager m left join
agen a
on a.id_agen = m.id_agen;

--3
select a.nama_agen as "Nama Agen", 
p.nama_player as "Nama Player", m.nama_manager as "Nama Manager"
from agen a, player p, manager m
where a.id_agen = p.id_agen and m.id_agen = a.id_agen
and m.id_agen = p.id_agen
order by 1 asc;

--4
select temp.negara as "Nama Negara", s.nama_stadium as "Stadion Terbesar", temp.cap as "Kapasitas Stadion"
from 
	(select n.nama_negara as negara, max(s.kapasitas) as cap
	from stadium s, negara n
	where n.id_negara = s.id_negara
	group by n.nama_negara) temp, negara n, stadium s
where temp.negara = n.nama_negara and n.id_negara = s.id_negara and s.kapasitas = temp.cap
order by 3 desc;