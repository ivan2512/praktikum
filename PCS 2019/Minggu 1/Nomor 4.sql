select 	case 
			when n.NAMA_NEGARA like '%B%' then 
				lpad(p.NAMA_PLAYER, 20, ' ') 
			else 
				p.NAMA_PLAYER 
			end 			as "NAMA_PLAYER", 
		sum(dt.SUBTOTAL) 	as "TOTAL_PENJUALAN", 
		n.NAMA_NEGARA 		as "NEGARA_ASAL"
from PLAYER p, D_TRANSAKSI dt, NEGARA n
where 	p.ID_NEGARA = n.ID_NEGARA
		and p.ID_PLAYER = dt.ID_PLAYER
		having avg(dt.SUBTOTAL) > 30000000
			and sum(dt.SUBTOTAL) > 50000000 
			and count(p.ID_PLAYER) > 1
group by p.NAMA_PLAYER, n.NAMA_NEGARA, p.ID_PLAYER
order by substr(p.NAMA_PLAYER, length(p.NAMA_PLAYER)-6);