--1
select DISTINCT rpad(c.NAMA_CLUB,30,' ') as "Club Aktif Transfer 3 Tahun"
from CLUB c, H_TRANSAKSI ht1, H_TRANSAKSI ht2, H_TRANSAKSI ht3
where 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2017'))
	or
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2018') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2017'))
	or 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2016') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
	or
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2015') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2013') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
	or 
	(((ht1.CLUB_ASAL = c.ID_CLUB or ht1.CLUB_BARU = c.ID_CLUB) 
		and to_char(ht1.TGL_TRANSFER,'YYYY') = '2012') and 
	((ht2.CLUB_ASAL = c.ID_CLUB or ht2.CLUB_BARU = c.ID_CLUB )
		and to_char(ht2.TGL_TRANSFER,'YYYY') = '2013') and 
	((ht3.CLUB_ASAL = c.ID_CLUB or ht3.CLUB_BARU = c.ID_CLUB )
		and to_char(ht3.TGL_TRANSFER,'YYYY') = '2014'))
order by substr("Club Aktif Transfer 3 Tahun", 2) desc;

--2
select temp.NEGARA as "Nama Negara", s.NAMA_STADIUM as "Stadion Terbesar", lpad(rpad(avg.CAP,20,' '),27,' ') as "Kapasitas Stadion Rata2"
from 
(	select n.NAMA_NEGARA as NEGARA, max(s.KAPASITAS) as CAP
	from STADIUM s, NEGARA n
	where n.ID_NEGARA = s.ID_NEGARA
	group by n.NAMA_NEGARA
) temp, 
	NEGARA n, STADIUM s,
(	select n.NAMA_NEGARA, round(avg(s.KAPASITAS)) || ' Orang' as CAP
	from STADIUM s, NEGARA n
	where s.ID_NEGARA = n.ID_NEGARA
	group by n.NAMA_NEGARA
	order by 1 desc
) avg
where temp.negara = n.nama_negara and n.id_negara = s.id_negara and s.kapasitas = temp.cap and temp.negara = avg.nama_negara
order by 3 desc;

--3
select m.NAMA_MANAGER as NAMA_MANAGER, c.NAMA_CLUB as NAMA_CLUB
from CLUB c 
right join CLUB_MANAGER cm
on c.ID_CLUB = cm.ID_CLUB
right join MANAGER m
on m.ID_MANAGER = cm.ID_MANAGER;

--4
select 	case 
			when n.NAMA_NEGARA like '%B%' then 
				lpad(p.NAMA_PLAYER, 20, ' ') 
			else 
				p.NAMA_PLAYER 
			end 			as "NAMA_PLAYER", 
		sum(dt.SUBTOTAL) 	as "TOTAL_PENJUALAN", 
		n.NAMA_NEGARA 		as "NEGARA_ASAL"
from PLAYER p, D_TRANSAKSI dt, NEGARA n
where 	p.ID_NEGARA = n.ID_NEGARA
		and p.ID_PLAYER = dt.ID_PLAYER
		having avg(dt.SUBTOTAL) > 30000000
			and sum(dt.SUBTOTAL) > 50000000 
			and count(p.ID_PLAYER) > 1
group by p.NAMA_PLAYER, n.NAMA_NEGARA, p.ID_PLAYER
order by substr(p.NAMA_PLAYER, length(p.NAMA_PLAYER)-6);

