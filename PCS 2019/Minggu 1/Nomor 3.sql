select m.NAMA_MANAGER as NAMA_MANAGER, c.NAMA_CLUB as NAMA_CLUB
from CLUB c 
right join CLUB_MANAGER cm
on c.ID_CLUB = cm.ID_CLUB
right join MANAGER m
on m.ID_MANAGER = cm.ID_MANAGER;