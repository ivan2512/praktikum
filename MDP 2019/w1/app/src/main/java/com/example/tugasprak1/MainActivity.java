package com.example.tugasprak1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Random r;
    private Button btns[][];
    private int counter;
    private boolean hasPressed;
    private Button previous, restart;
    private TextView move, helper;
    private String sekarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        startGame();
        Button _restart = findViewById(R.id.btnRestart);
        TextView _move = findViewById(R.id.tvMove);
        this.restart = _restart;
        this.move = _move;
         helper = findViewById(R.id.helpler);
        _restart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startGame();
            }
        });
    }

    private void startGame(){
        if (restart==null)
            restart = findViewById(R.id.btnRestart);
        if (move==null)
            move = findViewById(R.id.tvMove);
        restart.setText("RESTART");
        move.setText("Move: 0");

        for (Button row[]: btns)
            for (Button ele: row) {
                ele.setText("");
                ele.setBackgroundResource(android.R.drawable.btn_default);
            }
        int temp = r.nextInt(3);
        Button sementara;
        switch (temp){
            case 0:
                sementara = btns[0][0];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[0][1];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                sementara = btns[0][4];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                sementara = btns[1][1];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                sementara = btns[1][2];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                sementara = btns[2][2];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[3][2];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                sementara = btns[4][4];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                break;
            case 1:
                sementara = btns[0][2];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[0][3];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                sementara = btns[1][1];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                sementara = btns[2][2];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                sementara = btns[3][1];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                sementara = btns[3][2];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                sementara = btns[4][1];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[4][2];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                break;
            case 2:
                sementara = btns[0][0];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                sementara = btns[0][1];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[1][3];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                sementara = btns[2][1];
                sementara.setBackgroundColor(Color.CYAN);
                sementara.setText("B");
                sementara = btns[2][2];
                sementara.setBackgroundColor(Color.YELLOW);
                sementara.setText("Y");
                sementara = btns[3][1];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                sementara = btns[4][3];
                sementara.setBackgroundColor(Color.RED);
                sementara.setText("R");
                sementara = btns[4][4];
                sementara.setBackgroundColor(Color.GREEN);
                sementara.setText("G");
                break;
        }
        hasPressed = false;
        previous = null;
        sekarang = "NULL";
        counter = 0;
        TextView status = findViewById(R.id.tvStatus);
        status.setText("");
    }

    private void init(){
        this.r = new Random();
        btns = new Button[5][5];
        btns[0][0] = findViewById(R.id.btn00); btns[0][1] = findViewById(R.id.btn01);
        btns[0][2] = findViewById(R.id.btn02); btns[0][3] = findViewById(R.id.btn03);
        btns[0][4] = findViewById(R.id.btn04); btns[1][0] = findViewById(R.id.btn10);
        btns[1][1] = findViewById(R.id.btn11); btns[1][2] = findViewById(R.id.btn12);
        btns[1][3] = findViewById(R.id.btn13); btns[1][4] = findViewById(R.id.btn14);
        btns[2][0] = findViewById(R.id.btn20); btns[2][1] = findViewById(R.id.btn21);
        btns[2][2] = findViewById(R.id.btn22); btns[2][3] = findViewById(R.id.btn23);
        btns[2][4] = findViewById(R.id.btn24); btns[3][0] = findViewById(R.id.btn30);
        btns[3][1] = findViewById(R.id.btn31); btns[3][2] = findViewById(R.id.btn32);
        btns[3][3] = findViewById(R.id.btn33); btns[3][4] = findViewById(R.id.btn34);
        btns[4][0] = findViewById(R.id.btn40); btns[4][1] = findViewById(R.id.btn41);
        btns[4][2] = findViewById(R.id.btn42); btns[4][3] = findViewById(R.id.btn43);
        btns[4][4] = findViewById(R.id.btn44);
        for (Button row[]: btns)
            for (Button ele: row)
                ele.setOnClickListener(this);
    }

    private Button atas(int y, int x){
        y--;
        if (y>=0)
            return btns[y][x];
        else return null;
    }

    private Button bawah(int y, int x){
        y++;
        if (y<5)
            return btns[y][x];
        else return null;
    }

    private Button kiri(int y, int x){
        x--;
        if (x>=0)
            return btns[y][x];
        else return null;
    }

    private Button kanan(int y, int x){
        x++;
        if (x<5)
            return btns[y][x];
        else return null;
    }

    @Override
    public void onClick(View v) {
        Button b = (Button)v;
        if (!hasPressed){
            hasPressed = true;
            if (!b.getText().equals("")) {
                this.previous = b;
                sekarang = b.getText().toString();
                helper.setText(sekarang);
            }
        }
        else{
            //kalau neken start / end point lain
            if(!b.getText().equals(sekarang) && !b.getText().equals("")) {
                Toast.makeText(this, "Reset warna", Toast.LENGTH_SHORT).show();
                for (Button[] row:btns) {
                    for (Button tombol:row){
                        if (tombol.getText().equals("") && !tombol.equals(previous) ){
                            if (tombol.getBackground().equals(previous.getBackground())){
                                tombol.setBackgroundResource(android.R.drawable.btn_default);
                            }
                        }
                    }
                }
                if (!previous.getText().equals(sekarang))
                    previous.setBackgroundResource(android.R.drawable.btn_default);
                hasPressed = false;
                previous = null;
                sekarang = "NULL";
                helper.setText(sekarang);
            } else {
                int x=-1, y=-1;
                for (int i=0; i<btns.length; ++i){
                    for (int j=0; j<btns.length; ++j){
                        if (btns[i][j] == previous){
                            x = j; y = i;
                            break;
                        }
                    }
                }
                if (b.equals(atas(y, x)) || b.equals(bawah(y,x)) ||
                        b.equals(kiri(y,x)) || b.equals(kanan(y,x))){
                    if (!(b.getBackground() instanceof ColorDrawable)){
                        b.setBackground(previous.getBackground());
                        this.previous = b;
                        counter++;
                        move.setText("Move: "+counter);
                    } else if (b.getText().equals(sekarang)) {
                        Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show();
                        sekarang = "NULL";
                        hasPressed = false;
                        boolean flag = true;
                        for (Button[] i: btns) {
                            for (Button j:i){
                                if (!(j.getBackground() instanceof ColorDrawable))
                                    flag = false;
                            }
                        }
                        if (flag){
                            //Toast.makeText(this, "Layar Penuh", Toast.LENGTH_SHORT).show();
                            TextView status = findViewById(R.id.tvStatus);
                            status.setText("YOU WIN");
                            restart.setText("PLAY AGAIN");
                        }
                    }
                    else{
                        //Toast.makeText(this, "Sudah ada warnanya", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Toast.makeText(this, "Hanya bisa klik button disebelah button sebelumnya",
                    // Toast.LENGTH_SHORT).show();
                    if (b.getText().equals(sekarang))
                        Toast.makeText(this, "Belum sampai", Toast.LENGTH_SHORT).show();
                }
            }
            helper.setText(sekarang);
        }
        

    }
}
