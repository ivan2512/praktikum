<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class materi extends CI_Controller {

    public function __construct(Type $var = null)
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data = [];
        $this->load->view('register', $data);
    }
    
    private function insertMhs($nama = "", $alamat = "", $telepon = "")
    {
        $this->user->insertMhs($nama, $alamat);
    }

    private function updateMhs($id, $nama = "", $alamat = "", $telepon = "")
    {
        if (isset($id)){
            $hasil = $this->user->updateMhs($id, $nama, $alamat, $telepon);
            var_dump($hasil);
        }
    }
    public function login(Type $var = null)
    {
        $this->load->view('login');
    }

    public function masuk()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $detail = $this->user->getDetail($username, $password);
        if ($detail !== null){
            $data['plot'] = $this->user->getAllPlot();
            $data['detail'] =$detail;
            $this->main($data);
        } else {
            redirect(site_url('materi/login'));
        }
    }

    public function tanam($petakKe, $username)
    {
        $this->user->tanam($petakKe, $username);
        $detail = $this->user->getLagi($username);
        $data['detail'] =$detail;
        $data['plot'] = $this->user->getAllPlot();
        $this->main($data);
    }

    public function logout(Type $var = null)
    {
        $this->user->logout();
    }

    public function main($data)
    {
        $this->load->view('main', $data);
    }
    public function masterRegister()
    {
        $postan = $this->input->post();
        $this->form_validation->set_rules('username', 'username', 'required|min_length[6]');
        $this->form_validation->set_rules('password', 'password', 'required|min_length[8]|alpha_numeric');
        $nama = $postan['username'];
        $alamat = $postan['password'];
        if($this->form_validation->run()){
            if ($alamat !== $postan['cpassword']){
                echo "Password tidak sama dengan Konfirm password";
            } else {
                $this->insertMhs($nama, $alamat);
                redirect(site_url("materi"));
            }
        } else {
            echo validation_errors();
        }
    }

    public function masterUpdate()
    {
        $postan = $this->input->post();
        $nama = $postan['nama'];
        $alamat = $postan['alamat'];
        $telepon = $postan['telepon'];
        $temp = $this->updateMhs($postan['id'],$nama, $alamat, $telepon);
        redirect('tutor');
    }

    public function detail($id=-1)
    {
        if ($id>-1){
            $data['detail'] = $this->user->getDetail($id);
            $this->load->view('tutor/update', $data);
        } else {
            redirect('tutor');
        }
    }

    public function harvest($id, $username)
    {
        $this->user->harvest($id, $username);
        $detail = $this->user->getLagi($username);
        $data['detail'] =$detail;
        $data['plot'] = $this->user->getAllPlot();
        $this->main($data);
    }
}
