<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tutor extends CI_Controller {

    public function __construct(Type $var = null)
    {
        parent::__construct();
        $this->load->model('mahasiswa');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

	public function index()
	{
        $data['listmhs'] = $this->mahasiswa->getAllMhs();
        $this->load->view('tutor/index.php', $data);
    }
    
    private function insertMhs($nama = "", $alamat = "", $telepon = "")
    {
        $this->mahasiswa->insertMhs($nama, $alamat, $telepon);
    }

    private function updateMhs($id, $nama = "", $alamat = "", $telepon = "")
    {
        if (isset($id)){
            $hasil = $this->mahasiswa->updateMhs($id, $nama, $alamat, $telepon);
            var_dump($hasil);
        }
    }

    public function masterInsert()
    {
        $postan = $this->input->post();
        $nama = $postan['nama'];
        $alamat = $postan['alamat'];
        $telepon = $postan['telepon'];
        $this->insertMhs($nama, $alamat, $telepon);
        redirect('tutor');
    }
   

    public function masterUpdate()
    {
        $postan = $this->input->post();
        $nama = $postan['nama'];
        $alamat = $postan['alamat'];
        $telepon = $postan['telepon'];
        $temp = $this->updateMhs($postan['id'],$nama, $alamat, $telepon);
        redirect('tutor');
    }

    public function detail($id=-1)
    {
        if ($id>-1){
            $data['detail'] = $this->mahasiswa->getDetail($id);
            $this->load->view('tutor/update', $data);
        } else {
            redirect('tutor');
        }
    }
}
