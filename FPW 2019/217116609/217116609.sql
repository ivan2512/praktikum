-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2019 at 05:28 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `217116609`
--
CREATE DATABASE IF NOT EXISTS `217116609` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `217116609`;

-- --------------------------------------------------------

--
-- Table structure for table `dtrans`
--

DROP TABLE IF EXISTS `dtrans`;
CREATE TABLE `dtrans` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dtrans`
--

INSERT INTO `dtrans` (`id`, `nama_barang`, `harga_satuan`, `jumlah_pesanan`) VALUES
(1, 'makanan m3', 10100, 3),
(1, 'minuman m3', 450, 23),
(2, 'makanan m3', 10100, 3),
(2, 'minuman m3', 450, 23),
(3, 'makanan m2 1', 100000, 1),
(4, 'makanan m2 1', 100000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `htrans`
--

DROP TABLE IF EXISTS `htrans`;
CREATE TABLE `htrans` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `merchant` varchar(255) NOT NULL,
  `total` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `dari` varchar(255) NOT NULL,
  `ke` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `htrans`
--

INSERT INTO `htrans` (`id`, `email`, `merchant`, `total`, `dari`, `ke`) VALUES
(1, 'user@user.com', 'm3@merchant.com', 40650, 'alamat m3 1', 'Alamat User'),
(2, 'user@user.com', 'm3@merchant.com', 40650, 'alamat m3 1', 'Alamat User'),
(3, 'user@user.com', 'm2@merchant.com', 100000, 'alamat 3', 'Alamat User'),
(4, 'user@user.com', 'm2@merchant.com', 210000, 'alamat 4', 'Alamat User');

-- --------------------------------------------------------

--
-- Table structure for table `menu_makanan`
--

DROP TABLE IF EXISTS `menu_makanan`;
CREATE TABLE `menu_makanan` (
  `email` varchar(255) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `status_barang` int(1) DEFAULT 1,
  `harga_barang` int(11) NOT NULL,
  `deskripsi_barang` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_makanan`
--

INSERT INTO `menu_makanan` (`email`, `nama_barang`, `status_barang`, `harga_barang`, `deskripsi_barang`) VALUES
('m2@merchant.com', 'coba lagi', 0, 123455, 'asdfvbn1'),
('m2@merchant.com', 'makanan m2 1', 1, 100000, 'deskripsi makanan '),
('m3@merchant.com', 'makanan m3', 1, 10100, 'deskripsi'),
('m3@merchant.com', 'minuman m3', 1, 450, 'deskripsi minuman');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `status` int(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `saldo` int(11) NOT NULL DEFAULT 0,
  `isMerchant` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`status`, `email`, `password`, `nama`, `telepon`, `saldo`, `isMerchant`) VALUES
(0, 'm1@merchant.com', 'm1', 'm1', '08123123123', 0, 1),
(0, 'm2@merchant.com', 'm2', 'm2', '0878789876', 0, 1),
(0, 'm3@merchant.com', 'm3', 'm3', '01231451652', 0, 1),
(0, 'user@user.com', 'user', 'User Ivan Marcellino', '081216282788', 780001, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_alamat`
--

DROP TABLE IF EXISTS `user_alamat`;
CREATE TABLE `user_alamat` (
  `email` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_alamat`
--

INSERT INTO `user_alamat` (`email`, `alamat`, `tag`) VALUES
('m1@merchant.com', 'alamat 1', 'al 1'),
('m1@merchant.com', 'alamat 2', 'al 1'),
('m2@merchant.com', 'alamat 3', 'al 3'),
('m2@merchant.com', 'alamat 4', 'al 4'),
('m3@merchant.com', 'alamat m3 1', 'm31'),
('m3@merchant.com', 'alamat m3 2', 'm32'),
('user@user.com', 'Alamat User', 'Rumah');

-- --------------------------------------------------------

--
-- Table structure for table `user_basket`
--

DROP TABLE IF EXISTS `user_basket`;
CREATE TABLE `user_basket` (
  `email` varchar(255) NOT NULL,
  `merchant` varchar(255) NOT NULL,
  `dari` varchar(255) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dtrans`
--
ALTER TABLE `dtrans`
  ADD PRIMARY KEY (`id`,`nama_barang`);

--
-- Indexes for table `htrans`
--
ALTER TABLE `htrans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_makanan`
--
ALTER TABLE `menu_makanan`
  ADD PRIMARY KEY (`email`,`nama_barang`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `user_alamat`
--
ALTER TABLE `user_alamat`
  ADD PRIMARY KEY (`email`,`alamat`,`tag`);

--
-- Indexes for table `user_basket`
--
ALTER TABLE `user_basket`
  ADD PRIMARY KEY (`email`,`merchant`,`dari`,`nama_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `htrans`
--
ALTER TABLE `htrans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
