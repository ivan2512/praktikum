<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class modelUser extends CI_Model{
    public function __contruct() {
      parent::__contruct();
      $this->load->database('praktikum');
    }

    public function checkEmail($email) {
      $this->db->query("SELECT email from `user` where `email` = '$email'");
      return $this->db->affected_rows()==0;
    }

    public function register($userbaru) {
      $userbaru['isMerchant'] = ($userbaru['role']) === 'merchant';
      $this->db->query("INSERT INTO `user` (`email`, `password`, `telepon`, `isMerchant`, `nama`) values ('$userbaru[email]', '$userbaru[password]', '$userbaru[notelp]', '$userbaru[isMerchant]', '$userbaru[nama]')");
      $alamat = $userbaru['alamat']; $kategori = $userbaru['kategori'];
      for ($i=0; $i < count($userbaru['alamat']); $i++) { 
        $this->db->query("INSERT INTO `user_alamat` (`email`, `alamat`, `tag`) values ('$userbaru[email]', '$alamat[$i]', '$kategori[$i]')");
      }

      return $this->db->affected_rows();
    }

    public function login($email) {
      $this->db->query("update `user` set status = 1 where `email` = '$email'");
      return $this->getDetails($email);
    }

    public function logout($email)
    {
      $this->db->query("update `user` set status = 0 where `email` = '$email'");
      $this->db->query("update `user` set status = 0");
    }

    public function getPassword($email)
    {
      return $this->db->query("SELECT password from `user` where `email` = '$email'")->row()->password;
    }

    public function getRole($email)
    {
      return $this->db->query("SELECT isMerchant from `user` where `email` = '$email'")->row()->isMerchant;
    }

    public function getDetails($email)
    {
      return $this->db->query("select * from `user` where `email` = '$email'")->row();
    }

    public function getActiveUser()
    {
      return $this->db->query("select * from `user` where `status` = '1'")->row(); 
    }

    public function getAlamat($email)
    {
      $hasil = $this->db->query("select alamat from user_alamat where `email` = '$email'")->result();
      $temp = [];
      foreach ($hasil as $key => $value) {
        $temp[] = $value->alamat;
      }
      return $temp;
    }
    public function getKategori($email)
    {
      $hasil = $this->db->query("select tag from user_alamat where `email` = '$email'")->result();
      $temp = [];
      foreach ($hasil as $value) {
        $temp[] = $value->tag;
      }
      return $temp;
    }
//tambahan mulai dari sini week 4
    public function TopUp($email, $nominal)
    {
      $saldoAwal = $this->db->select('saldo')
        ->where('email', $email)
        ->get('user')
        ->row()->saldo;
      $nominal += $saldoAwal;
      $this->db->set('saldo', $nominal);
      $this->db->where('email', $email);
      $this->db->update('user');
    }

    public function getActiveMerchant()
    {
      $t = $this->db->distinct()->select('user.nama, user.email')->from('user')->join('menu_makanan', 'menu_makanan.email = user.email')->where('isMerchant', 1)->where('status_barang', 1)->get();
      $t = $t->result_array();
      for ($i=0; $i < count($t); $i++) { 
        $email = $t[$i]['email'];
        $t[$i]['alamat'] = $this->getAlamat($email);
      }
      return $t;
    }

    public function getActiveMenu($email)
    {
      return $this->db->where('email', $email)->where('status_barang', 1)->get('menu_makanan')->result_array();
    }

    public function add($dataArray)
    {
      $this->db->where('email', $dataArray['pemesan'])->where('merchant', $dataArray['merchant']);
      $temp = $this->db->where('dari', $dataArray['dari'])->where('nama_barang', $dataArray['makanan'])->get('user_basket')->row();
      if (isset($temp)){
        //query Update
        $qty = $temp->jumlah_pesanan + $dataArray['qty'];
        $this->db->set('jumlah_pesanan', $qty);
        $this->db->where('email', $dataArray['pemesan'])->where('merchant', $dataArray['merchant']);
        $this->db->where('dari', $dataArray['dari'])->where('nama_barang', $dataArray['makanan']);
        $this->db->update('user_basket');
      } else {
        $d['email'] = $dataArray['pemesan'];
        $d['merchant'] = $dataArray['merchant'];
        $d['nama_barang'] = $dataArray['makanan'];
        $d['jumlah_pesanan'] = $dataArray['qty'];
        $d['dari'] = $dataArray['dari'];
        $this->db->insert('user_basket', $d);
      }
    }

    public function getBasket($where)
    {
      return $this->db->select('user_basket.*, menu_makanan.harga_barang')->where($where)->from('user_basket')->join('menu_makanan', 'user_basket.nama_barang = menu_makanan.nama_barang and user_basket.merchant=menu_makanan.email')->get()->result();
    }

    public function doTransaction($arr)
    {
      $data = ['id'=>"", 
        'email'=>$arr['email'], 
        'merchant'=>$arr['merchant'], 
        'total'=>$arr['total'], 
        'dari'=>$arr['dari'], 
        'ke'=>$arr['tujuan']
      ];
      $this->db->insert('htrans', $data);
      $id = $this->db->select_max('id')->get('htrans')->row()->id;
      for ($i=0; $i < count($arr['nmakanan']); $i++) { 
        $rows= [];
        $rows['id'] = $id;
        $rows['nama_barang'] = $arr['nmakanan'][$i];
        $rows['harga_satuan'] = $arr['subtotal'][$i] / $arr['jmakanan'][$i];
        $rows['jumlah_pesanan'] = $arr["jmakanan"][$i];
        $this->db->insert('dtrans', $rows);
      }
      $this->TopUp($arr['email'], -1* ($arr['total'] + 10000));
      $this->db->where('email', $arr['email'])->where('merchant',$arr['merchant'])->where('dari', $arr['dari']);
      $this->db->delete('user_basket');
    }

    public function getAllTransactions($email)
    {
      $hasil = $this->db->select('user.nama as nama_merchant, htrans.*')->from('htrans')->join('user', 'user.email = htrans.merchant')->where('htrans.email', $email)->get()->result();
      foreach ($hasil as $value) {
        $value->detail = $this->db->where('id', $value->id)->get('dtrans')->result_array();
      }
      return $hasil;
    }
  }
?>