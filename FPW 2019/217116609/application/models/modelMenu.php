<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class modelMenu extends CI_Model{
    public function __contruct() {
      parent::__contruct();
      $this->load->database('praktikum');
    }

    public function selectAllMenu($email)
    {
      return $this->db->query("SELECT * FROM `menu_makanan` where `email` = '$email' and `status_barang` > -1")->result();
    }
    
    public function insertNewMenu($email, $namamakanan, $hargamakanan, $deskripsimakanan)
    {
      
      $this->db->query("INSERT INTO `menu_makanan` ( `email`, `nama_barang`, `harga_barang`, `deskripsi_barang`) values ('$email', '$namamakanan', '$hargamakanan', '$deskripsimakanan')");
      return true;  
    }
    public function updateMenu($email, $namalama, $namamakanan, $harga, $deskripsi)
    {
      if ($namalama !== $namamakanan){
        $hasil = $this->db->query("SELECT count(*) as hasil from `menu_makanan` where `email` = '$email' and `nama_barang` = '$namamakanan'")->row()->hasil;
        if ($hasil>0) return false;
      }
      $this->db->query("UPDATE `menu_makanan` set `nama_barang` = '$namamakanan', `harga_barang` = '$harga', `deskripsi_barang`='$deskripsi' where `nama_barang` = '$namalama' and `email` = '$email'");
      return true;
    }

    public function disable($email, $namamakanan)
    {
      $this->db->query("UPDATE `menu_makanan` set `status_barang` = 0 where `email` = '$email' and `nama_barang` = '$namamakanan'");
    }

    public function delete($email, $namamakanan)
    {
      $this->db->query("UPDATE `menu_makanan` set `status_barang` = -1 where `email` = '$email' and `nama_barang` = '$namamakanan'");
    }

    public function enable($email, $namamakanan)
    {
      $this->db->query("UPDATE `menu_makanan` set `status_barang` = 1 where `email` = '$email' and `nama_barang` = '$namamakanan'");
    }
  }
?>