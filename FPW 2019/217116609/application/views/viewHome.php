<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64">
      <div class="w3-panel w3-card-4">
        <?=form_open('week2/login').
        "<p>".form_submit('btnLogout', 'logout', ['class'=>'w3-btn w3-red'])."</p>"?>
        <?=form_hidden('email', $email)?>
        <?=form_close() ?>
        <div class='w3-row'><img src="<?=base_url("upload/image.jpg")?>" class="w3-round"><button class="w3-btn w3-disabled w3-hover-red">Change Picture</button></div>
        <div class="w3-row"><p class="w3-col s4">Nama Lengkap: </p><p class="w3-col s8"><?=$nama?></p></div>
        <div class="w3-row"><p class="w3-col s4">Alamat Email: </p><p class="w3-col s8"><?=$email?></p></div>
        <div class="w3-row"><p class="w3-col s4">Saldo: </p><p class="w3-col s8"><?=$saldo?></p></div>
        <div class="w3-row"><p class="w3-col s4">Password:</p><p class="w3-col s8 w3-btn w3-hover-red w3-disabled">Change Password</p></div>
        <div class="w3-row container">
          <table class="w3-table-all w3-hoverable">
            <tr><th>Alamat</th>
            <th>Kategori</th>
            </tr>
            <?php
              for ($i=0; $i<count($alamat); ++$i){
                ?>
                  <tr><td><?=$alamat[$i]?></td>
                  <td><?=$kategori[$i]?></td></tr>
                <?php
              }
            ?>
          </table>
        </div>

        <div class="w3-container"><h4>History Transaksi</h4>
          <?php
            foreach ($history as $value) {?>
              <div class="w3-row w3-border w3-padding">
                <p class="w3-large"><?=$value->nama_merchant?></p>
                <p>Pengiriman dari : <?=$value->dari?></p>
                <p>Menuju : <?=$value->ke?></p>
                <table class="w3-table w3-striped">
                <?php
                  foreach ($value->detail as $rows) { ?>
                    <tr>
                      <td><?=$rows['nama_barang']?>  </td>
                      <td>x <?=$rows['jumlah_pesanan']?></td>
                      <td>Rp. <?=$rows['harga_satuan']*$rows['jumlah_pesanan']  ?></td>
                    </tr>
                 <?php }
                ?>
                  <tr>
                    <td>Ongkir</td>
                    <td></td>
                    <td>Rp. 10000</td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td></td>
                    <td>Rp. <?=$value->total + 10000?></td>
                  </tr>
                </table>
              </div>
              <?php 
            }
          ?>
        </div>
    </div>
  </div>
</div>

<script>
  document.addEventListener('DOMContentLoaded', ()=>{
    var formPesanMakan = document.createElement('form');
    formPesanMakan.setAttribute('method', 'post');
    formPesanMakan.setAttribute('action', '<?=site_url('week4/dashboard')?>');
    var btnPesan = document.createElement('input');
    btnPesan.setAttribute('type', 'submit');
    btnPesan.setAttribute('name', 'submit');
    btnPesan.setAttribute('value', 'Pesan');
    btnPesan.setAttribute('class',"w3-bar-item w3-button w3-hover-black");

    formPesanMakan.appendChild(btnPesan);
    document.querySelector('#mySidebar').appendChild(formPesanMakan);
  });
</script>