<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64">
    <div class="w3-panel w3-card">
      <?=form_open('week2/login').
      "<p>".form_submit('btnLogout', 'logout', ['class'=>'w3-btn w3-red'])."</p>"?>
      <?=form_hidden('email', $email)?>
      <?=form_close() ?>
      <div class='w3-row'><img src="<?=base_url("upload/image.jpg")?>" class="w3-round"><button class="w3-btn w3-disabled w3-hover-red">Change Picture</button></div>
      <div class="w3-row"><p class="w3-col s4">Nama Lengkap: </p><p class="w3-col s8"><?=$nama?></p></div>
      <div class="w3-row"><p class="w3-col s4">Alamat Email: </p><p class="w3-col s8"><?=$email?></p></div>
      <div class="w3-row"><p class="w3-col s4">Saldo: </p><p class="w3-col s8"><?=$saldo?></p></div>
      <div class="w3-row"><p class="w3-col s4">Password:</p><p class="w3-col s8 w3-btn w3-hover-red w3-disabled">Change Password</p></div>
      <div class="w3-row container">
        <table class="w3-table-all w3-hoverable">
          <tr><th>Alamat</th>
          <th>Kategori</th>
          </tr>
          <?php
            for ($i=0; $i<count($alamat); ++$i){
              ?>
                <tr><td><?=$alamat[$i]?></td>
                <td><?=$kategori[$i]?></td></tr>
              <?php
            }
          ?>
        </table>
      </div>
    </div>
    <div class="w3-panel w3-card">
      <?php
      echo "<br>".validation_errors()."<br>";
      if (isset($pesanError)) echo $pesanError;
      echo form_open('week3/tambahMenu');
      echo form_hidden('email', $email);
      echo "<p>".form_input('makanan', '', ['class'=>'w3-input', 'placeholder'=>'Nama Makanan'])."</p>";
      echo "<p>".form_input('harga', '', ['class'=>'w3-input', 'placeholder'=>'Harga'])."</p>";
      echo "<p>".form_textarea('deskripsi', '', ['class'=>'w3-input', 'placeholder'=>'Deskripsi'])."</p>";
      echo "<p>".form_submit('btnAdd', 'Buat Menu Baru', ['class'=>'w3-btn w3-teal'])."</p>";
      echo form_close();
      ?>
    </div>
    <div class="w3-panel w3-card">
      <table class='w3-table'>
        <thead>
          <th>No</th>
          <th>Menu</th>
          <th>Harga</th>
          <th>Action</th>
        </thead>
        <tbody>
<?php
  $datamakanan = $menu;
  $i = 0;
  foreach ($menu as $value) {
    $i++;
    ?>
      <tr>
        <td><?=$i?></td>
        <td><?=$value->nama_barang?></td>
        <td><?=$value->harga_barang?></td>
        <td>
          <form action="<?=site_url('week3/gantiStatus')?>" method="post">
<?php
  $str = json_encode($value);
  $arr = []; $arr['id'] = "menu$i";
  $arr['value'] = $str;
  $arr['hidden'] = 'hidden';
  echo form_input($arr);
  echo form_hidden('email', $email);
  echo form_hidden('namabarang', $value->nama_barang);
?>
           <button class="w3-btn w3-teal" type="button" onclick="edit('<?=$email?>', '<?=$value->nama_barang?>', '<?='menu'.$i?>')">Edit</button>
          <?php if($value->status_barang === '1') {
            ?><input type="submit" value="Disable" name="btnDisable" class="w3-btn w3-yellow"><?php
          } else {?>
            <input type="submit" value="Enable" name="btnEnable" class="w3-btn w3-yellow">
          <?php
          }
          ?>
           <input type="button" value="Delete" name="btnDelete" class="w3-btn w3-red" onclick="hapus('<?=$email?>', '<?=$value->nama_barang?>', '<?='menu'.$i?>')">
          </form>
        </td>
      </tr>
    <?php
  }
?>  
        </tbody>
      </table>
    </div>
  </div>
</div>

<div id="modalEdit" class="w3-modal">
  <div class="w3-modal-content">
    <div class="w3-container">
      <br><br>
      <span onclick="document.getElementById('modalEdit').style.display='none'" class="w3-button w3-display-topright">&times;</span>
      <?=form_open('week3/update')?>
      <p><input type="text" name="nama_barang" id="editNama" class="w3-input"></p>
      <p><input type="text" name="harga_barang" id="editHarga" class="w3-input"></p>
      <?="<p>".form_textarea('deskripsi', '', ['class'=>'w3-input', 'id'=>'editDeskripsi'])."</p>"?>
      <p><input type="submit" value="Update" name="btnUpdate" class="w3-btn w3-yellow"></p>
      <?=form_hidden('email', $email)?>
      <input type="hidden" name="namalama" id='editNamaLama'>
      <?=form_close()?>
    </div>
  </div>
</div>

<div id="modalDelete" class="w3-modal">
  <div class="w3-modal-content">
    <div class="w3-container">
      <br><br>
      <span onclick="document.getElementById('modalDelete').style.display='none'" class="w3-button w3-display-topright">&times;</span>
      <h3 class="w3-red">APAKAH ANDA YAKIN INGIN MENGHAPUS MENU INI?</h3>
      <?=form_open('week3/update')?>
      <p><input type="text" name="nama_barang" id="deleteNama" readonly class="w3-input"></p>
      <p><input type="text" name="harga_barang" id="deleteHarga" readonly class="w3-input"></p>
      <?="<p>".form_textarea('deskripsi', '', ['class'=>'w3-input', 'id'=>'deleteDeskripsi', "readonly"=>'readonly'])."</p>"?>
      <p><input type="submit" value="Delete" name="btnDelete" class="w3-btn w3-input w3-red"></p>
      <?=form_hidden('email', $email)?>
      <?=form_close()?>
    </div>
  </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', ()=>{
        document.querySelector('#mySidebar').removeChild(document.querySelector('#btnMasuk'));
        var formPesanMakan = document.createElement('form');
        formPesanMakan.setAttribute('method', 'post');
        formPesanMakan.setAttribute('action', '<?=site_url('week2/pesan')?>');
        var btnPesan = document.createElement('input');
        btnPesan.setAttribute('type', 'submit');
        btnPesan.setAttribute('name', 'submit');
        btnPesan.setAttribute('value', 'Pesan');
        btnPesan.setAttribute('disabled', 'true');
        btnPesan.setAttribute('class',"w3-bar-item w3-button w3-hover-black w3-disabled");

        formPesanMakan.appendChild(btnPesan);
        document.querySelector('#mySidebar').appendChild(formPesanMakan);
    });

  function edit(email, menu, id) {
    document.querySelector('#modalEdit').style.display = 'block';
    document.querySelector('#editNama').value = menu;
    document.querySelector('#editNamaLama').value = menu;
    let temp = JSON.parse(document.querySelector('#'+id).value);
    console.log(temp);
    document.querySelector('#editHarga').value = temp['harga_barang'];
    document.querySelector('#editDeskripsi').value = temp['deskripsi_barang'];
  }

  function hapus(email, menu, id) {
    document.querySelector('#modalDelete').style.display = 'block';
    document.querySelector('#deleteNama').value = menu;
    let temp = JSON.parse(document.querySelector('#'+id).value);
    console.log(temp);
    document.querySelector('#deleteHarga').value = temp['harga_barang'];
    document.querySelector('#deleteDeskripsi').value = temp['deskripsi_barang'];
  }
</script>