<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64 w3-row-padding">
    <div class="w3-card w3-col s4 w3-padding-16 w3-content">
      <h2>Makanan dan Minuman</h2>
      <div class="w3-display-container w3-container"><p><button class="w3-display-middle w3-btn w3-hover-green" id='pesanMakanan'>Pesan</button></p></div>
    </div>
    <div class="w3-card w3-col s4 w3-padding-16">
      <h2>Taksi dan Ojek</h2>
      <div class="w3-display-container w3-container"><p><button class="w3-display-middle w3-btn w3-hover-green">Pesan</button></p></div>
    </div>
    <div class="w3-card w3-col s4 w3-padding-16">
      <h2>Riwayat</h2>
      <div class="w3-display-container w3-container"><p><button class="w3-display-middle w3-btn w3-hover-green">Pesan</button></p></div>
    </div>
  </div>
</div>

<div id="id01" class="w3-modal">
  <div class="w3-modal-content">
    <div class="w3-container w3-padding-16">
      <span onclick="document.getElementById('id01').style.display='none'"
      class="w3-button w3-display-topright">&times;</span>
      <form action="#" method="post">
        <input type="text" name="resto" class='w3-input' placeholder="Nama Resto">
        <button type="button" class='w3-btn w3-red w3-hover-pink' onclick='add()'>Add...</button>
        <div id='list'>
          <div class="w3-row"><input type="text" placeholder='Menu' name="menu[]" class='w3-input w3-col s8'> <input type="number" placeholder='Jumlah' name="jumlah[]" class='w3-input w3-col s4'></div>
        </div>
        <button type="submit" class='w3-btn w3-teal'>Pesan!</button>
      </form>
    </div>
  </div>
</div>

<script>
  document.addEventListener('DOMContentLoaded', function(){
    document.querySelector('#pesanMakanan').addEventListener(
      'click', ()=>{
        document.querySelector('#id01').style.display='block';
      }
    )
  });
</script>