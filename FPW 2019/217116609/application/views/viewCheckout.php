<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64">
    <div class="w3-panel w3-card-4">
      <h1>Pembayaran</h1>
      <?=form_open('week4/final')?>
      <p>Kirim ke: <select name="tujuan" class="w3-select">
        <?php
          for ($i=0; $i < count($user->alamat); $i++) { 
            $value = $user->alamat[$i];
            $kategori = $user->kategori[$i];
            ?><option value="<?=$value?>"><?=$kategori?></option><?php
          }
        ?>
      </select></p>
      <p>Saldo anda: <?=$user->saldo?></p>
      <h2>Pesanan Anda: </h2>
      <table class="w3-table">
      <?php
        $total = 0;
        foreach ($basket as $items) {
          $sementara = $items->jumlah_pesanan*$items->harga_barang;
          $total += $sementara; ?>
          <tr>
            <td><span class="w3-input"><?=$items->nama_barang?></span></td>
            <td><span class="w3-input">@ <?=$items->harga_barang?></span></td>
            <td><input type="number" name="jmakanan[]" id="<?=str_replace(' ', '_',$items->nama_barang)?>" class="w3-input" value="<?=$items->jumlah_pesanan?>"></td>
            <td><input class="w3-input subtotal" type="number" name="subtotal[]" id="subtotal_<?=str_replace(' ', '_',$items->nama_barang)?>" readonly value="<?=$sementara?>"></td>
            <td>
              <input type="button" value="-" class="w3-btn w3-yellow" onclick="minus('<?=str_replace(' ', '_',$items->nama_barang)?>')">
              <input type="button" value="+" class="w3-btn w3-cyan" onclick="plus('<?=str_replace(' ', '_',$items->nama_barang)?>')">
            </td>
            <input type="hidden" name="nmakanan[]" value="<?=$items->nama_barang?>">
            <input type="hidden" id="harga_<?=str_replace(' ', '_',$items->nama_barang)?>" value="<?=$items->harga_barang?>">
          </tr>
  <?php }
      ?>
      <tr>
        <td colspan="3" class="w3-right-align">Ongkos Kirim</td>
        <td>10000</td>
        <td></td>
      </tr>
      <tr>
        <td colspan="3" class="w3-right-align">Total belanja anda: </td>
        <td><input type="number" name="total" id="total" class="w3-input" value="<?=$total+10000?>"></td>
        <td></td>
      </tr>
      </table>
      <input type="hidden" name="email" value="<?=$user->email?>">
      <input type="hidden" name="merchant" value="<?=$merchant?>">
      <input type="hidden" name="dari" value="<?=$dari?>">
      <input type="hidden" name="saldo" value="<?=$user->saldo?>">
      <p><input type="submit" value="Pay Now" name="btnPay" class="w3-btn w3-green"></p>
      <?=form_close()?>
    </div>
  </div>
</div>
<script>
  function minus(params) {
    var temp = document.querySelector("#"+params);
    var harga = document.querySelector("#harga_"+params);
    if (parseInt(temp.value) > 0){
      temp.value -= 1;
      document.querySelector('#subtotal_'+params).value = temp.value * harga.value;
      countTotal();
    }
  }

  function countTotal() {
    var total = document.querySelector("#total");
    let t = 0; 
    var subs = document.querySelectorAll(".subtotal");
    subs.forEach(element => {
      t += parseInt(element.value);
    });
    total.value = t+10000;
  }

  function plus(params) {
    var temp = document.querySelector("#"+params);
    var harga = document.querySelector("#harga_"+params);
    temp.value = parseInt(temp.value)+1;
    document.querySelector('#subtotal_'+params).value = temp.value * harga.value;
    countTotal();
  }
</script>