<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64">
    <div class="w3-panel w3-card-4">
      <h1>Hello, <a href="<?=site_url('week2/profile')?>"><?=$user->nama?></a>  ! </h1>
      <?php
        if (isset($err)){
          echo "<h3 class='w3-yellow'>$err</h3>";
        }
      ?>

      <?=form_open('week2/login').
      "<p>".form_submit('btnLogout', 'logout', ['class'=>'w3-btn w3-red'])."</p>"?>
      <?=form_hidden('email', $email)?>
      <?=form_close()?>
      <?=form_open('week4/topup')?>
      <p>Your Balance: <?=$user->saldo?> <?=form_submit('btnTopUp', 'Top Up', ['class'=>'w3-btn w3-teal'])?></p>
      <?=form_hidden('email', $email)?>
      <?=form_close()?>

      <h1>Order makanan disekitarmu</h1>
      <?php
        foreach ($merchants as $m) {
      ?>
      <div class="w3-container w3-padding-16">
        <?=form_open('week4/explore')?>
        <ul class="w3-ul w3-hoverable"><?="<h4>".$m['nama']."</h4>"?>
        <?php foreach ($m['alamat'] as $value) {
          echo "<li>".$value."</li>";
        }?>
        </ul>
        <?=form_submit('bExplore', 'Telusuri', ['class'=>'w3-btn w3-black']);?>
        <?=form_hidden('email', $email)?>
        <?=form_hidden('emailMerchant', $m['email'])?>
        <?=form_close()?>
      </div>
      <?php
        }
      ?>
    </div>
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', ()=>{
      document.querySelector('#mySidebar').removeChild(document.querySelector('#btnMasuk'));
      var formPesanMakan = document.createElement('form');
      formPesanMakan.setAttribute('method', 'post');
      formPesanMakan.setAttribute('action', '<?=site_url('week2/profile')?>');
      var btnPesan = document.createElement('input');
      btnPesan.setAttribute('type', 'submit');
      btnPesan.setAttribute('name', 'submit');
      btnPesan.setAttribute('value', 'Profile');
      btnPesan.setAttribute('class',"w3-bar-item w3-button w3-hover-black");

      formPesanMakan.appendChild(btnPesan);
      document.querySelector('#mySidebar').appendChild(formPesanMakan);
  });
</script>