<div class="w3-main" style="margin-left:250px">
  <div class="w3-row w3-padding-64">
    <div class="w3-container">
      <div class="w3-panel w3-card-4">
        <div class="w3-text-red">
          <?php
            if (isset($error) && $error)
              echo validation_errors(); 
          ?>
        </div>
        <?=form_open(site_url('week2/register'))?>
        <p><?=form_input('nama', '', ['placeholder'=>"Nama Lengkap", 'class'=>"w3-input"])?></p>
        <div id="appendAlamat" class="w3-row">
          <div class="w3-col s9"><?=form_input('alamat[]', '', ['class'=>'w3-input', 'placeholder'=>'Alamat'])?></div>
          <div class="w3-col s2"><?=form_input('tag[]', '', ['class'=>'w3-input', 'placeholder'=>'Tag'])?></div>
          <div class="w3-col s1"><button type="button" class="w3-btn w3-pale-green" onclick="tambahAlamat(this)">Add</button></div>
        </div>
        <p><?=form_input('notelp', '', ['class'=>'w3-input', 'placeholder'=>'Nomor Telpon'])?></p>
        <p><?=form_input('email', "", ["class"=>'w3-input', 'placeholder'=>'Alamat Email'])?></p>
        <p><?=form_password('password', "", ['class'=>'w3-input', 'placeholder'=>'Password'])?></p>
        <p><?=form_password('cpassword', '', ['placeholder'=>'Konfirmasi Password', 'class'=>'w3-input'])?></p>
        <p><?=form_radio('role', 'merchant', false, ['class'=>'w3-check'])."Merchant"?></p>
        <p><?=form_radio('role', 'customer', true, ['class'=>'w3-check'])."Customer"?></p>
        <p class="w3-row">
        <?=form_checkbox('notRobot', 'not a robot', false, ['class'=>'w3-check w3-col s1']).form_label('Not A Robot', '', ['class'=>'w3-input w3-col s11'])?></p>
        <p><input type="submit" value="Register" name='btnRegis' class="w3-btn w3-red"></p>
        <?=form_close()?>

        <?=form_open(site_url('week2/login'))?>
            <p><input type="submit" value="Login" name='toLogin' class="w3-teal w3-btn"></p>
        <?=form_close()?>
      </div>
</div></div></div>