<!DOCTYPE html><html lang="en">
<head><title>Tugas Praktikum FAI</title>
<link rel="stylesheet" href="<?=base_url('css/w3s.css')?>">
<style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}
</style>
<body>
<div class="w3-top">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()">MENU</a>
    <a href="<?=base_url()?>" class="w3-bar-item w3-button w3-theme-l1">Angkutan Online</a>
  </div>
</div>
<!-- Sidebar -->
<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <!-- <i class="fa fa-remove"></i> -->
    X
  </a>
  <h4 class="w3-bar-item"><b>Angkutan Online</b></h4>
    <a class="w3-bar-item w3-button w3-hover-black" href="#">Tentang Kami</a>
</nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<script>
  var mySidebar;
  var overlayBg 
document.addEventListener('DOMContentLoaded', ()=>{
  mySidebar = document.getElementById("mySidebar");
  overlayBg = document.getElementById("myOverlay");
});

function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>