<div class="w3-main" style="margin-left:250px">
  <div class="w3-container w3-padding-64">
    <div class="w3-panel w3-card-4">
      <h1><?=$merchant->nama?></h1>
      Pilih restaurant
      <p><select id='source' class="w3-select" name="alamat" onchange="updateAlamatField()">
        <?php
          foreach ($merchant->alamat as $value) {?>
          <option value="<?=$value?>"><?=$value?></option>  
    <?php }
        ?>
      </select></p>
      
      <?=form_open('week4/checkout')?>
      <input type="hidden" name="dari" class="dari">
      <input type="hidden" name='email' value="<?=$cUser->email?>">
      <input type="hidden" name='merchant' value="<?=$merchant->email?>">
      <input type="submit" value="Check Out" class="w3-btn w3-red">
      <?=form_close()?>

      <?php foreach ($merchant->menu as $value) { ?>
      <div class="w3-card w3-panel">  
        <?=form_open('week4/addtocart')?>
        <p><?=$value['nama_barang']?></p>
        <p>Harga Satuan: Rp. <?=$value['harga_barang']?></p>
        <p><input type="number" name="inpQty" placeholder="Jumlah (min 1)" class="w3-input" min="1" required></p>
        <p><input class="w3-btn w3-teal" type="submit" value="Add to Basket" name="bAdd"></p>
        <?=form_hidden('nama_makanan', $value['nama_barang'])?>
        <?=form_hidden('merchant', $merchant->email)?>
        <?=form_hidden('email', $cUser->email)?>
        <input type="hidden" name="alamat" class="dari">
        <?=form_close()?>
      </div>
    <?php  } ?>
    </div>
  </div>
</div>

<script>
  function updateAlamatField() {
    var nodes = document.querySelectorAll('.dari');
    nodes.forEach(element => {
      element.value = document.querySelector('#source').value;
    });
  }

  document.addEventListener('DOMContentLoaded', ()=>{
    var elemenForm = document.createElement('form');
    elemenForm.setAttribute('method', 'post');
    elemenForm.setAttribute('action', '<?=site_url('week4/dashboard')?>');
    var btnPesan = document.createElement('input');
    btnPesan.setAttribute('type', 'submit');
    btnPesan.setAttribute('name', 'submit');
    btnPesan.setAttribute('value', 'Home');
    btnPesan.setAttribute('class',"w3-bar-item w3-button w3-hover-black");

    elemenForm.appendChild(btnPesan);
    document.querySelector('#mySidebar').appendChild(elemenForm);

    updateAlamatField();
  });
</script>