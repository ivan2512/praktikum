<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class week3 extends CI_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->view('viewHeader');
      $this->load->library('form_validation');
      $this->load->helper('form');
      $this->load->model('modelUser');
      $this->load->model('modelMenu');
    }
  
    public function profile()
    {
      //profile merchant
      $currentUser = $this->modelUser->getActiveUser();
      $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
      $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
      $currentUser->menu = $this->modelMenu->selectAllMenu($currentUser->email);
      $this->load->view('viewMerchant', $currentUser);
    }

    public function tambahMenu()
    {
      $this->form_validation->set_rules('makanan', 'Makanan', 'required');
      $this->form_validation->set_rules('harga', 'Harga Makanan', 'required|numeric');
      $this->form_validation->set_rules('deskripsi', 'Deskripsi Makanan', 'regex_match[/^([a-z 0-9])+$/i]');
      echo "<br><br><br>";
      if ($this->form_validation->run()){
        $namamakanan = $this->input->post('makanan');
        $hargamakanan = $this->input->post('harga');
        $deskripsimakanan = $this->input->post('deskripsi');
        $email = $this->input->post('email');
        $pesan = $this->modelMenu->insertNewMenu($email, $namamakanan, $hargamakanan, $deskripsimakanan);
        if ($pesan){
          redirect(site_url('week3/profile'));
        } else {
          $currentUser = $this->modelUser->getActiveUser();
          $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
          $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
          $currentUser->menu = $this->modelMenu->selectAllMenu($currentUser->email);
          $currentUser->pesanError = "Menu sudah pernah dimasukkan sebelumnya";
          $this->load->view('viewMerchant', $currentUser);
        }
      } else {
        $currentUser = $this->modelUser->getActiveUser();
        $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
        $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
        $currentUser->menu = $this->modelMenu->selectAllMenu($currentUser->email);
        $this->load->view('viewMerchant', $currentUser);
      }
    }

    public function gantiStatus(Type $var = null)
    {
      $inputan = $this->input->post();
      if (isset($inputan['btnDisable'])){
        $this->disable($inputan['email'], $inputan['namabarang']);
      } else if (isset($inputan['btnEnable'])){
        $this->enable($inputan['email'], $inputan['namabarang']);
      }
      redirect(site_url('week3/profile'));
    }

    public function disable($email, $namabarang)
    {
      $this->modelMenu->disable($email, $namabarang);
    }

    public function enable($email, $namabarang)
    {
      $this->modelMenu->enable($email, $namabarang);
    }

    public function delete($email, $namabarang)
    {
      $this->modelMenu->delete($email, $namabarang);      
    }

    public function update()
    {
      echo "<br><br><br>";
      $inputan = $this->input->post();
      if (isset($inputan['btnDelete'])){
        $this->delete($inputan['email'], $inputan['nama_barang']);
        redirect(site_url('week3/profile'));
      } else if (isset($inputan['btnUpdate'])){
        $this->form_validation->set_rules('nama_barang', 'Makanan', 'required');
        $this->form_validation->set_rules('harga_barang', 'Harga Makanan', 'required|numeric');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi Makanan', 'regex_match[/^([a-z ])+$/i]');
        if ($this->form_validation->run()){
          $berhasil = $this->modelMenu->updateMenu(
            $inputan['email'], 
            $inputan['namalama'], 
            $inputan['nama_barang'], 
            $inputan['harga_barang'],
            $inputan['deskripsi']
          );
          if (!$berhasil){
            $currentUser = $this->modelUser->getActiveUser();
            $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
            $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
            $currentUser->menu = $this->modelMenu->selectAllMenu($currentUser->email);
            $currentUser->pesanError = "Nama menu sudah pernah digunakan";
            $this->load->view('viewMerchant', $currentUser);
          } else {
            redirect(site_url('week3/profile'));
          }
        } else {
          $currentUser = $this->modelUser->getActiveUser();
          $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
          $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
          $currentUser->menu = $this->modelMenu->selectAllMenu($currentUser->email);
          $this->load->view('viewMerchant', $currentUser);
        }
      }
    }
  }
?>