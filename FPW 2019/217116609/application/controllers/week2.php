<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class week2 extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->view('viewHeader.php');
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->model('modelUser');
  }
  
	public function index()
	{
    $this->load->view('viewIndex');
    $this->load->view('viewFooter');
  }

  public function profile()
  {
    $currentUser = $this->modelUser->getActiveUser();
    $currentUser->alamat = $this->modelUser->getAlamat($currentUser->email);
    $currentUser->kategori = $this->modelUser->getKategori($currentUser->email);
    $currentUser->history = $this->modelUser->getAllTransactions($currentUser->email);
    $this->load->view('viewHome', $currentUser);
  }

  public function login() {
    $input = $this->input->post();
    $data = [];
    if (isset($input['btnLogin'])){
      $ada = !$this->modelUser->checkEmail($input['email']);
      if ($ada){
        $password = $this->modelUser->getPassword($input['email']);
        if ($password === $input['pass']){
          $role = $this->modelUser->getRole($input['email']);
          if ($role){ //jika merchant
            $this->modelUser->login($input['email']);
            redirect('week3/profile');
          } else { //jika bukan
            $this->modelUser->login($input['email']);
            //redirect('week2/profile');
            redirect('week4/dashboard');
          }
        } else {
          $this->load->view('viewLogin', $data);
        }
      } else {
        $this->load->view('viewLogin', $data);
      }
    } 
    else if (isset($input['btnLogout'])){
      $this->modelUser->logout($input['email']);
      $this->load->view('viewLogin', $data);
    }
    else {
        //akses url langsung / href dari sidebar
        $this->load->view('viewLogin', $data);
    }
  }

  public function debug(Type $var = null)
  {
    $this->modelUser->cobaKoneksi();
  }

  public function register(Type $var = null) {
    $input = $this->input->post();
    if (isset($input['btnRegis'])){
      $this->form_validation->set_rules('nama', 'Nama', 'required');
      $this->form_validation->set_rules('notelp', 'Nomor Telpon', 'numeric|min_length[10]|max_length[16]');
      $this->form_validation->set_rules('alamat', 'Alamat', 'callback_validate_alamat');
      $this->form_validation->set_rules('tag', 'Kategori Alamat', 'callback_validate_kategori');
      $this->form_validation->set_rules('email', 'Alamat Email', "required|valid_email|callback_unik_email");
      $this->form_validation->set_rules('password', 'Password', 'required');
      $this->form_validation->set_rules('cpassword', 'Konfirmasi Password', 'matches[password]');
      $this->form_validation->set_rules('notRobot', 'Cekbox Robot', 'required');
      if ($this->form_validation->run()){
        $user = [];
        $user['email'] = $input['email'];
        $user['password'] = $input['password'];
        $user['nama'] = $input['nama'];
        $user['alamat'] = $input['alamat'];
        $user['kategori'] = $input['tag'];
        $user['alamat'] = array_filter($user['alamat']);
        $user['kategori'] = array_filter($user['kategori']);
        $user['role'] = $input['role'];
        if (count($user['alamat']) !== count($user['kategori'])){
          $Jalamat = count($user['alamat']); $Jkategori = count($user['kategori']);
          if($Jalamat > $Jkategori){
            $user['alamat'] = array_slice($user['alamat'], 0, $Jkategori);
          } else {
            $user['kategori'] = array_slice($user['kategori'], 0, $Jkategori);
          }
        }
        $user['saldo'] = 0;
        $user['notelp'] = $input['notelp'];
        echo "<br><br><br><br>";
        if ($this->modelUser->register($user)){
          redirect('week2/login');
        }
      } else {
        $data['error'] = true;
        $this->load->view('viewRegister', $data);
      }
    } else {
      $this->load->view('viewRegister');
    }
  }

  public function unik_email() {
    $this->form_validation->set_message('unik_email', '{field} sudah pernah digunakan');
    return $this->modelUser->checkEmail($this->input->post('email'));
  }
    
  public function validate_alamat() {
    $alamat = $this->input->post('alamat');
    foreach ($alamat as $value) {
      if ($value !== "")
        return true;
    }
    $this->form_validation->set_message('validate_alamat', '{field} harus diisi');
    return false;
  }

  public function validate_kategori() {
    $alamat = $this->input->post('tag');
    foreach ($alamat as $value) {
      if ($value !== "")
        return true;
    }
    $this->form_validation->set_message('validate_kategori', '{field} harus diisi');
    return false;
  }
}
?>