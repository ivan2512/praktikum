<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class week4 extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->view('viewHeader.php');
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->model('modelUser');
    $this->load->model('modelMenu');
  }

  public function dashboard($err = "")
  {
    $data['user'] = $this->modelUser->getActiveUser();
    $data['email'] = $data['user']->email;
    $data['merchants'] = $this->modelUser->getActiveMerchant();
    if ($err===""){
    } else {
      if ($err==="err"){
        $data['err'] = "Saldo tidak cukup";
      } elseif ($err==="ok") {
        $data['err'] = "Transaksi berhasil";
      }
    }
    $this->load->view('homeUser', $data);
  }
  
  public function TopUp()
  {
    if ($this->input->post('bTopup')){
      $email = $this->input->post('email');
      $besar = $this->input->post('besar');
      $this->modelUser->TopUp($email, $besar);
      redirect('week4/dashboard');
    } else {
      $data['email'] = $this->input->post('email');
      $this->load->view('viewTopup', $data);
    }
  }

  public function explore($merchant = "")
  {
    if ($this->input->post('bExplore')){
      $merchant = $this->modelUser->getDetails($this->input->post('emailMerchant'));
      $merchant->menu = $this->modelUser->getActiveMenu($merchant->email);
      $merchant->alamat = $this->modelUser->getAlamat($merchant->email);

      $currentUser = $this->modelUser->getDetails($this->input->post('email'));
      $data['cUser'] = $currentUser;
      $data['merchant'] = $merchant;

      $this->load->view('viewOrder', $data);
    }
  }

  public function addtocart()
  {
    if ($this->input->post('bAdd')){
      $in = $this->input->post();
      $data = [];
      $data['pemesan'] = $in['email'];
      $data['merchant'] = $in['merchant'];
      $data['makanan'] = $in['nama_makanan'];
      $data['qty'] = $in['inpQty'];
      $data['dari'] = $in['alamat'];
      $this->modelUser->add($data);
      redirect('week4/dashboard');
    }
  }

  public function checkout(){
    $data['user'] = $this->modelUser->getActiveUser();
    $data['user']->alamat = $this->modelUser->getAlamat($data['user']->email);
    $data['user']->kategori = $this->modelUser->getKategori($data['user']->email);
    $t["user_basket.email"] = $this->input->post('email');
    $t['merchant'] = $this->input->post('merchant');
    $t['dari'] = $this->input->post('dari');
    $data['basket'] = $this->modelUser->getBasket($t);
    $data['merchant'] = $this->input->post('merchant');
    $data['dari'] = $this->input->post('dari');
    $this->load->view('viewCheckout', $data);
  }

  public function final()
  {
    $inp = $this->input->post();
    echo "<br><br><br>";
    for ($i=count($inp['jmakanan'])-1; $i >= 0; $i-=1) { 
      if ($inp['jmakanan'][$i] === '0'){
        array_splice($inp['nmakanan'],$i, 1);
        array_splice($inp['jmakanan'],$i, 1);
        array_splice($inp['subtotal'],$i, 1);
      }
    }
    if ($inp['saldo'] >= ($inp['total'] + 10000)){
      $this->modelUser->doTransaction($inp);
      redirect("week4/dashboard/ok");
    } else {
      redirect('week4/dashboard/err');
    }
  }
}
?>