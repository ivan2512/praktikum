<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(Type $var = null)
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = [];
		$this->form_validation->set_message('required', 'Fieldnya kok kosong');
		$this->form_validation->set_rules(
			'username', 'username', 'required'
		);
		$this->form_validation->set_rules(
			'password', 'password', 'required'
		);
		$this->form_validation->set_rules(
			'cpassword', 'password', 'matches[password]'
		);
		if ($this->input->post('btnSubmit')){
			if (!$this->form_validation->run()){
				$this->load->view('vLogin', 
					array('listusername'=>$this->input->post('listuser'), 'listpassword'=>$this->input->post('listpass')));
			} else {
				$data['listusername'] = $this->input->post('listuser');
				$data['listpassword'] = $this->input->post('listpass');
				$data['listusername'] .= $this->input->post('username')."|";
				$data['listpassword'] .= $this->input->post('password')."|";
				$this->load->view('vIndex', $data);
			}
		} 
		else if ($this->input->post('btnBack')){
			$data['listusername'] = $this->input->post('listuser');
			$data['listpassword'] = $this->input->post('listpass');
			$this->load->view('vLogin', $data);
		}
		else {
			$this->load->view('vLogin', array('listusername'=>null, 'listpassword'=>null));
		}
		
	}

}
?>