<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class materi extends CI_Controller {

	public function __construct(Type $var = null)
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data = [];
		// $this->form_validation->set_message('required', 'Fieldnya kok kosong');
		// $this->form_validation->set_rules(
		// 	'username', 'username', 'required'
		// );
		// $this->form_validation->set_rules(
		// 	'password', 'password', 'required'
		// );
		// $this->form_validation->set_rules(
		// 	'cpassword', 'password', 'matches[password]'
		// );
		// if ($this->input->post('btnSubmit')){
		// 	if (!$this->form_validation->run()){
		// 		$this->load->view('vLogin', 
		// 			array('listusername'=>$this->input->post('listuser'), 'listpassword'=>$this->input->post('listpass')));
		// 	} else {
		// 		$data['listusername'] = $this->input->post('listuser');
		// 		$data['listpassword'] = $this->input->post('listpass');
		// 		$data['listusername'] .= $this->input->post('username')."|";
		// 		$data['listpassword'] .= $this->input->post('password')."|";
		// 		$this->load->view('vIndex', $data);
		// 	}
		// } 
		// else if ($this->input->post('btnBack')){
		// 	$data['listusername'] = $this->input->post('listuser');
		// 	$data['listpassword'] = $this->input->post('listpass');
		// 	$this->load->view('vLogin', $data);
		// }
		// else {
		// 	$this->load->view('vLogin', array('listusername'=>null, 'listpassword'=>null));
        // }
        
        if ($this->input->post('regis')){
            $this->form_validation->set_rules('nama', 'Nama', 'required|alpha');
            $this->form_validation->set_rules('hp', 'Nama', 'required|numeric');
            $this->form_validation->set_rules('password', 'Nama', 'required|alpha_numeric|min_length[6]|max_length[10]');
            $this->form_validation->set_rules('cpassword', 'Nama', 'required|matches[password]');
            $data['listnama'] = $this->input->post('listnama');
            $data['listhp'] = $this->input->post('listhp');
            $data['listpassword'] = $this->input->post('listpassword');
            $data['liststatus'] = $this->input->post('liststatus');
            if (!$this->form_validation->run()){
                $this->load->view('register', $data);
                echo validation_errors();
            } else {
                $nohp = $this->input->post('hp');
                if (in_array($nohp, explode("|",$data['listhp']))){
                    echo "Nomor HP kembar";
                    $this->load->view('register', $data);

                } else {
                    $data['listnama'] .= $this->input->post('nama')."|";
                    $data['listhp'] .= $this->input->post('hp')."|";
                    $data['listpassword'] .= $this->input->post('password')."|";
                    $data['liststatus'] .= '1'."|";
                     $this->load->view('register', $data);

                }
            }
        } else if ($this->input->post('login')) {
            $data['listnama'] = $this->input->post('listnama');
            $data['listhp'] = $this->input->post('listhp');
            $data['listpassword'] = $this->input->post('listpassword');
            $data['liststatus'] = $this->input->post('liststatus');
            $this->load->view('login', $data);
        }else {
            $this->load->view('register', $data);

        }

    }
    
    public function login()
    {
        if ($this->input->post('regis')){
            $data['listnama'] = $this->input->post('listnama');
            $data['listhp'] = $this->input->post('listhp');
            $data['listpassword'] = $this->input->post('listpassword');
            $data['liststatus'] = $this->input->post('liststatus');
            $this->load->view('register', $data);
        } else if ($this->input->post('login')) {
            $data['listnama'] = $this->input->post('listnama');
            $data['listhp'] = $this->input->post('listhp');
            $data['listpassword'] = $this->input->post('listpassword');
            $data['liststatus'] = $this->input->post('liststatus');
            
            $nohp = $this->input->post('hp');
            $pass = $this->input->post('password');

            if ($nohp==='00000'){
                if ($pass==='admin'){
                    $data['nama'] = 'admin';
                    $this->load->view('profile', $data);
                }
            }
            else {
                $index = -1;
                $arrHp = explode('|', $data['listhp']);
                $arrPass = explode('|', $data['listpassword']);
                $arrStatus = explode('|', $data['liststatus']);
                $arrNama = explode('|', $data['listnama']);
                for ($i=0; $i < count($arrHp); $i++) { 
                    if ($nohp == $arrHp[$i]){
                        if ($arrPass[$i] == $pass){
                            if ($arrStatus[$i] == '1'){
                                $data['nama'] = $arrNama[$i];
                                $this->load->view('profile', $data);
                                break;
                            }
                        }
                    }
                }
                
            }

            

            // $this->load->view('login', $data);
        }

        
    }

    public function logout(Type $var = null)
    {
        # code...
        $data['listnama'] = $this->input->post('listnama');
        $data['listhp'] = $this->input->post('listhp');
        $data['listpassword'] = $this->input->post('listpassword');
        $data['liststatus'] = $this->input->post('liststatus');
        // unset($data['nama']);
        $this->load->view('login', $data);
    }
}
?>