<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tutor</title>
    <style>
        input, label{
            margin: 5px 0px;
        }
    </style>
</head>
<body>
<?php
    echo form_open('Welcome/index');
    echo form_label("Username")."<br>".form_input('username', '', 'placeholder="Username"')."<br>";
    echo form_label("Password")."<br>".form_password('password', '', "placeholder='Password'")."<br>";
    echo form_label("Confirm Password")."<br>".form_password('cpassword', '', "placeholder='Password'")."<br>";
    echo form_hidden('listuser', $listusername);
    echo form_hidden('listpass', $listpassword);
    echo form_submit('btnSubmit', 'Submit');
    echo form_close();
    echo "<pre style='color:red'>".validation_errors()."</pre>";
?>
</body>
</html>