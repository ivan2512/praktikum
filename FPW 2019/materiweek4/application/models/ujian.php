<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ujian extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function selectallsoal()
    {
        return $this->db->get('soal')->result();
    }

    public function tambahSoal($arr)
    {
        var_dump($arr);
        $arr['id'] = '';
        $this->db->insert('soal', $arr);
    }

    public function tambahUjian($arr)
    {
        var_dump($arr);
        $data = [];
        $data['id'] = "";
        $data['namaujian'] = $arr['namaujian'];
        $this->db->insert('ujian', $data);

        $idlast = $this->db->select("max(id) as id")->get('ujian')->row();
        $idlast = $idlast->id;

        unset($arr['namaujian']);
        unset($arr['soal']);

        foreach ($arr as $key=>$value) {
            if ($value === 'on'){
                $this->db->insert('soal_ujian',['id_soal'=>$key, 'id_ujian'=>$idlast]);
            }
        }

    }

    public function getUjian()
    {
        return $this->db->get('ujian')->result();
    }

    public function getUjianBy($id)
    {
        return $this->db->where('id', $id)->get('ujian')->row();
    }

    public function getSoalUjian($idUjian)
    {
        return $this->db->select('soal.id, teks, a, b, c, d')->from('soal_ujian')->join('soal', 'soal.id = soal_ujian.id_soal')->where('id_ujian', $idUjian)->get()->result();
    }
}
?>