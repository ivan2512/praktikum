<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function insertUser($username, $password, $nama)
    {
        $this->db->insert('user', ['username'=>$username, 'password'=>$password, 'nama'=>$nama]);
        return $this->db->affected_rows();
    }

    public function getDetailUser($username, $password)
    {
        $var = $this->db->where('username', $username)->where("password = '$password'")->get("user");
        return $var->row();
    }

    public function getDetailUsers($username)
    {
        $var = $this->db->where('username', $username)->get("user");
        return $var->row();
    }
}
?>