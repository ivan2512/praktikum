<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('ujian');
        $this->load->helper('url');
        $this->load->helper('form');
    }

    public function index( )
    {
        if ($this->input->post('bLogin')){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if ($username === $password && $username === "admin"){
                redirect('materi/mastersoal');
            }
        }
        $this->load->view('login');
    }

    public function login(Type $var = null)
    {
        # code...
        if ($this->input->post('bLogin')){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if ($username === $password && $username === "admin"){
                redirect('materi/mastersoal');
            } else if (strrev($username) === $password){
                $data['username'] = $username;
                $data['ujians'] = $this->ujian->getUjian();
                $this->load->view('siswa', $data);
            }
        }else {
            $this->load->view('login');
        }
    }
    public function mastersoal()
    {
        
        $this->load->view('master');
    }

    public function submitsoal(Type $var = null)
    {
        if ($this->input->post('bSubmit')){
            $inputan = $this->input->post();
            unset($inputan['bSubmit']);
            $this->ujian->tambahSoal($inputan);
            redirect('materi/mastersoal');
        }
    }
    public function ujian( )
    {
        $data['soal'] = $this->ujian->selectallsoal();
        $this->load->view('ujian', $data);
    }

    public function submitujian( )
    {
        $inp = $this->input->post();
        if(isset($inp['bSubmit'])){
            unset($inp['bSubmit']);
            $this->ujian->tambahUjian($inp);
            redirect('materi/ujian');
        }
    }

    public function ikut($id)
    {
        $ujian = $this->ujian->getUjianBy($id);
        $soal = $this->ujian->getSoalUjian($ujian->id);
        var_dump($soal);
        $data['soal'] = $soal;
        $this->load->view('ikut', $data);
    }
}

?>