const express = require("express");
const mysql = require("mysql");
const app = express();
const jwt = require("jsonwebtoken");

const pool = mysql.createPool({
    host:"localhost",
    database:"tutor_soa_minggu6",
    user:"root",
    password:""
})

app.use(express.urlencoded({extended:true}));

app.post("/api/login",function(req,res){
    const username = req.body.username;
    const password = req.body.password;
    pool.getConnection(function(err,conn){
        if(err) res.status(500).send(err);
        else{
            conn.query(`select * from user where username='${username}' and password ='${password}'`,function(error,result){
                if(error ) res.status(500).send(error);
                else{
                    if(result.length <=0){
                        return res.status(400).send("Invalid Username or password");
                    }
                    const token = jwt.sign({    
                        "username":username,
                        "level":0
                    }   ,"praksoa");
                    res.status(200).send(token);
                }
            })
        }
    });
});
//authorization ->hak akses
//authentication ->masuk ta ga
app.get("/api/tes",function(req,res){
    const token = req.header("x-auth-token");
    let user = {};
    if(!token){
        res.status(401).send("Token not found");
    }
    try{
        user = jwt.verify(token,"praksoa");
    }catch(err){
        //401 not authorized
        res.status(401).send("Token Invalid");
    }
    if((new Date().getTime()/1000)-user.iat>3*86400){
        return res.status(400).send("Token expired");
    }
    if(user.level>0){ //cek bukan admin, kalau admin level 0
        //proses authorization
        return res.status(400).send("you are not allowed to access this resource")
    }
    res.status(200).send(user);
});


app.listen(3000,function(){
    console.log("Listening to port 3000");
})