const express = require('express');
const app = express();
const secret = "rahasia";
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const pool = mysql.createPool({
    database: "materi_soa_6",
    user: "root",
    host: "localhost",
    password: "",
})

const executeQuery = (query, param)=>{
    return new Promise((resolve, reject)=>{
        pool.query(query, param, function (error, result) {
            if (error) reject(error);
            resolve(result);
        })
    });
}

app.use(express.urlencoded({extended: true}));

const router = express.Router();
router.post('/register', async(req, res)=>{
    const {nama, password, nomorhp, alamat, saldo, tipe_user} = req.body;
    if (nama.length > 0 && password.length > 0
        && nomorhp.length > 0 && alamat.length > 0
        && tipe_user.length == 1){
        if (saldo.length == 0) saldo = 0;
        let hasilchecknohp = await executeQuery("SELECT * FROM user WHERE nomorhp = ?", [nomorhp]);
        if (hasilchecknohp.length == 0){
            if (tipe_user == 0){
                if (await adaadmin()){
                    res.status(400).json({
                        status: 400, message: "Admin sudah ada di database"
                    })
                } else {
                    let temp = await executeQuery("INSERT INTO user (nomorhp, nama, alamat, password, tipe, saldo) values (?, ?, ?, ?, ?, ?)", [
                        nomorhp, nama, alamat, password, tipe_user, saldo
                    ])
                    res.status(200).json({
                        status: 200, message: "Berhasil registrasi admin",
                        admin: {
                            nomorhp: nomorhp, nama: nama, alamat:alamat
                        }
                    })
                }
            }   
            else {
                let temp = await executeQuery("INSERT INTO user (nomorhp, nama, alamat, password, tipe, saldo) values (?, ?, ?, ?, ?, ?)", [
                    nomorhp, nama, alamat, password, tipe_user, saldo
                ])
                res.status(200).json({
                    status: 200, message: "Berhasil registrasi user",
                    admin: {
                        nomorhp: nomorhp, nama: nama, alamat:alamat
                    }
                })
            }
        } else {
            res.status(400).json({
                status: 400, message: "Nomor HP telah terdaftar di database"
            })
        }
    }
    else {
        res.status(400).json({
            status: 400, message: "Field harus terisi semua"
        })
    }
})

router.post('/login', async (req, res)=>{
    const {nomorhp, password} = req.body;
    let checklogin = await executeQuery("SELECT * FROM user WHERE nomorhp = ? AND password = ?", [nomorhp, password]);

    if (checklogin.length > 0){
        let token = jwt.sign({
            data: {
                tipe: checklogin[0].tipe,
                nomorhp: checklogin[0].nomorhp
            }
        }, secret, {
            expiresIn: '90m'
        })
        res.status(200).json({
            token: token,
            // user: checklogin[0]
        })
    }
    else {
        res.status(200).json({
            status: 400,
            message: "nomorhp atau password salah"
        })
    }
})

router.post('/addMovie', async (req, res)=>{
    // let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InRpcGUiOjEsIm5vbW9yaHAiOiIwODEyMTYyODI3ODgifSwiaWF0IjoxNTg2OTM2NDM4LCJleHAiOjE1ODY5NDE4Mzh9.584FxSfRpG_hiF41uvDCgnhmczsM45Dy0qwcU7k-xP4"
    const token = req.header("x-auth-token");
    const {judul, tahun, director, durasi, harga} = req.body;
    var id = judul.substr(0, 2).toUpperCase();
    let check = await executeQuery("SELECT * FROM movie WHERE id LIKE ?", [`%${id}%`]);
    id += ((check.length + 1).toString()).padStart(3, '0');
    try {
        let hasil = jwt.verify(token, secret);
        if (Date.now() > hasil.exp){
            if (hasil.data.tipe == 0){
                let query = "INSERT INTO movie (id, judul, tahun, director, durasi, harga) values (?, ?, ?, ?, ?, ?)"
                let params = [
                    id, judul, tahun, director, durasi, harga
                ]
                let t = await executeQuery(query, params)
                console.log(params, t)
                res.status(200).json({
                    status: 200,
                    message: "Berhasil add movie"
                })
            }
            else {
                res.status(200).json({
                    status: 400,
                    message: "Bukan admin nda boleh add movie"
                })
            }
        }
        else {
            res.status(400).json({
                status: 400, 
                message: "token expired"
            })
        }  
    } catch (error) {
        res.status(400).json({
            status: 400,
            error: error,
            message: "Token invalid"
        })
    }  
})

router.get('/movies', async (req, res)=>{
    const judul = req.query.title;
    const token = req.header('x-auth-token');

    try {
        let hasil = jwt.verify(token, secret);
        if (Date.now() > hasil.exp){
            let query = "SELECT * FROM movie WHERE judul LIKE ?";
            let param = [`%${judul}%`]
            console.log(param);
            res.status(200).json({
                status: 200,
                data: await executeQuery(query, param)
            })
        }
        else {
            res.status(400).json({
                status: 400, 
                message: "token expired"
            })
        }
    } catch (error) {
        res.status(400).json({
            status: 400,
            error: error,
            message: "Token invalid"
        })
    }
})

router.post('/topup', async (req, res)=>{
    const {nomorhp, saldo} = req.body;
    
    const token = req.header('x-auth-token');
    if (!token){
        res.status(400).json({
            status: 400,
            message: "Token invalid"
        })
    }
    else {
        try {
            let hasil = jwt.verify(token, secret);
            if (Date.now() > hasil.exp){
                if (hasil.data.tipe == 1 && hasil.data.nomorhp == nomorhp){
                    var previous = await executeQuery("SELECT * FROM user WHERE nomorhp = ?", [nomorhp])
                    previous = previous[0].saldo;
                    previous = parseInt(previous);
                    let baru = parseInt(saldo) + previous;
                    
                    await executeQuery("UPDATE `user` SET `saldo`="+baru+" WHERE `nomorhp` = ?", [
                        nomorhp
                    ])

                    res.status(200).json({
                        status: 200,
                        message: "Berhasil toptup"
                    })
                }
                else {
                    res.status(400).json({
                        status: 403,
                        message: "Topup hanya untuk user"
                    })
                }
            }
            else {
                res.status(400).json({
                    status: 400, 
                    message: "token expired"
                })
            }
        } catch (error) {
            res.status(400).json({
                status: 400,
                error: error,
                message: "Token invalid"
            })
        }
    }
})

router.post('/rent', async(req, res)=>{
    
})

app.use('/api', router);

app.listen(3000, ()=>console.log(`Listening on port 3000`));

async function adaadmin() {
    let t = await executeQuery("SELECT * FROM user WHERE tipe = 0", []);
    return t.length > 0; 
}