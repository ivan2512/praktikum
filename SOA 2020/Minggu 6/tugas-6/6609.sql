-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 11:24 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `6609`
--
CREATE DATABASE IF NOT EXISTS `6609` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `6609`;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` varchar(7) NOT NULL,
  `judul` varchar(128) NOT NULL,
  `bahan` text NOT NULL,
  `kategori` varchar(32) NOT NULL,
  `waktu` varchar(48) NOT NULL,
  `pembuat` varchar(48) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `judul`, `bahan`, `kategori`, `waktu`, `pembuat`) VALUES
('REAP001', 'Tanpa Dua', '[\"satu\",\"tiga\"]', 'Appetizer', '1 jam', 'free@free.com'),
('REAP002', 'Tanpa Dua', '[\"satu\",\"tiga\"]', 'Appetizer', '1 jam', 'free@free.com'),
('REAP003', 'Bahan Lengkap', '[\"satu\",\"dua\",\"tiga\"]', 'Appetizer', '1 jam', 'free@free.com'),
('REAP004', 'Bahan Lengkap', '[\"satu\",\"keju murah\",\"tiga\"]', 'Appetizer', '1 jam', 'free@free.com');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `resep` varchar(7) NOT NULL,
  `reviewer` varchar(48) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `tanggal`, `resep`, `reviewer`, `isi`) VALUES
(1, '2020-04-17', 'REAP001', 'free@free.com', 'Cocok banget buat dicoba nih'),
(2, '2020-12-03', 'REAP001', 'premium@premium.com', 'Cocok banget buat dicoba nih'),
(3, '2020-12-03', 'REAP001', 'premium@premium.com', 'Cocok banget buat dicoba nih'),
(4, '2020-12-05', 'REAP002', 'premium@premium.com', 'Ganti review');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `email` varchar(48) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `tipe` int(1) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`email`, `nama`, `tipe`, `password`) VALUES
('free@free.com', 'free user', 1, 'password'),
('ivan@ivan.com', 'ivan', 0, 'password'),
('premium@premium.com', 'premium user', 2, 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
