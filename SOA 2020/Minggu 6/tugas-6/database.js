const mysql = require('mysql');
const pool = mysql.createPool({
    database: '6609-w6',
    host: 'localhost',
    user: 'root',
    password: ''
});

const execute = async (query, params) => {
  return new Promise((resolve, reject)=>{
    pool.query(query, params, async (error, results) => {
        if (error) reject(error);
        else resolve(results);
    })
  })
};

module.exports = {pool, execute};