const express = require('express');
const app = express();
app.use(express.urlencoded({extended: true}));
const database = require('./database');
const jwt = require('jsonwebtoken');
const secret = "217116609";

const router = express.Router();
router.post('/registerUser', async(req, res)=>{
    const {email, nama, tipe, password} = req.body;
    if (email && nama && tipe && password){
        let hasil = await database.execute(
            "SELECT * FROM users WHERE email = ?",
            [email]
        );
        if (hasil.length == 0){
            hasil = await database.execute(
                "INSERT INTO users (email, nama, tipe, password) values (?, ?, ?, ?)",
                [email, nama, tipe, password]
            );
            res.status(200).json({
                status: 200, message: "sukses register",
                data: { user: {
                    email, nama,
                    tipe: tipe==0?"admin":tipe==1?'free':tipe==2?'premium':'unknown',
                } }
            })
        }
        else {
            res.status(400).json({
                status:400, message: "email telah terdaftar dalam database"
            });
        }
    } else {
        res.status(400).json({
            status:400, message: "harap isi semua field"
        });
    }
});

router.post('/loginUser', async (req, res) => {
    const {email, password} = req.body;
    if (email && password){
        let hasil = await database.execute(
            "SELECT email, nama, tipe FROM users WHERE email = ? AND password = ?",
            [email, password]
        )
        if (hasil.length > 0){
            res.status(200).json({
                status: 200, message: "login sukses",
                token: jwt.sign({
                    data: { user: hasil[0] }
                }, secret, {expiresIn: '1h'})
            });
        } else {
            res.status(400).json({
                status: 400, message: "Username atau password salah"
            })
        }
    } else {
        res.status(400).json({
            status: 400, message: 'harap isi semua field'
        })
    }
});

router.post('/createRecipe', async (req, res) => {
    const token = req.header('6609-auth');
    try {
        let temp = await checktoken(token);
        const user = temp.data.user;
        if (user.tipe == 0){
            res.status(400).json({status: 400, message: "Admin tidak boleh buat recipe"})
        }
        else {
            const {judul, bahan, kategori, waktu} = req.body;
            if (judul && bahan && kategori && waktu){
                let id = "RE" + (kategori==='Main Course'?'MC' : kategori==='Appetizer'?'AP' : kategori==='Dessert'? 'DS' : 'UK');
                if (id.includes('UK')){
                    res.status(400).json( {status: 400, message: 'invalid kategori'})
                } else {
                    const pembuat = temp.data.user.email;
                    let t = await database.execute("SELECT * FROM recipes WHERE id LIKE ?",
                        [`%${id}%`]);
                    id += (t.length+1).toString().padStart(3, '0');
                    const params = [judul, JSON.stringify(bahan), kategori, waktu, pembuat, id];
                    let hasil = await database.execute(
                        "INSERT INTO recipes " +
                        "(judul, bahan, kategori, waktu, pembuat, id) values " +
                        "(?, ?, ?, ?, ?, ?)",
                        params
                    );
                    console.log(hasil);
                    
                    res.status(200).json({
                        status: 200, message: "berhasil insert",
                        recipe : { id, judul, bahan, kategori, waktu, pembuat }
                    })
                }
            } else {
                res.status(400).json({status: 400, message: 'harap isi semua field'})
            }
        }
    } catch (e) {
        console.log("error",e);
        res.status(400).json({status: 400, message: e.toString()})
    }
});

router.get('/searchRecipe', async (req, res) => {
    const token = req.header('6609-auth');
    try {
        let verified = await checktoken(token);
        const user = verified.data.user;
        const {query, exclude} = req.query;
        if (query){
            const dbquery = "SELECT * FROM recipes WHERE judul like ?";
            const param = [ `%${query}%`];
            let hasil = await database.execute(dbquery, param);
            hasil = hasil.map(e => {return { ...e, bahan: JSON.parse(e.bahan) }});
            if (exclude && user.tipe==2){
                if (user.tipe == 2){
                    hasil = hasil.filter(e => {return !e.bahan.find(el => el.split(' ').includes(exclude) )});
                    console.log('premium user');
                    res.status(200).json( formatkembalian(hasil) );
                }
            }
            else {
                console.log('tanpa exclude / exclude tapi bukan premium');
                res.status(200).json( formatkembalian(hasil) );
            }
        } else {
            let hasil = await database.execute("SELECT * FROM recipes");
            hasil = hasil.map(e => {return { ...e, bahan: JSON.parse(e.bahan) }});
            console.log('tanpa query');
            res.status(200).json( formatkembalian(hasil) );
        }
    } catch (e) {
        console.log("error",e);
        res.status(400).json({status: 400, message: e.toString()})
    }
});

router.post('/reviewRecipe', async (req, res) => {
    const token = req.header('6609-auth');
    try {
        const verified = await checktoken(token);
        const {kode_resep, isi_review} = req.body;
        if (kode_resep && isi_review){
            const checkrecipe = await database.execute("SELECT * FROM recipes WHERE id = ?", [kode_resep]);
            if (checkrecipe.length > 0){
                const resep = checkrecipe[0];
                const user = verified.data.user;
                const email = user.email;
                let tanggal = new Date(Date.now());
                if (req.body.tanggal) {
                    tanggal = Date.parse(req.body.tanggal);
                    if (!tanggal) tanggal = new Date(Date.now());
                    else tanggal = new Date(tanggal);
                }
                const queryinsert = "INSERT INTO reviews (tanggal, resep, reviewer, isi) VALUES (?, ?, ?, ?)";
                const paraminsert = [tanggal, resep.id, email, isi_review]
                const hasilquery = await database.execute(queryinsert, paraminsert);
                res.json({status: 200, message: "berhasil insert review"});
            }
            else {
                res.status(400).json({status: 400, message: `kode resep ${kode_resep} tidak ditemukan dalam database`});
            }
        }
        else {
            res.status(400).json({status: 400, message: "harap isi semua field"});
        }
    } catch (error) {
        console.error(error);
        res.status(400).json({status: 400, message: error.toString()});
    }
});

router.get('/getReview', async (req, res) => {
    const token = req.header('6609-auth');
    try {
        const verified = await checktoken(token);
        const {date} = req.query;
        if (date){
            const splitdate = date.split('/');
            let tanggal = new Date();
            if (splitdate[0] > 12 || splitdate[0] < 0)
                throw new Error("invalid date")

            tanggal.setMonth( parseInt(splitdate[0])-1 );
            tanggal.setDate( parseInt(splitdate[1]) );
            tanggal.setFullYear( parseInt(splitdate[2]) );
            tanggal.setHours(0); tanggal.setMinutes(0);
            tanggal.setSeconds(0); tanggal.setMilliseconds(0);
            const reviews = await database.execute(
                "SELECT reviews.*, r.judul FROM reviews, recipes r WHERE r.id = reviews.resep AND reviews.tanggal = ? ORDER BY resep", 
                [tanggal]
            );
            const kembalian = { tanggal_review : formattanggal(tanggal), kumpulan_review: [] };
            for (let index = 0; index < reviews.length; index++) {
                const element = reviews[index];
                kembalian['kumpulan_review'].push({
                    email_reviewer: element.reviewer,
                    kode_resep: element.resep,
                    judul_resep: element.judul,
                    isi_review: element.isi
                });
            }
            res.json(kembalian);
        }   
        else {
            const reviews = await database.execute("SELECT reviews.*, r.judul FROM reviews, recipes r WHERE r.id = reviews.resep ORDER BY resep", [])
            const kembalian = [];
            for (let index = 0; index < reviews.length; index++) {
                const element = reviews[index];
                const currentresep = kembalian.find(e => e.kode_resep == element.resep);
                if (currentresep){
                    currentresep.review_resep.push({
                        email_reviewer: element.reviewer,
                        isi_reviewer: element.isi,
                        tanggal_review: formattanggal(new Date(element.tanggal))
                    });
                }
                else {
                    const resepbaru = {
                        kode_resep : element.resep,
                        judul_resep: element.judul,
                        review_resep : []
                    }
                    resepbaru.review_resep.push({
                        email_reviewer: element.reviewer,
                        isi_reviewer: element.isi,
                        tanggal_review: formattanggal(new Date(element.tanggal))
                    })
                    kembalian.push(resepbaru);
                }           
            }
            res.json(kembalian)
        }    
    } catch (error) {
        console.error("error getReview", error);
        res.status(400).json({status: 400, message: error.toString()});
    }
});

app.use('/api', router);

function formattanggal(tanggal){
    return (tanggal.getMonth()+1).toString().padStart(2, '0') +"/"+ (tanggal.getDate()).toString().padStart(2, '0') +"/"+ (tanggal.getFullYear()).toString().padStart(4, '0');
}

function checktoken(token){
    return new Promise(((resolve, reject) => {
        if (token){
            try {
                const hasil = jwt.verify(token, secret);
                return resolve(hasil);
            } catch (e) {
                console.log(e);
                reject('token invalid / expired');
            }
        } else {
            reject('no token passed')
        }
    }));
}

function formatkembalian(recipearr){
    return recipearr.map(e => { return {
        kode_resep: e.id, judul_resep: e.judul,
        type_resep: e.kategori, bahan: e.bahan,
        email_pembuat_resep: e.pembuat
    }});
}
module.exports = app;