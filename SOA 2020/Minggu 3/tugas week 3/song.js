const db = require('./database');
const eksport = {};

eksport.deletesong = async (req, res) => {
    const judul = req.params.judul;
    let hapus = await db.executeQuery("SELECT * FROM lagu where judul_lagu = ?", [judul]);
    if (hapus.length > 0) {
        await db.executeQuery("DELETE FROM lagu WHERE judul_lagu = ?", [judul]);
        res.status(200).json({
            status: 200,
            message: "Lagu berhasil didelete"
        });
    } else {
        res.status(400).json({
            status: 400,
            message: `Lagu dengan judul ${judul} tidak ditemukan`
        })
    }
}

eksport.uploadsong = async (req, res) => {
    const { judul_lagu, genre, durasi, id_pengguna } = req.body;
    var kode = "";
    var skip = false;
    if (genre == "POP") kode = "10";
    else if (genre == "JAZZ") kode = "20";
    else if (genre == "COUNTRY") kode = "30";
    else {
        skip = true;
        res.status(400).json({
            status: 400,
            message: "Invalid genre. Input genre : POP, JAZZ, COUNTRY"
        });
    }
    if (!skip) {
        res.header("Content-Type", "application/json");
        try {
            let hasilQuery = await db.executeQuery("SELECT * FROM pengguna WHERE idpengguna = ?", [parseInt(id_pengguna)]);
            let checkJudul = await db.executeQuery("SELECT * FROM lagu WHERE judul_lagu like ?", [`%${judul_lagu}%`]);
            if (hasilQuery.length <= 0) {
                res.status(400).write(JSON.stringify({
                    status: 400,
                    message: `ID pengguna ${id_pengguna} tidak ditemukan`
                }));
            } else if (hasilQuery[0].tipe_pengguna == "0") {
                res.status(400).write(JSON.stringify({
                    status: 400,
                    message: `ID pengguna ${id_pengguna} tidak memiliki hak untuk mengupload lagu`
                }));
            } else if (checkJudul.length > 0) {
                res.status(400).write(JSON.stringify({
                    status: 400,
                    message: `Judul ${judul_lagu} sudah ada dalam database`
                }));
            } else {
                var idlagu = "S" + kode;
                let kodes = await db.executeQuery("SELECT lpad(ifnull(max(substr(id_lagu, -3)), 0) + 1,3, '0') as maks FROM lagu WHERE id_lagu like ?",
                    [`%${idlagu}%`]);
                idlagu = idlagu + kodes[0].maks;
                let hasilInsert = await db.executeQuery("INSERT INTO lagu (id_lagu, judul_lagu, genre, durasi, id_uploader) values (?, ?, ?, ?, ?)",
                    [idlagu, judul_lagu, genre, durasi, id_pengguna]);
                const { nama, email } = hasilQuery[0];
                res.status(200).write(JSON.stringify({
                    status: 200,
                    message: `Lagu ${judul_lagu} berhasil diinsert!`,
                    lagu: {
                        id_lagu: idlagu,
                        judul_lagu, genre, durasi,
                        uploader: {
                            id: id_pengguna,
                            nama,
                            email
                    }   }
                })  );
            }
        } catch (err) {
            res.status(400).write(JSON.stringify(err));
        }
        res.end();
    }
}

eksport.searchsong = async (req, res) => {
    const keyword = req.params.keyword;
    let hasil = await db.executeQuery("SELECT * FROM lagu WHERE judul_lagu LIKE ?", [`%${keyword}%`]);
    res.json({status: 200, lagu: hasil})
    res.end();
}
module.exports = eksport;