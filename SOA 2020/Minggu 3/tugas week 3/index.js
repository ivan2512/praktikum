const express = require("express");
const app = express();
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
const port = 3000;
const db = require('./database');
const registeruser = require('./register');
const {createplaylist, getplaylist, updateplaylist} = require('./playlist');
const {searchsong, deletesong, uploadsong} = require('./song');
[[]]
app.post("/registerUser", async (req, res) => registeruser(req, res));

app.post("/uploadSong", async (req, res) => uploadsong(req, res));

app.post("/createPlaylist", async (req, res) => createplaylist(req, res));

app.get("/searchSongByKeyword/:keyword", (req, res) => searchsong(req, res));

app.delete("/deleteSong/:judul", async (req, res) => deletesong(req, res));

app.get("/getPlaylist/:email", async (req, res) => getplaylist(req, res));

app.put("/updatePlaylist/:id", async (req, res) => updateplaylist(req, res));

app.post("/followPlaylist", async (req, res) => {
    const {id_user, id_playlist, password_user} = req.body;
    res.header('Content-Type', 'application/json');
    let login = await db.executeQuery("SELECT * FROM pengguna where idpengguna = ? and password = ?", [id_user, password_user]);
    if (login.length > 0){
        let checkvalid = await db.executeQuery("SELECT * FROM playlist WHERE id_playlist = ?", [id_playlist]);
        if (checkvalid.length > 0){
            if (checkvalid[0].status_playlist == 1){
                try {
                    await db.executeQuery("INSERT INTO follow_playlist (id_playlist, followed_by) values (?, ?)", [id_playlist, id_user]);
                    res.write(JSON.stringify({
                        status: 200,
                        message: "Berhasil follow playlist"
                    }))
                } catch (error) {
                    await db.executeQuery("DELETE FROM follow_playlist WHERE id_playlist = ? and followed_by = ?", [id_playlist, id_user]);
                    res.write(JSON.stringify({
                        status: 200,
                        message: "Berhasil unfollow playlist"
                    }))
                }
            } else {
                res.status(400).write(JSON.stringify({status: 400,
                    message:"Playlist bersifat private, tidak bisa difollow"
                }));    
            }
        } else {
            res.status(400).write(JSON.stringify({status: 400,
                message:"Playlist tidak ditemukan"
            }));
        }
    } else {
        res.status(400).write(JSON.stringify({
            status: 400, message:"Invalid password / User not found"
        }))
    }
    res.end();
});

app.get("/getPopularPlaylist", async (req, res) => {
    let populars = await db.executeQuery("SELECT p.id_playlist, p.nama_playlist, count(fp.followed_by) as followers_count  FROM playlist p join follow_playlist fp on fp.id_playlist = p.id_playlist group by p.id_playlist having count(fp.followed_by) > 19");
    for (let index = 0; index < populars.length; index++) {
        const element = populars[index];
        let song_collection = await db.executeQuery("SELECT l.judul_lagu from lagu l join lagu_playlist lp on l.id_lagu = lp.id_lagu");
        let followed_by = await db.executeQuery("select p.email from pengguna p join follow_playlist fp on fp.followed_by = p.idpengguna"); 
        element.song_collection = song_collection.map(e => e.judul_lagu);
        element.followed_by = followed_by.map(e => e.email);

    }
    res.json(populars);
});



app.listen(port, () => {
    console.log(`listening on ${port}`);
});