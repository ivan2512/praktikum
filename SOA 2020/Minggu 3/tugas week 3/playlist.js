const db = require('./database');
const playlistmodule = {};
playlistmodule.createplaylist = async (req, res) => {
    const { email_pengguna, nama_playlist, id_lagu, status_playlist } = req.body;
    res.header("Content-Type", "application/json");
    const status = status_playlist == "0" ? "S" : "P";
    let temp = await db.checkLagu(id_lagu);
    if (temp) {
        var idpl = "PL" + status;
        let kodes = await db.executeQuery("SELECT lpad(ifnull(max(substr(id_playlist, -3)), 0) + 1,3, '0') as maks FROM playlist WHERE id_playlist like ?", [`%${idpl}%`] );
        idpl = idpl + kodes[0].maks;
        let id_pengguna = await db.executeQuery("SELECT idpengguna, email, nama, tipe_pengguna FROM pengguna WHERE email = ?", [email_pengguna]);

        if (id_pengguna.length <= 0) {
            res.status(400).write(JSON.stringify({
                status: 400,
                message: "Email tidak ditemukan"
            }));
        } else if (id_pengguna[0].tipe_pengguna == 1){
            res.status(400).write(JSON.stringify({
                status: 400,
                message: "Artist tidak dapat membuat playlist."
            }))
        } else {
            let hasilCreatePlaylist = await db.executeQuery("INSERT INTO playlist (id_playlist, id_creator, nama_playlist, status_playlist) VALUES (?, ?, ?, ?)", [idpl, id_pengguna[0].idpengguna, nama_playlist, status_playlist]);
            id_lagu.forEach(element => {
                db.executeQuery("INSERT INTO lagu_playlist (id_playlist, id_lagu) VALUES (?, ?)", [idpl, element]);
            });
            let lagus = await db.executeQuery("SELECT * FROM LAGU WHERE id_lagu IN (?)", [id_lagu]);

            res.status(200).write(JSON.stringify({
                status: 200, message: `Playlist ${nama_playlist} berhasil dibuat!`,
                playlist: {
                    nama_playlist,
                    id_playlist: idpl,
                    status_playlist: status_playlist,
                    lagu: lagus,
                    creator: {
                        ...id_pengguna[0]
                    }
                }
            }));
        }
    } else {
        res.status(400).write(JSON.stringify({
            status: 400,
            message: "Ada id lagu yang tidak ditemukan dalam database"
        }));
    }
    res.end();
}

playlistmodule.getplaylist = async (req, res) => {
    const email = req.params.email;    
    let pengguna = await db.executeQuery(`SELECT * FROM pengguna WHERE email = ?`, [email]);    
    res.header('Content-Type', 'application/json');
    if (pengguna.length > 0){        
        let playlists = await db.executeQuery(`SELECT id_playlist, nama_playlist, status_playlist FROM playlist WHERE id_creator = ?`, [pengguna[0].idpengguna]);
        for (let index = 0; index < playlists.length; index++) {
            const element = playlists[index];
            let lagus = await db.executeQuery("SELECT l.* FROM lagu l JOIN lagu_playlist lp ON l.id_lagu = lp.id_lagu WHERE lp.id_playlist = ?", [element.id_playlist]);
            element.lagus = lagus;
        }
        res.status(200).write(JSON.stringify({
            status: 200,
            playlists: playlists
        }));
    }
    else {
        res.status(400).write(JSON.stringify({
            status: 400,
            message: `Pengguna dengan email ${email} tidak ditemukan`
        }));
    }
    res.end();
}

playlistmodule.updateplaylist = async (req, res)=>{
    const id = req.params.id; const nama_playlist = req.body.nama_playlist;
    res.header("Content-Type", 'application/json');
    let temp = await db.executeQuery("SELECT * FROM playlist WHERE id_playlist = ?", [id]);
    if (temp.length > 0){
        await db.executeQuery("UPDATE playlist set nama_playlist = ? WHERE id_playlist = ?", [nama_playlist, id]);
        let playlistbaru = await db.executeQuery("SELECT * FROM playlist WHERE id_playlist = ?", [id]);
        res.status(200).write(JSON.stringify({
            status: 200, 
            playlist: playlistbaru
        }));
    } else {
        res.status(400).write(JSON.stringify({
            status: 400, 
            message: `Playlist dengan id ${id} tidak ditemukan`
        }));
    }
    res.end();
}

module.exports = playlistmodule;