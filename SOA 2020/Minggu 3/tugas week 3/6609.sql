-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2020 at 02:48 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `6609`
--
CREATE DATABASE IF NOT EXISTS `6609` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `6609`;

-- --------------------------------------------------------

--
-- Table structure for table `follow_playlist`
--
-- Creation: Mar 13, 2020 at 04:13 PM
--

DROP TABLE IF EXISTS `follow_playlist`;
CREATE TABLE IF NOT EXISTS `follow_playlist` (
  `id_playlist` varchar(6) NOT NULL,
  `followed_by` int(11) NOT NULL,
  PRIMARY KEY (`id_playlist`,`followed_by`),
  KEY `fk_follow_user` (`followed_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `follow_playlist`
--

INSERT INTO `follow_playlist` (`id_playlist`, `followed_by`) VALUES
('PLP001', 1),
('PLP001', 2),
('PLP001', 3),
('PLP001', 4),
('PLP001', 6),
('PLP001', 7),
('PLP001', 8),
('PLP001', 9),
('PLP001', 10),
('PLP001', 11),
('PLP001', 12),
('PLP001', 13),
('PLP001', 14),
('PLP001', 15),
('PLP001', 16),
('PLP001', 17),
('PLP001', 18),
('PLP001', 19),
('PLP001', 20),
('PLP001', 21);

-- --------------------------------------------------------

--
-- Table structure for table `lagu`
--
-- Creation: Mar 13, 2020 at 01:42 AM
--

DROP TABLE IF EXISTS `lagu`;
CREATE TABLE IF NOT EXISTS `lagu` (
  `id_lagu` varchar(6) NOT NULL,
  `judul_lagu` varchar(255) NOT NULL,
  `genre` varchar(10) NOT NULL,
  `durasi` int(10) UNSIGNED NOT NULL,
  `id_uploader` int(11) NOT NULL,
  PRIMARY KEY (`id_lagu`),
  KEY `fk_uploader_lagupengguna` (`id_uploader`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lagu`
--

INSERT INTO `lagu` (`id_lagu`, `judul_lagu`, `genre`, `durasi`, `id_uploader`) VALUES
('S10001', 'Lagu 1', 'POP', 100, 17),
('S10002', 'Lagu 7', 'POP', 100, 18),
('S10003', 'Lagu 2', 'POP', 100, 18),
('S10004', 'Lagu 8', 'POP', 100, 19),
('S20001', 'Lagu 3', 'JAZZ', 100, 19),
('S20002', 'Lagu 9', 'JAZZ', 100, 20),
('S20003', 'Lagu 4', 'JAZZ', 100, 20),
('S20004', 'Lagu 5', 'JAZZ', 100, 16),
('S20005', 'Lagu 6', 'JAZZ', 100, 17),
('S30001', 'COUNTRY 1', 'COUNTRY', 100, 17),
('S30002', 'COUNTRY 3', 'COUNTRY', 100, 19),
('S30003', 'COUNTRY 7', 'COUNTRY', 100, 18),
('S30004', 'COUNTRY 9', 'COUNTRY', 100, 20),
('S30005', 'COUNTRY 2', 'COUNTRY', 100, 18),
('S30006', 'COUNTRY 8', 'COUNTRY', 100, 19),
('S30007', 'COUNTRY 4', 'COUNTRY', 100, 20);

-- --------------------------------------------------------

--
-- Table structure for table `lagu_playlist`
--
-- Creation: Mar 13, 2020 at 01:38 AM
--

DROP TABLE IF EXISTS `lagu_playlist`;
CREATE TABLE IF NOT EXISTS `lagu_playlist` (
  `id_playlist` varchar(6) NOT NULL,
  `id_lagu` varchar(6) NOT NULL,
  PRIMARY KEY (`id_playlist`,`id_lagu`),
  KEY `fk_lagu_playlistkelagu` (`id_lagu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lagu_playlist`
--

INSERT INTO `lagu_playlist` (`id_playlist`, `id_lagu`) VALUES
('PLP001', 'S10001'),
('PLP001', 'S20001'),
('PLP002', 'S10004'),
('PLP002', 'S30001');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--
-- Creation: Mar 13, 2020 at 04:45 AM
--

DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE IF NOT EXISTS `pengguna` (
  `idpengguna` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tipe_pengguna` tinyint(1) NOT NULL,
  PRIMARY KEY (`idpengguna`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`idpengguna`, `email`, `nama`, `password`, `tipe_pengguna`) VALUES
(1, '1', 'user ke-1', 'password', 0),
(2, '2', 'user ke-2', 'password', 0),
(3, '3', 'user ke-3', 'password', 0),
(4, '4', 'user ke-4', 'password', 0),
(6, '6', 'user ke-6', 'password', 0),
(7, '7', 'user ke-7', 'password', 0),
(8, '8', 'user ke-8', 'password', 0),
(9, '9', 'user ke-9', 'password', 0),
(10, '10', 'user ke-10', 'password', 0),
(11, '11', 'user ke-11', 'password', 0),
(12, '12', 'user ke-12', 'password', 0),
(13, '13', 'user ke-13', 'password', 0),
(14, '14', 'user ke-14', 'password', 0),
(15, '15', 'user ke-15', 'password', 0),
(16, '16', 'user ke-16', 'password', 1),
(17, '17', 'user ke-17', 'password', 1),
(18, '18', 'user ke-18', 'password', 1),
(19, '19', 'user ke-19', 'password', 1),
(20, '20', 'user ke-20', 'password', 1),
(21, '21', 'user ke-21', 'password', 1);

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--
-- Creation: Mar 13, 2020 at 01:46 AM
--

DROP TABLE IF EXISTS `playlist`;
CREATE TABLE IF NOT EXISTS `playlist` (
  `id_playlist` varchar(7) NOT NULL,
  `id_creator` int(11) NOT NULL,
  `nama_playlist` varchar(255) NOT NULL,
  `status_playlist` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_playlist`),
  KEY `fk_creator_playlistkepengguna` (`id_creator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`id_playlist`, `id_creator`, `nama_playlist`, `status_playlist`) VALUES
('PLP001', 1, 'popular playlist', 1),
('PLP002', 10, 'unpopular', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `follow_playlist`
--
ALTER TABLE `follow_playlist`
  ADD CONSTRAINT `fk_follow_playlist` FOREIGN KEY (`id_playlist`) REFERENCES `playlist` (`id_playlist`),
  ADD CONSTRAINT `fk_follow_user` FOREIGN KEY (`followed_by`) REFERENCES `pengguna` (`idpengguna`);

--
-- Constraints for table `lagu`
--
ALTER TABLE `lagu`
  ADD CONSTRAINT `fk_uploader_lagupengguna` FOREIGN KEY (`id_uploader`) REFERENCES `pengguna` (`idpengguna`);

--
-- Constraints for table `lagu_playlist`
--
ALTER TABLE `lagu_playlist`
  ADD CONSTRAINT `fk_lagu_playlistkelagu` FOREIGN KEY (`id_lagu`) REFERENCES `lagu` (`id_lagu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_playlist` FOREIGN KEY (`id_playlist`) REFERENCES `playlist` (`id_playlist`);

--
-- Constraints for table `playlist`
--
ALTER TABLE `playlist`
  ADD CONSTRAINT `fk_creator_playlistkepengguna` FOREIGN KEY (`id_creator`) REFERENCES `pengguna` (`idpengguna`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
