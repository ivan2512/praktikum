const db = require('./database');
module.exports = async (req, res) => {
    const {email, nama, password, tipe_pengguna } = req.body;
    res.header("Content-Type", "application/json");
    try {
        let hasilQuery = await db.executeQuery("INSERT INTO pengguna (email, nama, password, tipe_pengguna) VALUES (?, ?, ?, ?)", [email, nama, password, parseInt(tipe_pengguna)]);
        res.status(200).write(JSON.stringify({
            status: 200,
            message: "Registrasi user " + email + " berhasil!",
            user: {
                id: hasilQuery.insertId,
                email, nama,
                tipe_pengguna: tipe_pengguna == 0 ? "User Biasa" : "Artist"
            }
        }));
    } catch (error) {
        console.error('error', error);
        if (error.error.code === "ER_DUP_ENTRY") {
            res.status(400).write(
                JSON.stringify({
                    status: 400,
                    message: "Registrasi gagal! Email sudah pernah digunakan sebelumnya"
            }));
        } else {
            res.status(400).write(JSON.stringify(error));
        }
    }
    res.end();
}