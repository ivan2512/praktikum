const mysql = require('mysql')
const pool = mysql.createPool({
    connectionLimit: 3,
    host: '127.0.0.1',
    database: '6609',
    user: 'root',
    password: ''
})

const executeQuery = async (query, parameter) => {
    return new Promise((resolve, reject) => {
        pool.query(query, parameter, (error, results) => {
            if (error) {
                console.error("ERROR::", error);
                reject({query, parameter, error})
            }
            resolve(results);
        })
    });
}

const checkLagu = async (lagu) => {
    for (let index = 0; index < lagu.length; index++) {
        const element = lagu[index];
        let temp = await executeQuery("SELECT * FROM lagu WHERE id_lagu = ?", [element]);

        if (!(temp.length > 0)) {
            return false;
        }
    }
    return true;
}

module.exports = {
    pool,
    executeQuery,
    checkLagu
};