const mysql = require('mysql');

const pool = mysql.createPool({
    database: "materi_soa_7",
    user: 'root',
    password: '',
    host: 'localhost'
});

function query(query, param){
    return new Promise((resolve, reject) => {
        pool.query(query, param, (error, result)=>{
            if (error) reject(error);
            else resolve(result);
        })
    })
}

module.exports = {
    pool, query
}