const express = require('express');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
const app = express();
const port = 3000;
const db = require('./database');
app.use(express.urlencoded({extended: true}));

function makeid(length) {
    let result           = '';
    const characters       = '0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

morgan.token('nama-token', (req, res)=>{
    if (req.body['key']){
        return req.body['key'];
    }
    else if (req.params && req.params['key']){
        return req.params['key'];
    }
    else if (req.query && req.query['key']){
        return req.query['key'];
    }
    else return "no-token";
});

app.use(morgan(
   // ':method :url :status :response-time ms - :res[content-length] :nama-token',
    ':method :url :status :response-time ms - :res[content-length] token=:nama-token',
    {
        stream: fs.createWriteStream(path.join(__dirname, "access.log"), {flags:'a'})
    }
));

const router = express.Router();

router.post('/addUser', async (req, res) => {
    let {nomorhp, password, nama, tipe, saldo} = req.body;
    if (!saldo) saldo = 0;
    if (nomorhp && password && nama && tipe){
        const cek = await db.query("SELECT * FROM user WHERE nomorhp = ?", [nomorhp]);
        if (cek.length == 0){
            let q = "INSERT INTO user (nomorhp, password, nama, tipeuser, saldo, gkey, playmusic) VALUES (?, ?, ?, ?, ?, ?, ?)";
            let id = makeid(10);
            let params = [nomorhp, password, nama, tipe, saldo, id, 0];

            let hasil = await db.query(q, params);
            if (hasil){
                await res.json({key: id});
            }
            else {
                res.status(400).json({message: "error"});
            }
        }
        else {
            res.status(400).json({
                message: 'nomor hp sudah terdaftar di database'
            })
        }
    } else {
        res.status(400).json({
            message: "harap isi semua field"
        })
    }
});

router.post('/topUp', async (req, res) => {
    const key = req.body.key || req.query.key; 
    const {topup} = req.body;
    if (key && topup){
        const cek = await db.query("SELECT * FROM user WHERE gkey = ?", [key]);
        if (cek.length > 0){
            let user = cek[0];
            if (user['tipeuser'] == '0'){
                res.status(403).json({
                    message: 'artist tidak perlu topup'
                })
            }
            else {
                await db.query("UPDATE `USER` SET saldo = saldo + ? WHERE gkey = ?", [topup, key])
                res.status(200).json({
                    message: "berhasil topup"
                })
            }
        }
        else {
            res.status(400).json({
                message: "key not found"
            })
        }
    }
    else {
        res.status(400).json({
            message: "harap isi semua field"
        })
    }
});


router.post('/subscribe', async (req, res) => {
   const key = req.body.key || req.query.key;
   console.log("key", req.query.key);
   console.log("key",key);
   if (key){
        let check = await db.query("SELECT * FROM USER WHERE GKEY = ?", [key]);
        if (check){
            let user = check[0];
            if (user['tipeuser'] == '2'){
                let saldo = parseInt(user['saldo']);
                if (saldo >= 50000){
                    let hasil = await db.query("UPDATE USER SET SALDO = saldo-50000, tipeuser = 1 WHERE gkey = ?", [key]);
                    let kembalian =  await db.query("SELECT * FROM USER WHERE GKEY = ?", [key]);
                    let u = kembalian[0];
                    delete u['password'];
                    res.status(200).json({
                        message: "berhasil jadi premium",
                        user: u
                    })
                }
                else {
                    delete user['password'];
                    res.status(400).json({message: "saldo tidak cukup", user: user});
                }
            }
            else {
                delete user['password'];
                res.status(400).json({message: "tidak bisa apply premium", user: user});
            }
        }
        else {
            res.status(400).json({message: "user not found"});
        }
   }
   else {
       res.status(400).json({
           message: "harap isi semua field"
       })
   }
});

router.post('/addsong', async (req, res) => {
    const key = req.body.key || req.query.key;
    const {judul_lagu, genre, durasi} = req.body;
    if (key && judul_lagu && genre && durasi){
        let check = await db.query("SELECT * FROM USER WHERE GKEY = ?", [key]);
        if (check.length > 0){
            let user = check[0];
            if (user['tipeuser'] == 0){
                let g = "INSERT INTO music (judul, genre, durasi, nomorhpartis) VALUES (?, ?, ?, ?)";
                let param = [judul_lagu, genre, durasi, user['nomorhp']];
                let q = await db.query(g, param);
                delete user['password'];
                console.log(q);
                res.status(200).json({
                    message: "berhasil add song",
                    lagu: {id: q.insertId, judul: judul_lagu, genre: genre, durasi: durasi, artis: user}
                })
            }
            else {
                res.status(400).json({
                   message: "bukan artist ga boleh add song"
                });
            }
        }
        else {
            res.status(400).json({
                message: "user not found"
            });
        }
    }
    else {
        res.status(400).json({
            message: "harap isi semua field"
        })
    }
});

router.post('/listen', async (req, res) => {
    const key = req.body.key || req.query.key;
    const id_lagu = req.body.id_lagu;
    if (key && id_lagu){
        let check = await db.query("SELECT * FROM USER WHERE GKEY = ?", [key]);
        let lagu = await db.query("SELECT * FROM music WHERE id = ?", [id_lagu]);
        if (check.length > 0 && lagu.length > 0){
            lagu = lagu[0];
            let hpartis = lagu['nomorhpartis'];
            let u = check[0];
            if (u['tipeuser'] == 2){
                if (u['playmusic'] == 10){
                    res.status(403).json({message: "ga bisa play music lagi udah 10 kali"})
                }
                else {
                    await db.query("UPDATE USER SET playmusic = playmusic+1 WHERE gkey = ?", [key]);
                    await db.query("UPDATE USER SET saldo = saldo + 1000 WHERE nomorhp = ?", [hpartis]);
                    res.json({message: "berhasil play music"});
                }
            }
            else {
                await db.query("UPDATE USER SET playmusic = playmusic+1 WHERE gkey = ?", [key]);
                await db.query("UPDATE USER SET saldo = saldo + 1000 WHERE nomorhp = ?", [hpartis]);
                res.json({message: "berhasil play music"});
            }
        }
        else {
            res.status(400).json({
                message: "song / user not found"
            })
        }
    }
    else {
        res.status(400).json({
            message: "harap isi semua field"
        });
    }
});

router.delete('/song', async (req, res) => {
    const key = req.body.key || req.query.key;
    const idlagu = req.body.idlagu;

    if (key && idlagu){
        let check = await db.query("SELECT * FROM USER WHERE GKEY = ?", [key]);
        let lagu = await db.query("SELECT * FROM music WHERE id = ?", [idlagu]);
        if (check.length > 0 && lagu.length > 0){
            let user = check[0];
            lagu = lagu[0];
            if (user['nomorhp'] == lagu['nomorhpartis']){
                await db.query("DELETE from music WHERE id = ?", [lagu['id']])
                res.status(200).json({
                    message: "song deleted"
                })
            }
            else {
                res.status(400).json({
                    message: "lagu bukan milik user tersebut"
                })
            }
        }
        else {
            res.status(400).json({
                message: "lagu/ user not found"
            })
        }
    }
    else {
        res.status(400).json({
            message: "harap isi semua field"
        })
    }
});

app.use('/api', router);
app.listen(port, ()=>{
    console.log(`listening on ${port}`)
});