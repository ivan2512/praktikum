const mysql = require('mysql');
const pool = mysql.createPool({
   database: '6609',
   host: 'localhost',
   user: 'root',
   password: ''
});

function query(query, param){
    if (!Array.isArray(param)) param = [param]
    return new Promise((resolve, reject) => {
        pool.query(query, param, (error, results)=>{
            if (error) reject (error);
            else resolve (results);
        })
    })
}

module.exports = { pool, query};