const express = require('express');
const router = express.Router();
const db = require('./database');
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');

const App = express();
App.use(express.urlencoded({extended: true}));

App.use(morgan(
    ':method :url :status :response-time ms :res[content-length]',
    { stream: fs.createWriteStream(path.join(__dirname, "access.log"), {flags:'a'}) }
));
App.use(morgan('dev'));

router.get('/', async (req, res) => {
    console.log('accessed endpoint');
    res.json({
        message: "success"
    })
});

const checkemail = "SELECT * FROM users WHERE email = ?";
const checkkey = "SELECT * FROM users WHERE apikey = ?";
router.post('/registerUser', async (req, res) => {
    const user = {...req.body};
    if (!(user.tipe && user.tipe.length > 0)) user.tipe = 0;
    if (!(user.saldo && user.saldo.length > 0)) user.saldo = 0;
    if (!(user.hits && user.hits.length > 0)) user.hits = 10;

    if (user.nama && user.email &&  user.password && user.hp){
        let params = [ user['nama'], user['email'], user['password'], user['hp'],
            user['email'], user['saldo'], user['tipe'], user['hits'] ];
        let check = await db.query(
            "SELECT nama, email, apikey, saldo, hits FROM users WHERE email = ?", user['email']);
        if (check.length > 0)
            res.status(400).json({
                message: "email sudah terdaftar dalam database"});
        else{
            try{
                let h = await db.query("INSERT INTO users " +
                    "(nama, email, password, hp, apikey, saldo, tipe, hits) VALUES " +
                    "(?, ?, ?, ?, md5(?), ?, ?, ?)", params);
                res.json(await db.query(
                    "SELECT nama, email, apikey, saldo, hits FROM users WHERE email = ?", user['email']));
            } catch (e) {
                console.error(e);
                res.status(500).json(e);
            }
        }
    } else {
        res.status(400).json({message: "field tidak lengkap"});
    }
});

router.post('/topup', async (req, res) => {
    const {email, nominal} = req.body;
    if (email && parseInt(nominal)){
        let check = await db.query(checkemail, [email]);
        if (check.length  > 0){
            let update = await db.query("UPDATE users SET saldo = saldo + ? WHERE email = ?",
                [parseInt(nominal), email]);
            let user = await db.query("SELECT email, saldo FROM users WHERE email = ?", [email])
            res.json({message: "sukses top up", user: user[0]})
        }
        else
            res.status(400).json({message: `email ${email} not found in database`, email: email})
    }
    else res.status(400).json({message: "field tidak lengkap", fields_needed: {email: 'string', nominal: 'number'}});
});

router.post('/getPremium', async (req, res) => {
    const {key, email} = req.body;
    if (key && email){
        let check = await db.query(checkemail+" and apikey = ? and tipe != 1", [email, key]);
        if (check.length > 0){
            let check = await db.query("UPDATE users SET tipe = 1 WHERE email = ? AND apikey = ?", [email, key]);
            let suer = (await db.query("SELECT nama, email, hp, apikey, tipe, hits from users where email = ?", email) )[0];
            suer.tipe = suer.tipe === 1 ? 'PREMIUM USER' :  "FREE USER";
            res.json({message: "sukses jadi premium", user: suer})
        }
        else res.status(400).json({message: `gagal jadi premium`, data: {email: email, key: key}})
    }
    else res.status(400).json({message: "field tidak lengkap", fields_needed: {key: 'string', email: 'string'}});
});

router.post('/addApiSubscription', async (req, res) => {
    const {email, amount} = req.body;
    if (email && parseInt(amount)){
        let check = await db.query(checkemail, email);
        if (check.length > 0){
            let user = check[0];
            delete user.password;
            if (user.saldo >= parseInt(amount) * 50){
                let d = await db.query("UPDATE users SET hits = hits + ?, saldo = saldo - ? WHERE email = ?", [parseInt(amount), parseInt(amount)*50, email]);
                let user = (await db.query(checkemail, email))[0];
                res.json({message: "berhasil tambah api subscription", user})
            }
            else res.status(400).json({message: "saldo tidak cukup untuk topup", data: {email, amount:parseInt(amount)}, user: user})
        }
        else res.status(400).json({message: `email ${email} not found in database`, email: email})
    }
    else res.status(400).json({message: "field tidak lengkap", fields_needed: {email: 'string', amount: 'number'}});
});

router.post('/createReport', async (req, res) => {
    const key = req.body.key;
    const {judul, deskripsi, jenis, alamat, kodepos, tanggal} = req.body;
    let pelapor = req.body.pelapor;
    let objdate = new Date(); objdate.setHours(0,0,0,0);
    let elementanggal = tanggal.split('-');
    objdate.setFullYear(elementanggal[0]);
    objdate.setMonth(parseInt(elementanggal[1]) - 1);
    objdate.setDate(elementanggal[2]);
    if (key && judul && deskripsi && jenis && alamat && kodepos && objdate){
        let check = (await db.query(checkkey, key))[0];
        delete check['password'];
        if (check.hits > 0){
            if (check.tipe === 1){
                if (pelapor){ pelapor = check.email }
                else pelapor = 'anonymous';
            }
            else {
                if (pelapor){ pelapor = check.email }
                else pelapor = check.email;
            }
            let queryreport = "INSERT INTO reports " +
                "(judul, deskripsi, jenis, alamat, kodepos, tanggal, pelapor) values " +
                "(?, ?, ?, ?, ?, ?, ?)";
            let params = [judul, deskripsi, jenis, alamat, kodepos, objdate, pelapor];
            check = await db.query(queryreport, params);
            await db.query("UPDATE users SET hits = hits - 1 WHERE apikey = ?", key);
            res.json({message: 'report created', report: await db.query("SELECT * FROM reports WHERE id = ?", check.insertId)});
        }
        else res.status(400).json({message: "quota akses anda habis", user: check});
    }
    else res.status(400).json({message: "field tidak lengkap",
        fields_needed: {key:'string', judul:'string', deskripsi:'string',
            jenis :'string', alamat :'string', kodepos:'string', tanggal: 'string yyyy-mm-dd'}});
});

router.get('/searchReport', async (req, res) => {
    const key = req.query.apiKey;
    if (key){
        let user = (await db.query(checkkey, key));
        if (user.length > 0){
            user = user[0]; delete user['password'];
            if (user.hits <= 0) res.status(400).json({message: "hak akses apiKey anda habis", user});
            else{
                let reports = await db.query("SELECT * FROM REPORTS");
                if (user.tipe === 0) reports = reports.filter( e => new Date(e.tanggal).getFullYear() === 2020);
                const qq = req.query;
                // console.log("queries",qq);
                if (qq.jenis_kriminalitas && qq.jenis_kriminalitas.length > 0)
                    reports = reports.filter( e => e.jenis === qq.jenis_kriminalitas );
                if (qq.zip_code && qq.zip_code.length > 0)
                    reports = reports.filter( e => e.kodepos === qq.zip_code );
                if (user.tipe === 1 && qq.tahun && qq.tahun.length === 4)
                    reports = reports.filter( e => new Date(e.tanggal).getFullYear() === parseInt(qq.tahun));
                await db.query("UPDATE users SET hits = hits -1 WHERE apikey = ?", key);
                res.json(reports);
            }
        }
        else res.status(400).json({message: "apiKey tidak valid"});
    }
    else res.status(400).json({message: "field tidak lengkap", fields_needed: {apiKey: 'string'}});
});

App.use('/api', router);
module.exports = App;