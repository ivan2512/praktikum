-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2020 at 11:26 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `6609`
--
CREATE DATABASE IF NOT EXISTS `6609` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `6609`;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kodepos` varchar(10) NOT NULL,
  `tanggal` datetime NOT NULL,
  `pelapor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `judul`, `deskripsi`, `jenis`, `alamat`, `kodepos`, `tanggal`, `pelapor`) VALUES
(1, 'Berita Satu', 'ini deskripsi berita satu', 'tabrak lari', 'alamat kejadian berita satu', '12345', '2020-04-23 00:00:00', 'anonymous'),
(2, 'Berita Dua', 'ini deskripsi berita dua', 'penculikan', 'alamat kejadian berita dua', '54321', '2020-04-23 00:00:00', 'anonymous'),
(3, 'Berita Tiga', 'deskripsi berita tiga', 'kekerasan', 'bukan alamat kejadian berita satu dan dua', '987654', '2020-04-23 00:00:00', 'anonymous'),
(4, 'Berita Empat', 'ini deskripsi berita empat', 'aktifitas mencurigakan', 'Ngagel Jaya Selatan', '96385', '2019-04-23 00:00:00', 'anonymous'),
(5, 'Berita Lima', 'ini deskripsi berita lima', 'kekerasan', 'alamat berita lima', '87456', '2018-04-16 00:00:00', 'anonymous'),
(6, 'Berita Enam', 'ini deskripsi berita enam', 'tabrak lari', 'alamat kejadian berita enam', '12345', '2018-04-16 00:00:00', 'anonymous'),
(7, 'Berita Tujuh', 'ini deskripsi berita tujuh', 'kekerasan', 'alamat berita tujuh', '54321', '2019-03-01 00:00:00', 'anonymous'),
(8, 'berita delapan', 'deskripsi berita delapan', 'aktifitas mencurigakan', 'alamat berita delapan', '963852', '2020-12-12 00:00:00', 'email'),
(9, 'berita sembilan', 'deskripsi berita sembilan', 'kekerasan', 'alamat berita sembilan', '963852', '2019-12-12 00:00:00', 'email'),
(10, 'berita sepuluh', 'deskripsi berita sepuluh', 'penculikan', 'alamat berita sepuluh', '147852', '2005-12-12 00:00:00', 'email'),
(11, 'berita sebelas', 'deskripsi berita sebelas', 'tabrak lari', 'alamat berita sebelas', '54321', '2019-12-12 00:00:00', 'email'),
(12, 'berita 12', 'deskripsi berita 12', 'penculikan', 'alamat berita 12', '987654', '2009-12-12 00:00:00', 'email'),
(13, 'berita 13', 'deskripsi berita 13', 'aktifitas mencurigakan', 'alamat berita 13', '87456', '2009-12-12 00:00:00', 'email'),
(14, 'berita 14', 'deskripsi berita 14', 'tabrak lari', 'alamat berita 14', '963852', '2020-12-12 00:00:00', 'email'),
(15, 'berita 15', 'deskripsi berita 15', 'penculikan', 'alamat berita 15', '12345', '2020-07-28 00:00:00', 'email'),
(16, 'berita 16', 'deskripsi berita 16', 'penculikan', 'alamat berita 16', '456789', '2020-07-28 00:00:00', 'email'),
(17, 'berita 17', 'deskripsi berita 17', 'tabrak lari', 'alamat berita 17', '456789', '2020-01-28 00:00:00', 'email'),
(18, 'berita 18', 'deskripsi berita 18', 'aktifitas mencurigakan', 'alamat berita 18', '456789', '2020-02-29 00:00:00', 'email'),
(19, 'berita 19', 'deskripsi berita 19', 'aktifitas mencurigakan', 'alamat berita 19', '456789', '2020-02-01 00:00:00', 'email'),
(20, 'berita 20', 'deskripsi berita 20', 'kekerasan', 'alamat berita 20', '456789', '2000-02-29 00:00:00', 'email');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `hp` varchar(20) NOT NULL,
  `apikey` varchar(10) NOT NULL,
  `saldo` int(10) UNSIGNED NOT NULL,
  `tipe` int(1) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`nama`, `email`, `password`, `hp`, `apikey`, `saldo`, `tipe`, `hits`) VALUES
('free', 'email', 'password', 'nohp', '0c83f57c78', 500, 0, 7),
('baru', 'paru', 'password', 'nohp', '6ed8bd2d20', 462, 1, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
