const express = require('express')
const app = express();
const port = 3000;
const user = require('./user')

var pengguna = [];
var current_id = 1;

var songs = [];
var current_song_id = 1;

var playlists = [];
var current_playlist_id = 1;

app.use(express.urlencoded({extended: true}));

app.post('/registerUser', (req, res)=>{
    let temp = user.registrasiuser(req, res, pengguna, current_id)
    current_id = temp.current_id;
    pengguna = temp.pengguna;
})
app.post('/uploadSong', (req, res)=>uploadsong(req, res));
app.post('/createPlaylist', (request, response)=>createplaylist(request, response));
app.get('/searchSongByKeyword/:keyword', (req,  res)=>{
    res.json({status:'success', search_results: songs.filter(s => s.judul.includes(req.params.keyword))})
});

app.get('/getPlaylist/:email', (req, res)=>{
    const lop = playlists.filter(ele => ele.creator == req.params.email);
    if (pengguna.find(elem => elem.email == req.params.email))
        res.json({status:'success', playlists: lop});
    else 
        res.json({status:'fail', message:`${req.params.email} tidak terdaftar sebagai user`});
})

app.delete('/deleteSong/:judul', (req, res)=>{
    const newsongs = songs.filter(e => e.judul !== req.params.judul);
    if (newsongs.length == songs.length){
        res.status(400).json({
            status:'fail',
            message:'judul tidak ditemukan',
        })
    } else {
        songs = newsongs;
        res.json({
            status:'success',
            message: `Lagu dengan judul ${req.params.judul} berhasil dihapus`
        })
    }
})

app.put('/updatePlaylist/:id', (req, res)=>{
    const playlist = playlists.find(p => p.id_playlist==req.params.id);
    if (playlist){
        playlist.judul_playlist = req.body.judul_baru;
        playlists = playlists.filter(p => p.id_playlist !== playlist.id_playlist);
        playlists.push(playlist);
        res.json({
            status:'success',
            message:'Playlist berhasil diupdate',
            playlist
        })
    }
    else {
        res.json({
            status:'fail',
            message:`Playlist dengan ID ${req.params.id} tidak ditemukan`,
        })
    }
})

app.get('/debug', (req, res)=>{
    res.json({
        pengguna, songs, playlists
    })
})

app.listen(port, ()=> console.log(`Listening on ${port}`))

const uploadsong = (req, res)=>{
    const song = req.body
    let checklagu = songs.find(e => e.judul == song.judul )
    if (!checklagu){
        let checkuser = pengguna.find(e => e.id == song.uploader);
        if (checkuser){
            if (checkuser.tipe === '1'){
                song.id = current_song_id++;
                songs = [...songs, song];
                res.json({
                    status:'success',
                    message:'Lagu berhasil ditambahkan',
                    song: song,
                })
            }
            else {
                res.status(400).json({
                    status: 'fail',
                    message: 'Pengguna tidak mempunyai hak untuk mengupload lagu'
                })    
            }
        }
        else {
            res.status(400).json({
                status: 'fail',
                message: 'Pengguna tidak ditemukan'
            })
        }
    } 
    else {
        res.status(400).json({
            message:"Judul lagu sudah ada sebelumnya",
            status: 'fail'
        })
    }
}

const createplaylist = (req, res)=>{
    let checkuser = pengguna.find(e => e.email == req.body.creator);
    var flag = true;
    const list_lagu = [...req.body.lagus.map(e=> parseInt(e))];
    list_lagu.forEach(element => {
        if (!songs.find(e=>e.id == element)){
            flag = false;
            return;
        }
        element = parseInt(element)
    });
    if (checkuser && flag){
        const playlist = {
            judul_playlist: req.body.name,
            creator: req.body.creator,
            collection_lagu: list_lagu,
            id_playlist: current_playlist_id++,
        }
        playlists.push(playlist);
        res.json({
            status:'success',
            playlist: playlist
        });
    } else {
        res.json({
            status:'fail',
            message: (checkuser)?'Beberapa lagu tidak ditemukan':'User tidak valid'
        })
    }
}
