const registrasiuser = (req, res, pengguna, current_id)=>{
    const {email, nama, password, tipe} = req.body;
    const temp = pengguna.find(elem=> elem.email === email)
    if (temp){
        res.status(400).json({
            status: 'fail', 
            message: `Registrasi gagal! Email ${email} sudah terpakai sebelumnya`
        });
    }
    else {
        const user = {
            id: current_id++,
            email, nama, password, tipe
        }
        pengguna = [...pengguna, user]
        let kirim = {
            user_id: user.id,
            email, nama, tipe
        }
        res.status(200).json({
            status: 'success',
            message: `Registrasi user ${email} berhasil!`,
            user: kirim,
        })
    }
    return {current_id, pengguna};
}

module.exports = { registrasiuser }