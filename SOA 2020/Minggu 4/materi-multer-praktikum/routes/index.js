const express = require('express');
const router = express.Router();
const db = require('../database');
const multer = require('multer');
const upload = multer({storage: multer.diskStorage({
        destination: (request, file, callback)=>{
            callback(null, './uploads');
        },
        filename:  async (request, file, callback)=>{
            const username = request.body.username;
            let id = username[0].toUpperCase() + username[1].toUpperCase();
            let jumlah = await db.executeQuery("SELECT * FROM video WHERE username = ?", [username]);
            let temp = jumlah.length + 1;
            id += temp.toString().padStart(3, '0');

            await db.executeQuery("INSERT INTO video (username, judul, deskripsi) values (?, ?, ?)", [
                username,
                request.body.judul,
                request.body.deskripsi,
            ]);

            let str = file.originalname.substring(file.originalname.indexOf('.'), file.originalname.length);
            console.log(id)
            callback(null, id.toString()+str.toString());
        }
    })
});

router.post('/user', async (req, res) => {
    const user = {
        ...req.body
    }
    res.header('Content-Type', 'application/json');
    if (user.hasOwnProperty('username') &&
        user.hasOwnProperty('password') &&
        user.hasOwnProperty('nomorhp')){
        try{
            let cek = await db.executeQuery("SELECT id, username FROM user WHERE username = ?", [user.username]);
            if (cek.length == 0){
                let hasil = await db.executeQuery("INSERT INTO user (username, password, nohp) values (?, ?, ?)", [user.username, user.password, user.nomorhp]);
                res.write(JSON.stringify({
                    status: 200,
                    message:"Berhasil register user baru",
                    user: {
                        username: user.username,
                        "nomorhp": user.nomorhp
                    }
                }))
            }
            else {
                res.write(JSON.stringify(
                    {status: 400,
                    message: "Username sudah pernah digunakan sebelumnya"}
                ));
                res.status(400);
            }
        } catch (e) {
            console.error(e);
            res.status(500);
            res.write(JSON.stringify({
                status: 500,
                message:"unexpected error, baca pesan di console"
            }))
        }
    } else {
        res.write(JSON.stringify({
            status:400,
            message:'Field tidak lengkap'
        }))
        res.status(400);
    }
    res.end();
});

router.post('/subscribe', async (req, res) => {
   // { username, password, usercari }
   let username = req.body.username;
   let password = req.body.password;
   let search = req.body.usercari;

   try{
       let hasilcari = await db.executeQuery("SELECT * FROM user WHERE username = ?", [search]);
       if (hasilcari.length > 0){
            let ceksudahsubscribe = await db.executeQuery("SELECT * FROM subscribe where username = ? and usernamesub = ?",
                [username, search]);
            if (ceksudahsubscribe.length > 0){
                res.status(200).json({
                    status: 200, message: "Sudah pernah subscribe",
                    ps:"Tidak ada pengecekan password dalam penjelasan soal"
                });
            }
            else {
                let hasilsub = await db.executeQuery("INSERT INTO subscribe (username, usernamesub) values (?, ?) ", [username, search]);
                res.status(200).json({
                    status: 200, message: "Berhasil subscribe",
                    ps:"Tidak ada pengecekan password dalam penjelasan soal"
                });
            }
       }else {
           res.status(200).json({
               status: 200,
               message: `Username ${search} tidak ditemukan`
           });
       }
   } catch (e) {
       console.error(e);
       res.status(500).json({
           status: 500,
           message: "unexpected error",
       })
   }
});

router.delete('/subscribe', async (req, res) => {
    // { username, password, usercari }
    let username = req.body.username;
    let password = req.body.password;
    let search = req.body.usercari;

    try{
        let hasilcari = await db.executeQuery("SELECT * FROM user WHERE username = ?", [search]);
        if (hasilcari.length > 0){
            let ceksudahsubscribe = await db.executeQuery("SELECT * FROM subscribe where username = ? and usernamesub = ?",
                [username, search]);
            if (ceksudahsubscribe.length > 0){
                await db.executeQuery("DELETE FROM subscribe WHERE username = ? and usernamesub = ?",
                    [username, search]);
                res.status(200).json({
                    status: 200, message: "Unsubbed",
                    ps:"Tidak ada pengecekan password dalam penjelasan soal"
                });
            }
            else {
                res.status(200).json({
                    status: 200, message: `${username} belum subscribe ke ${search}`,
                    ps:"Tidak ada pengecekan password dalam penjelasan soal"
                });
            }
        }else {
            res.status(200).json({
                status: 200,
                message: `Username ${search} tidak ditemukan`
            });
        }
    } catch (e) {
        console.error(e);
        res.status(500).json({
            status: 500,
            message: "unexpected error",
        })
    }
});

router.get('/viewsubscriber/:username', async (req, res) => {
    //{ username, password }
    let username = req.params.username;
    try{
        let subscribers = await db.executeQuery("SELECT username FROM subscribe WHERE usernamesub = ?", [username]);
        res.status(200).json({
            status: 200,
            data: {
                subscriber: subscribers.length,
                list: subscribers
            }
        })
    } catch (e) {
        console.error(e);
        res.status(500).json({
            status: 500,
            message: "unexpected error",
        })
    }
});

router.post('/video', upload.single('filename'),async (req, res) => {

    // { username, password, judul, deskripsi, filename }
    if (req.file){
        res.status(200).json({
            status: 200,
            message:"berhasil upload video"
        })
    }else {
        res.status(400);
        res.json({
            message: "Thumbnail harus disertakan"
        })
    }

});

router.post('/cobaUpload', upload.single('field-name'), async (req, res) => {
    res.send('masuk /api/cobaUpload');
});

module.exports = router;
