const mysql = require('mysql');

const pool = mysql.createPool(require('./config').database);

exports.getConnection = async () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((error, connection)=>{
            if (error) reject (error);
            else resolve (connection);
        })
    })
};

exports.executeQuery = (query, parameter, connection=null)=>{
    return new Promise((resolve, reject) => {
        if (connection){
            connection.query(query, parameter, function (error, results) {
                if (error) reject(error);
                else resolve(results);
            });
        } else {
            pool.query(query, parameter, function (error, results) {
                if (error) reject(error);
                else resolve(results);
            })
        }
    })
};