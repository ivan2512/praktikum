const mysql = require('mysql');
const pool = mysql.createPool({
    database: '6609-w4',
    user:'root',
    password:'',
    host:'127.0.0.1'
});
const executeQuery = (query, parameters) => {
    return new Promise((resolve, reject) => {
        pool.query(query, parameters, (error, results)=>{
            if (error) reject(error);
            else resolve(results);
        })
    })
};
module.exports = { pool, executeQuery };