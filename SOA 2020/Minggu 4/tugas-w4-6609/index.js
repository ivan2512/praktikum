const express = require('express');
const app = express();
const port = 3000;
const database =  require('./database');
const multer = require('multer');
const path = require('path');

app.use(express.urlencoded({extended: true}));
app.use(express.static('images'))
app.set("view engine", "ejs");
app.use(require('cookie-parser')());

app.get('/', async (req, res) => {
    res.clearCookie('username');
    res.render('./pages/loginpage');
});
app.get('/loginPage', async (req, res) => {
    res.clearCookie('username');
    res.render('./pages/loginpage');
});

app.post('/checkLogin', async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    if (username === password && password === 'admin'){
        console.log('masuk bagian admin');
        res.cookie('username', 'admin', {httpOnly: true});
        res.redirect('/adminPage');
    }
    else {
        let temp = await database.executeQuery("SELECT * FROM user WHERE username = ?", [username]);
        if (temp.length > 0){
            //sudah ada username tersebut dalam database
            if (password === temp[0].password){
                res.cookie('username', username, {httpOnly: true});
                res.redirect('/homePage');
            } else { res.redirect('/'); }
        }
        else {
            //belum ada username tersebut, insert dan redirect ke homepage
            await database.executeQuery("INSERT INTO user (username, password) VALUES (?, ?)", [username, password]);
            res.cookie('username', username, {httpOnly: true});
            res.redirect('/homePage');
        }
    }
});

app.get('/adminPage', async (req, res) => {
    let kuki = req.cookies;
    res.clearCookie('error');
    if (kuki['username'] === 'admin'){
        let beritas = await database.executeQuery("SELECT * FROM berita WHERE status = 1", []);
        res.render('./pages/admin', { error: kuki['error'], berita: beritas });
    }
    else { res.redirect('/'); }
});

const upload = multer({
    storage: multer.diskStorage({
        destination: (request, file, callback)=>{ callback(null, './images'); },
        filename:  async (req, file, callback)=>{
            const judul = req.body.judul;  const kategori = req.body.kategori;
            const isi = req.body.isi; let tanggal = new Date(req.body.tanggal);
            let waktu = req.body.waktu.split(':');
            tanggal = new Date(tanggal.getFullYear(), tanggal.getMonth(), tanggal.getDate(), waktu[0], waktu[1]);
            let t = await database.executeQuery(
                "INSERT INTO berita (judul, kategori, isi, tanggal, gambar, status) values (?, ?, ?, ?, ?, 1)",
                [ judul, kategori, isi, tanggal, file.originalname ]);
            let nama_gambar = t.insertId; let ext = path.extname(file.originalname);
            nama_gambar = nama_gambar + ext;
            let hasil = await database.executeQuery("UPDATE berita SET gambar = ? WHERE id = ?", [nama_gambar, t.insertId]);
            callback(null, nama_gambar);
        }
    }),
    fileFilter: async (req, file, callback) => {
        if (file.mimetype.toString().includes('png') || file.mimetype.toString().includes('jpeg')){
            const judul = req.body.judul; const kategori = req.body.kategori;
            const tanggal = new Date(req.body.tanggal); const isi = req.body.isi;
            const waktu = req.body.waktu;
            if (judul && kategori && tanggal && waktu && isi) callback(null, true);
            else callback(new Error("Field ada yang kosong"));
        }
        else{ callback(new Error("Salah tipe file")); }
    },
});
app.post('/uploadBerita', async (req, res) => {
    const middleware = upload.single('gambar');
    middleware(req, res, async (error) => {
        if (error){
            res.cookie('error', error.message, { httpOnly: true });
            res.redirect('/adminPage');
        }
        else{ res.redirect('/adminPage'); }
    });
});

app.delete('/deleteBerita', async (req, res)=>{
    let hasil = await database.executeQuery("UPDATE berita SET status = 0 WHERE id = ?", [req.body.id]);
    res.json(hasil);
});

app.get('/homePage', async (req, res) => {
    let kuki = req.cookies;
    res.render('./pages/homepage');
});

app.get('/berita', async (req, res)=>{
    let beritas = await database.executeQuery("SELECT * FROM berita WHERE status = 1 ORDER BY tanggal DESC");
    for (let index = 0; index < beritas.length; index++) {
        const element = beritas[index];
        element.comments = await database.executeQuery("SELECT * FROM comment WHERE berita = ? ORDER BY tanggal DESC", [element.id]);
    }
    await res.json(beritas);
});

app.post('/commentBerita', async(req, res)=>{
    const user = req.cookies.username;
    const berita = req.body.idberita;
    const komentar = req.body.comment;
    if (komentar.includes("kasar")){ return res.end(); }
    let hasil = await database.executeQuery(
        "INSERT INTO comment (berita, user, komentar) values (?, ?, ?)",
        [ berita, user, komentar ]);
    res.write(JSON.stringify(hasil));
    res.end();
});

app.listen(port, ()=>{ console.log(`listening on ${port}`); });
