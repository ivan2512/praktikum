const express = require("express");
const app = express();
const port = 8080;

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));

const renderKalkulatorBilangan = (req, res, params = {}) => {
  res.render("./kalkulatorbilangan.ejs", params);
};

const renderKalkulatorTanggal = (req, res, params = {}) => {
  res.render("./kalkulatortanggal.ejs", params);
};

const renderKalkulatorDecimalOctal = (req, res, params = {}) => {
  res.render("./kalkulatordecimaloctal.ejs", params);
};

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

app.get("/", (req, res) => {
  renderKalkulatorBilangan(req, res);
});

app.get("/kalkulatorBilanganPage", (req, res) => {
  renderKalkulatorBilangan(req, res);
});

app.get("/kalkulatorTanggalPage", (req, res) => {
  renderKalkulatorTanggal(req, res);
});

app.get("/kalkulatorDecimalOctalPage", (req, res) => {
  renderKalkulatorDecimalOctal(req, res);
});

app.post("/calculateBilangan", (req, res, next) => {
  const { angka1, angka2, operator } = req.body;
  params = {};
  try {
    if (angka1 === "" || operator === "" || angka2 === "") {
      throw new Error();
    }
    hasil = eval(`${angka1} ${operator} ${angka2}`);
    params.hasil = true;
    params.hasilmessage = "Hasil perhitungan: " + hasil.toLocaleString();
  } catch (error) {
    params.status = true;
    params.errormessage = "Input harus diisi semua";
  }
  renderKalkulatorBilangan(req, res, params);
});

app.post("/calculateTanggal", (req, res) => {
  var { tgl1, tgl2 } = req.body;
  params = {};
  try {
    if (tgl1 === "" || tgl2 === "") {
      throw new Error("Seluruh input harus diisi");
    }
    const millisinaday = 1000 * 60 * 60 * 24;
    tgl1 = new Date(tgl1);
    tgl2 = new Date(tgl2);
    diff = (tgl1.getTime() - tgl2.getTime()) / millisinaday;
    diff = Math.abs(diff);
    params.hasil = true;
    params.hasilmessage = `Perbedaannya ${diff} hari <br> atau \<br\>`;
    bulan = Math.floor(diff / 30);
    minggu = diff - bulan * 30;
    minggu = Math.floor(minggu / 7);
    hari = diff - minggu * 7 - bulan * 30;
    params.hasilmessage += `Perbedaannya ${bulan} bulan ${minggu} minggu ${hari} hari`;
  } catch (error) {
    params.status = true;
    params.errormessage = error;
  }
  renderKalkulatorTanggal(req, res, params);
});

app.post("/calculateDecimalOctal", (req, res) => {
  const { pilihan, tobeconverted } = req.body;
  params = {};
  try {
    if (isNaN(pilihan) || tobeconverted === "") {
      throw new Error("Input harus diisi semua");
    }
    if (pilihan == 2) {
      if (tobeconverted.includes("8") || tobeconverted.includes("9")) {
        throw new Error("Bilangan Octal hanya dari 0-7");
      }
      digits = tobeconverted.split("");
      sum = 0;
      digits.forEach(element => {
        sum *= 8;
        sum += parseInt(element);
      });
      params.hasil = true;
      params.hasilmessage = `Hasil konversi ${parseInt(tobeconverted).toLocaleString()} menjadi bilangan deimal adalah ${sum.toLocaleString()}`;
    } else {
      digits = parseInt(tobeconverted);
      hasil = "";
      while (digits > 0) {
        temp = digits % 8;
        hasil = temp.toString() + hasil;
        digits = Math.floor(digits / 8);
      }
      params.hasil = true;
      params.hasilmessage = `Hasil konversi ${parseInt(tobeconverted).toLocaleString()} menjadi bilangan octal adalah ${parseInt(hasil).toLocaleString()}`;
    }
  } catch (error) {
    params.status = true;
    params.errormessage = error;
  }
  renderKalkulatorDecimalOctal(req, res, params);
});
