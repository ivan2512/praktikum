﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace perpustakaanKelas
{
    public class kotak
    {
        public Rectangle block { get; set; }
        public int angka { get; set; }
        public bool selected { get; set; }

        public kotak(int x, int y, int angka)
        {
            block = new Rectangle(x, y, 50, 50);
            this.angka = angka;
            selected = false;
        }
    }
}
