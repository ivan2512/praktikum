﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using perpustakaanKelas;


namespace materiweek4
{
    public partial class Form1 : Form
    {
        kotak[,] papanKu = new kotak[5, 5];
        kotak[,] papanMusuh = new kotak[5, 5];
        List<Rectangle> lingkaran;
        Graphics g;
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
            lingkaran = new List<Rectangle>();
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    papanKu[j, i] = new kotak(j*60+10, i*60+10, r.Next(25) + 1);
                    if (kembar(papanKu[j, i], j, i, papanKu))
                    {
                        j--;
                    }
                }
            }
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    papanMusuh[j, i] = new kotak((j + 5) * 60 + 10, i * 60 + 10, r.Next(25) + 1);

                    if (kembar(papanMusuh[j, i], j, i, papanMusuh))
                    {
                        j--;
                    }
                }
            }
        }

        private bool kembar(kotak ini, int x, int y, kotak[,] arr)
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (j == x && y == i) { }
                    else
                    {
                        if (arr[j, i]!=null)
                        {
                            if (ini.angka == arr[j, i].angka)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            Font f = new Font("Arial", 14);

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    e.Graphics.FillRectangle(new SolidBrush(Color.Yellow) ,papanKu[j, i].block);
                    e.Graphics.DrawString(papanKu[j, i].angka + "", f, new SolidBrush(Color.Black), new Point(j * 60 + 10,i * 60 + 10));

                    e.Graphics.FillRectangle(new SolidBrush(Color.Green), papanMusuh[j, i].block);
                    e.Graphics.DrawString(papanMusuh[j, i].angka + "", f, new SolidBrush(Color.Black), new Point((j+5) * 60 + 10, i * 60 + 10));
                }
            }
            foreach (var item in lingkaran)
            {
                g.FillEllipse(new SolidBrush(Color.Red), item);
            }
            f = new Font("Courier New", 32);
            g.DrawString("BINGO",f, new SolidBrush(Color.Black), new Point(10, 420));
            g.DrawString("BINGO", f, new SolidBrush(Color.Black), new Point(300, 420));
            if (int.Parse(player.Text) > 0)
            {
                g.DrawLine(Pens.Red, new Point(10, 440), new Point(10+(int.Parse(player.Text) * 32), 440));
            }
            if (int.Parse(musuh.Text) > 0)
            {
                g.DrawLine(Pens.Blue, new Point(300, 440), new Point(300+(int.Parse(musuh.Text) * 32), 440));
            }
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Point mouse = new Point(e.X, e.Y);
            foreach (var item in papanKu)
            {
                if (item.block.Contains(mouse) && item.selected==false)
                {
                    lingkaran.Add(item.block);
                    item.selected = true;
                    foreach (var punyaMusuh in papanMusuh)
                    {
                        if (item.angka == punyaMusuh.angka)
                        {
                            punyaMusuh.selected = true;
                            lingkaran.Add(punyaMusuh.block);
                            break;
                        }
                    }
                }
            }
            foreach (var item in papanMusuh)
            {
                if (!item.selected)
                {
                    lingkaran.Add(item.block);
                    item.selected = true;
                    foreach (var punyaMusuh in papanKu)
                    {
                        if (item.angka == punyaMusuh.angka)
                        {
                            punyaMusuh.selected = true;
                            lingkaran.Add(punyaMusuh.block);
                            break;
                        }
                    }
                    break;
                }
            }
            this.Invalidate();
            int c = cekBingo(papanKu);
            player.Text = c+"";
            c = cekBingo(papanMusuh);
            musuh.Text = c + "";
            Invalidate();
            if (int.Parse(player.Text) >= 5)
            {
                MessageBox.Show("Player Win!");
            } else if (int.Parse(musuh.Text) >= 5)
            {
                MessageBox.Show("Musuh Menang");
            }
            
        }
        private int cekBingo(kotak[,] papan)
        {
            int c = 0;
            bool flag1 = true;
            bool flag2 = true;
            for (int i = 0; i < 5; i++)
            {
                if (cekH(papan, i))
                    c++;
                if (cekV(papan, i))
                    c++;
                if (!papan[i, i].selected)
                {
                    flag1 = false;
                }
                if (!papan[i, 4-i].selected)
                {
                    flag2 = false;
                }
            }
            if (flag1) ++c;
            if (flag2) ++c;
            return c;
        }

        private bool cekH(kotak[,] papan, int i)
        {
            if (papan[i, 0].selected)
            {
                if (papan[i, 1].selected)
                {
                    if (papan[i, 2].selected)
                    {
                        if (papan[i, 3].selected)
                        {
                            if (papan[i, 4].selected)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        private bool cekV(kotak[,] papan, int i)
        {
            if (papan[0, i].selected)
            {
                if (papan[1, i].selected)
                {
                    if (papan[2, i].selected)
                    {
                        if (papan[3, i].selected)
                        {
                            if (papan[4, i].selected)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
    
}
