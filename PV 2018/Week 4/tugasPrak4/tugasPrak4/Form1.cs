﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace tugasPrak4 {
    public partial class Form1 : Form {
        //pukul pakai WASD atau panah
        //area pukulan ditandai kotak berwarna merah
        SoundPlayer temp;
        player me;
        List<musuh> arr;
        bool diMainMenu = true;
        bool diHighScore = false;
        bool kegepuk = false;
        Font f;
        Rectangle play, HighScore, Quit;
        Rectangle Back;
        int[] skorTertinggi;
        int berapaDetik = 0;
        public Form1() {
            InitializeComponent();
            me = new player();
        }
        private void Form1_Paint(object sender, PaintEventArgs e) {
            Graphics g = e.Graphics;
            g.DrawImage(global::tugasPrak4.Properties.Resources.bg, 0, 0, this.Width, this.Height);
            if (diMainMenu) {
                g.FillRectangle(Brushes.Red, play);
                g.DrawString("PLAY", f, Brushes.Black, play.Left+(play.Width/2)-2*24, play.Top+(play.Height/2)-(f.Height/2));
                g.FillRectangle(Brushes.Cyan, HighScore);
                g.DrawString("HIGH SCORE", f, Brushes.Black, HighScore.Left + (HighScore.Width / 2) - (5 * 24), HighScore.Top + ((HighScore.Height - f.Height) / 2));
                g.FillRectangle(Brushes.White, Quit);
                g.DrawString("QUIT", f, Brushes.Black, Quit.Left + (Quit.Width / 2) - 2 * 24, Quit.Top + Quit.Height / 2 - f.Height / 2);
            } else if (diHighScore) {
                g.FillRectangle(Brushes.Wheat, new Rectangle(10, 10, 410, 400));
                g.DrawString("HIGH SCORE", f, Brushes.Black, 10, 25);
                for (int i = 0; i < 5; i++) {
                    g.DrawString((i+1)+"    "+skorTertinggi[i], f, Brushes.Black, 10, i * 50 + 75);
                }
                g.FillRectangle(Brushes.Black, Back);
                g.DrawString("BACK", f, Brushes.White, Back.X, Back.Y);
            } else {
                g.DrawString("Skor: "+me.Skor.ToString(), f, Brushes.Black, 5, 5);
                g.DrawString("Life: " + me.Life.ToString(), f, Brushes.Black, 5, 30);
                g.DrawImage((Image)(new Bitmap(me.Gbr[me.Arah, me.Gerakan], new Size(120, 240))), new Point(this.Width / 3 + 15, 50));
                Pen p = new Pen(Color.Red, 5);
                g.DrawRectangle(p, me.kiri);
                g.DrawRectangle(p, me.tengah);
                g.DrawRectangle(p, me.kanan);
                foreach (musuh item in arr) {
                    if (item is kiri) {
                        kiri temp = (kiri)item;
                        g.DrawImage(temp.gbr[temp.gerakan, me.Gerakan], 10, temp.y, 120, 240);
                        temp.setRect(new Rectangle(10, temp.y, 120, 240));
                    } else {
                        kananTengah temp = (kananTengah)item;
                        g.DrawImage(temp.gbr[temp.gerakan, me.Gerakan], temp.x, temp.y, 120, 240);
                        temp.setRect(new Rectangle(temp.x, temp.y, 120, 240));
                    }
                }
                for (int i = arr.Count-1; i >=0; i--) {
                    if (arr[i].gerakan == 1) {
                        arr.Remove(arr[i]);
                    }
                }
            }
            if (kegepuk) {
                g.DrawString("Hit SPACE to continue", f, Brushes.Black, 0, Height / 2);
            }
        }
        private void timer1_Tick(object sender, EventArgs e) {
            this.Invalidate();
            me.Gerakan++;
            berapaDetik += 160;
            for (int i = arr.Count-1; i >=0 ; i--) {
                if (arr[i].getRect().IntersectsWith(me.kiri) && me.Arah == 1) {
                    arr[i].gerakan = 1;
                    me.Skor++;
                    arr[i].hit = true;
                    arr[i].gerakan = 1;
                    temp.Play();
                    
                } else if (arr[i].getRect().IntersectsWith(me.tengah) && me.Arah == 2) {
                    arr[i].gerakan = 1;
                    me.Skor++;
                    arr[i].hit = true;
                    arr[i].gerakan = 1;
                    temp.Play();
                } else if (arr[i].getRect().IntersectsWith(me.kanan) && me.Arah == 3) {
                    arr[i].gerakan = 1;
                    me.Skor++;
                    arr[i].hit = true;
                    arr[i].gerakan = 1;
                    temp.Play();
                }
            }
            if (me.Skor >= 10) {
                timer2.Interval = 1000;
            }
            bool reset = false;
            for (int i = arr.Count-1; i >= 0; i--) {
                if (arr[i].getRect().IntersectsWith(me.kiriAtas)|| arr[i].getRect().IntersectsWith(me.kananAtas)|| arr[i].getRect().IntersectsWith(me.tengahAtas)) {
                    if (!arr[i].hit) {
                        timer2.Enabled = false;
                        me.Life--;
                        arr.RemoveAt(i);
                        reset = true;
                        break;
                    }
                }
            }
            if (reset) {
                arr = new List<musuh>();
                kegepuk = true;
                me.Arah = 4;                
            }
            for (int i = 0; i < arr.Count; i++) {
                arr[i].y -= 20;
            }
            if (me.Life <= 0) {
                kegepuk = false;
                timer1.Stop();
                timer2.Stop();
                diMainMenu = true;
                RefreshLayar();
                for (int i = 4; i >= 0; i--) {
                    if (me.Skor > skorTertinggi[i]) {
                        skorTertinggi[i] = me.Skor;
                        break;
                    }
                }
                Array.Sort(skorTertinggi);
                Array.Reverse(skorTertinggi);
            }
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e) {
            if (!kegepuk) {
                switch (e.KeyCode) {
                    case Keys.A:
                    case Keys.Left:
                        me.Arah = 1;
                        break;
                    case Keys.Right:
                    case Keys.D:
                        me.Arah = 3;
                        break;
                    case Keys.W:
                    case Keys.Down:
                    case Keys.Up:
                    case Keys.S:
                        me.Arah = 2;
                        break;
                    case Keys.X:
                        me.Arah = 4;
                        break;
                }
            } else {
                switch (e.KeyCode) {
                    case Keys.Space:
                        if (kegepuk) {
                            me.Arah = 0;
                            timer2.Start();
                            kegepuk = !kegepuk;
                        }
                        break;
                }
            }
        }
        void RefreshLayar() {
            if (diHighScore) {
                Back = new Rectangle(this.Width - 130, this.Height - 80, 100, 50);
                play = new Rectangle();
                HighScore = new Rectangle();
                Quit = new Rectangle();
            } else if (diMainMenu) {
                play = new Rectangle(10, 110, 410, 150);
                HighScore = new Rectangle(10, 270, 410, 150);
                Quit = new Rectangle(10, 440, 410, 150);
                Back = new Rectangle();
            } else {
                play = new Rectangle();
                HighScore = new Rectangle();
                Quit = new Rectangle();
                Back = new Rectangle();
            }
        }
        private void Form1_KeyUp(object sender, KeyEventArgs e) {
            if (!kegepuk) {
                if (e.KeyCode == Keys.X || e.KeyCode == Keys.W || e.KeyCode == Keys.A || e.KeyCode == Keys.S || e.KeyCode == Keys.D || e.KeyCode == Keys.Right || e.KeyCode == Keys.Left || e.KeyCode == Keys.Down || e.KeyCode == Keys.Up) {
                    me.Arah = 0;
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e) {
            timer1.Enabled = false;
            timer2.Enabled = false;
            play = new Rectangle(10, 110, 410, 150);
            HighScore = new Rectangle(10, 270, 410, 150);
            Quit = new Rectangle(10, 440, 410, 150);
            Back = new Rectangle(this.Width-130,this.Height-80,100,50);
            skorTertinggi = new int[5];
            for (int i = 0; i < skorTertinggi.Length; i++) {
                skorTertinggi[i] = 0;
            }
            f = new Font("Courier New", 24, FontStyle.Bold);
            temp = new SoundPlayer("pukul.wav");
        }
        private void Form1_MouseClick(object sender, MouseEventArgs e) {
            Point mouse = e.Location;
            if (play.Contains(mouse)) {
                timer2.Interval = 2000;
                diMainMenu = false;
                diHighScore = false;
                RefreshLayar();
                this.Invalidate();
                timer1.Enabled = true;
                timer2.Enabled = true;
                me = new player();
                arr = new List<musuh>();
            } else if (Quit.Contains(mouse)) {
                this.Close();
            } else if (HighScore.Contains(mouse)) {
                diMainMenu = false;
                diHighScore = true;
                RefreshLayar();
                this.Invalidate();
            } else if (Back.Contains(mouse)) {
                diHighScore = false;
                diMainMenu = true;
                RefreshLayar();
                this.Invalidate();
            }
        }
        private void timer2_Tick(object sender, EventArgs e) {
            Random r = new Random();
            int jumlah = r.Next(1, 4);
            if (jumlah == 3) {
                arr.Add(new kiri(this.Height-50));
                arr.Add(new kananTengah(this.Height - 50, 150));
                arr.Add(new kananTengah(this.Height - 50, 300));
            } else if (jumlah == 2) {
                arr.Add(new kiri(this.Height-50));
                arr.Add(new kananTengah(this.Height - 50, r.Next(1, 3) * 150));
            } else {
                if (r.Next(2) == 0) {
                    arr.Add(new kiri(this.Height-50));
                } else {
                    arr.Add(new kananTengah(this.Height - 50, r.Next(1, 3) * 150));
                }
            }
            this.Invalidate();   
        }
    }

    //CLASS PLAYER
    public class player {
        public Image[,] Gbr { get; set; }
        public Rectangle kiri, tengah, kanan;
        public Rectangle kiriAtas, tengahAtas, kananAtas;
        public int Skor { get; set; }
        public int Life { get; set; }
        private int gerakan;
        public int Gerakan {
            get { return gerakan; }
            set {
                if (value > 1) value %= 1;
                gerakan = value;
            }
        }
        public int Arah { get; set; }
        public player() {
            Gbr = new Image[5, 2];
            Gbr[0, 0] = global::tugasPrak4.Properties.Resources.idle1;
            Gbr[0, 1] = global::tugasPrak4.Properties.Resources.idle2;
            Gbr[1, 0] = global::tugasPrak4.Properties.Resources._13;
            Gbr[1, 1] = global::tugasPrak4.Properties.Resources._12;
            Gbr[2, 0] = global::tugasPrak4.Properties.Resources.hitR1;
            Gbr[2, 1] = global::tugasPrak4.Properties.Resources.hitR2;
            Gbr[3, 0] = global::tugasPrak4.Properties.Resources.hitL1;
            Gbr[3, 1] = global::tugasPrak4.Properties.Resources.hitL2;
            Gbr[4, 0] = global::tugasPrak4.Properties.Resources._2;
            Gbr[4, 1] = global::tugasPrak4.Properties.Resources._3;
            //0 idle, 1 kiri, 2 tengah, 3 kanan
            Skor = 0; Life = 3;
            kiri = new Rectangle(0, 180, 150, 150);
            tengah = new Rectangle(150, 180, 150, 150);
            kanan = new Rectangle(300, 180, 150, 150);
            kiriAtas = new Rectangle(0, 150, 150, 50);
            kananAtas = new Rectangle(300, 150, 150, 50);
            tengahAtas = new Rectangle(150, 150, 150, 50);
        }
    }
    //CLASS MUSUH
    public class musuh {
        public Image[,] gbr;
        public Rectangle keliling;
        public int gerakan = 0;
        public int y, x;
        public bool hit;
        public musuh() {
            gbr = new Image[2, 2];
            //0 jalan, 1 kepukul
            keliling = new Rectangle();
        }
        public void setRect(Rectangle r) {
            keliling = r;
        }
        public Rectangle getRect() {
            return keliling;
        }
    }
    //LANE KIRI
    public class kiri : musuh {
        public kiri(int y) {
            gbr[0, 0] = global::tugasPrak4.Properties.Resources._5;
            gbr[0, 1] = global::tugasPrak4.Properties.Resources._8;
            gbr[1, 0] = global::tugasPrak4.Properties.Resources._22;
            gbr[1, 1] = global::tugasPrak4.Properties.Resources._22;
            this.y = y;
        }
    }
    //LANE KANAN dan TENGAH
    public class kananTengah : musuh {
        public kananTengah(int y, int x) {
            gbr[0, 0] = global::tugasPrak4.Properties.Resources._6;
            gbr[0, 1] = global::tugasPrak4.Properties.Resources._10;
            gbr[1, 0] = global::tugasPrak4.Properties.Resources._21;
            gbr[1, 1] = global::tugasPrak4.Properties.Resources._21;
            this.y = y;
            this.x = x;
        }
    }
}
