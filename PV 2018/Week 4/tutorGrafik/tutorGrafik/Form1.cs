﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorGrafik
{
    public partial class Form1 : Form
    {
        Graphics g;
        List<int> x, y, dx, dy;
        List<Color> cc;
        Color[] arColor;
        Image[] sprites;
        int counter = 0;
        
        Random r;
        public Form1()
        {
            InitializeComponent();
            x = new List<int>();
            y = new List<int>();
            dx = new List<int>();
            dy = new List<int>();
            cc = new List<Color>();
            arColor = new Color[3];
            arColor[0] = Color.FromArgb(255, 0, 0);
            arColor[1] = Color.FromArgb(0, 255, 0);
            arColor[2] = Color.FromArgb(0, 0, 255);
            r = new Random();
            sprites = new Image[4];
            sprites[0] = Image.FromFile("spp2.png");
            sprites[1] = Image.FromFile("spp3.png");
            sprites[2] = Image.FromFile("spp4.png");
            sprites[3] = Image.FromFile("spp5.png");
            foreach (var item in sprites)
            {
                item.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            Invalidate();
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                x.Add(r.Next(this.Width - 30));
                y.Add(r.Next(this.Height - 30));
                dx.Add(r.Next(2) * 2 - 1);
                dy.Add(r.Next(2) * 2 - 1);
                cc.Add(arColor[r.Next(3)]);
                this.Invalidate();
            }
            else if (e.Button == MouseButtons.Left)
            {
                int posX = e.X; int posY = e.Y;
                for (int i = x.Count - 1; i >= 0; i--)
                {
                    if ((new Rectangle(x[i], y[i], 30, 30).Contains(new Point(posX, posY))))
                    {
                        x.RemoveAt(i);
                        y.RemoveAt(i);
                        cc.RemoveAt(i);
                        dx.RemoveAt(i);
                        dy.RemoveAt(i);
                    }
                }
                Invalidate();
            }
        }

        private void gerakbunder_Tick(object sender, EventArgs e)
        {
            ++counter;
            counter %= 4;
            for (int i = x.Count - 1; i >= 0; i--)
            {
                if (x[i] + 5 * dx[i] < 0 || x[i] + 5 * dx[i] + 30 > Width)
                {
                    dx[i] *= -1;
                }
                if (y[i] + 5 * dy[i] < 0 || y[i] + 5 * dy[i] + 30 > Height)
                {
                    dy[i] *= -1;
                }
                x[i] += 5 * dx[i]; y[i] += 5 * dy[i];
            }
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            for (int i = x.Count-1; i >=0; i--)
            {
                g.FillEllipse(new SolidBrush(cc[i]), new Rectangle(x[i], y[i], 30, 30));
            }
            g.DrawImage(sprites[counter], 10, 10, 100, 100);
        }
    }
}
