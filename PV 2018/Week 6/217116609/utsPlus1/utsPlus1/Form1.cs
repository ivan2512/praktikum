﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Windows.Forms;
using perpustakaanKelas;
using System.IO;

namespace utsPlus1 {
    public partial class Form1 : Form {
        List<Mahasiswa> data;
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            data = new List<Mahasiswa>();
            refreshListBox();
        }

        private void refreshListBox() {
            listBox1.DataSource = null;
            listBox1.DataSource = data;
        }

        private void insert_Click(object sender, EventArgs e) {
            if (teksboksNama.Text == "" || teksboksNrp.Text == "") {
                MessageBox.Show("Data tidak boleh kosong");
            } else {
                data.Add(new Mahasiswa(nrp: teksboksNrp.Text, nama: teksboksNama.Text));
                refreshListBox();
            }
        }

        private void save_Click(object sender, EventArgs e) {
            DialogResult temp = saveFileDialog1.ShowDialog();
            if (temp == DialogResult.OK) {
                string s = saveFileDialog1.FileName;
                XmlDocument doc = new XmlDocument();
                XmlNode listMahasiswa = doc.CreateElement("listmahasiswa");
                doc.AppendChild(listMahasiswa);

                for (int i = 0; i < data.Count; i++) {
                    XmlElement mahasiswa = doc.CreateElement("mahasiswa");
                    XmlAttribute nrp = doc.CreateAttribute("NRP");
                    nrp.Value = data[i].Nrp;
                    mahasiswa.Attributes.Append(nrp);
                    //mahasiswa.SetAttribute("nrp", data[i].Nrp);
                    mahasiswa.InnerText = data[i].Nama;
                    listMahasiswa.AppendChild(mahasiswa);
                }
                doc.Save(s);
            }
        }

        private void open_Click(object sender, EventArgs e) {
            DialogResult temp = openFileDialog1.ShowDialog();
            if (temp == DialogResult.OK) {
                string s = openFileDialog1.FileName;
                XmlDocument doc = new XmlDocument();
                doc.Load(s);

                XmlNode _listmahasiswa = doc.ChildNodes[0];
                data.Clear();
                for (int i = 0; i < _listmahasiswa.ChildNodes.Count; i++) {
                    XmlElement _mahasiswa = (XmlElement)_listmahasiswa.ChildNodes[i];
                    Mahasiswa m = new Mahasiswa(nrp: _mahasiswa.GetAttribute("NRP"), nama: _mahasiswa.InnerText);
                    data.Add(m);
                }
                refreshListBox();

                //data.Clear();
                //XmlNodeList _nodeList = doc.GetElementsByTagName("mahasiswa");
                //for (int i = 0; i < _nodeList.Count; i++) {
                //    XmlElement _mahasiswa = (XmlElement)_nodeList[i];
                //    Mahasiswa m = new Mahasiswa(nrp: _mahasiswa.GetAttribute("NRP"), nama: _mahasiswa.InnerText);
                //    data.Add(m);
                //}
                //refreshListBox();
            }
        }

        private void delete_Click(object sender, EventArgs e) {
            if (listBox1.SelectedIndex > -1) {
                data.RemoveAt(listBox1.SelectedIndex);
                refreshListBox();
            }
        }

        private void openText_Click(object sender, EventArgs e) {
            StreamReader baca = new StreamReader("data.txt");
            data.Clear();

            for (int i = 0; !baca.EndOfStream; i++) { 
                string s = baca.ReadLine();
                string[] attr = s.Split('-');
                Mahasiswa m = new Mahasiswa(nama: attr[1], nrp: attr[0]);
                data.Add(m);
            }
            refreshListBox();
            baca.Close();
        }

        private void saveText_Click(object sender, EventArgs e) {
            StreamWriter tulis = new StreamWriter("data.txt");
            for (int i = 0; i < data.Count; i++) {
                tulis.WriteLine(data[i].Nrp+"-"+data[i].Nama);
            }


            tulis.Close();
        }
    }
}
