﻿namespace utsPlus1 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.teksboksNrp = new System.Windows.Forms.TextBox();
            this.teksboksNama = new System.Windows.Forms.TextBox();
            this.insert = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.delete = new System.Windows.Forms.Button();
            this.open = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.saveText = new System.Windows.Forms.Button();
            this.openText = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NRP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama";
            // 
            // teksboksNrp
            // 
            this.teksboksNrp.Location = new System.Drawing.Point(66, 10);
            this.teksboksNrp.Name = "teksboksNrp";
            this.teksboksNrp.Size = new System.Drawing.Size(115, 20);
            this.teksboksNrp.TabIndex = 2;
            // 
            // teksboksNama
            // 
            this.teksboksNama.Location = new System.Drawing.Point(66, 38);
            this.teksboksNama.Name = "teksboksNama";
            this.teksboksNama.Size = new System.Drawing.Size(115, 20);
            this.teksboksNama.TabIndex = 3;
            // 
            // insert
            // 
            this.insert.Location = new System.Drawing.Point(212, 10);
            this.insert.Name = "insert";
            this.insert.Size = new System.Drawing.Size(75, 23);
            this.insert.TabIndex = 4;
            this.insert.Text = "Insert";
            this.insert.UseVisualStyleBackColor = true;
            this.insert.Click += new System.EventHandler(this.insert_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 76);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(407, 290);
            this.listBox1.TabIndex = 5;
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(212, 39);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(75, 23);
            this.delete.TabIndex = 6;
            this.delete.Text = "Delete";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // open
            // 
            this.open.Location = new System.Drawing.Point(313, 10);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(106, 23);
            this.open.TabIndex = 7;
            this.open.Text = "Open from XML";
            this.open.UseVisualStyleBackColor = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(313, 41);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(106, 23);
            this.save.TabIndex = 8;
            this.save.Text = "Save to XML";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "XML Files|*.xml";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "XML Files|*.xml";
            // 
            // saveText
            // 
            this.saveText.Location = new System.Drawing.Point(435, 43);
            this.saveText.Name = "saveText";
            this.saveText.Size = new System.Drawing.Size(106, 23);
            this.saveText.TabIndex = 10;
            this.saveText.Text = "Save to TXT";
            this.saveText.UseVisualStyleBackColor = true;
            this.saveText.Click += new System.EventHandler(this.saveText_Click);
            // 
            // openText
            // 
            this.openText.Location = new System.Drawing.Point(435, 12);
            this.openText.Name = "openText";
            this.openText.Size = new System.Drawing.Size(106, 23);
            this.openText.TabIndex = 9;
            this.openText.Text = "Open from TXT";
            this.openText.UseVisualStyleBackColor = true;
            this.openText.Click += new System.EventHandler(this.openText_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 375);
            this.Controls.Add(this.saveText);
            this.Controls.Add(this.openText);
            this.Controls.Add(this.save);
            this.Controls.Add(this.open);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.insert);
            this.Controls.Add(this.teksboksNama);
            this.Controls.Add(this.teksboksNrp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox teksboksNrp;
        private System.Windows.Forms.TextBox teksboksNama;
        private System.Windows.Forms.Button insert;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button open;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button saveText;
        private System.Windows.Forms.Button openText;
    }
}

