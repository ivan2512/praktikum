﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace perpustakaanKelas {
    [Serializable]
    public class Mahasiswa {
        public string Nama { get; set; }
        public string Nrp { get; set; }

        public Mahasiswa(string nama, string nrp) {
            Nama = nama;
            Nrp = nrp;
        }

        public override string ToString() {
            return Nrp + " - " + Nama;
        }
    }
}
