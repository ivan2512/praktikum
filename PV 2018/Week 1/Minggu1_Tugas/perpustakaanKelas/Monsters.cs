﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perpustakaanKelas {
    public class Monsters {
        private int maxHP;
        private int currHP;
        public int Attack { get; private set; }
        public int Defense { get; private set; }
        public string Name { get; private set; }
        private string weakEle;
        private string strongEle;
        public int MaxHP {
            get {
                return maxHP;
            }
            private set {
            } }
        public string Resistance {
            get { return strongEle; }
            set { strongEle = value; }
        }
        public string Weakness {
            get { return weakEle; }
            set { weakEle = value; }
        }
        public int CurrHP {
            get { return currHP; }
            set {
                if (value >= 0 && value <= maxHP) currHP = value;
                else if (value < 0) currHP = 0;
            }
        }
        public int Reward { get; private set; }
        public Monsters(int i) {
            switch (i) {
                case 0:
                    Name = "Rathian";
                    maxHP = 4250;
                    Attack = 75;
                    Defense = 10;
                    Weakness = "Thunder";
                    Resistance = "Poison";
                    Reward = 5300;
                    break;
                case 1:
                    Name = "Rathalos";
                    maxHP = 4785;
                    Attack = 85;
                    Defense = 15;
                    Weakness = "Thunder";
                    Resistance = "Fire";
                    Reward = 7600;
                    break;
                default:
                    Name = "Nergigante";
                    maxHP = 5000;
                    Attack = 100;
                    Defense = 20;
                    Weakness = "Dragon";
                    Resistance = "Fire";
                    Reward = 10000;
                    break;
            }
            CurrHP = maxHP;

        }
        public override string ToString() {
            return this.Name;
        }
    }
}
