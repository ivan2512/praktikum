﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perpustakaanKelas {
    public class Items {
        public string Name { get; set; }
        public int Price { get;  set; }
        public Items() { }
        public override string ToString() {
            return Name;
        }
    }

    public class Potion : Items {
        public Potion() {
            Name = "Potion";
            Price = 100;
        }
    }

    public class MegaPotion : Items {
        public MegaPotion() {
            Name = "Mega Potion";
            Price = 200;
        }
    }

    public class MaxPotion : Items {
        public MaxPotion() {
            Name = "Max Potion";
            Price = 500;
        }
    }

    public class DemonDrug : Items {
        public DemonDrug() {
            Name = "Demon Drug";
            Price = 1000;
        }
    }

    public class Armorskin : Items {
        public Armorskin() {
            Name = "Armorskin";
            Price = 1000;
        }
    }
}
