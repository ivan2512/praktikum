﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perpustakaanKelas {
    public class Weapon {
        public string Name { get; private set; }
        private int rawAtt;
        public int Raw_Attack {
            get { return rawAtt; }
            set { rawAtt = value; }
        }

        private int elmAtt;
        public int Element_Attack {
            get { return elmAtt; }
            set { elmAtt = value; }
        }

        public string Element { get; set; }
        private Boolean upgraded;
        public Weapon(int i) {
            upgraded = false;
            switch (i) {
                case 0:
                    Name = "Hammer";
                    Raw_Attack = 550;
                    Element = "Fire";
                    break;
                case 1:
                    Name = "Greatsword";
                    Raw_Attack = 625;
                    Element = "Poison";
                    break;
                case 2:
                    Name = "Long Sword";
                    Raw_Attack = 600;
                    Element = "Dragon";
                    break;
                case 3:
                    Name = "Switch Axe";
                    Raw_Attack = 620;
                    Element = "Thunder";
                    break;
                default:
                    throw new Exception("Wrong index");
            }
        }

        public void upgrade() {
            if (upgraded) {
                Element_Attack = Element_Attack + 10;
            }
            else {
                Element_Attack = 100;
                Name = Element +" "+ Name;
                upgraded = true;
            }
        }


        public override string ToString() {
            return Name;
        }
    }
}
