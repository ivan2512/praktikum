﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using perpustakaanKelas;

namespace MHW_TugasPrak1 {
    public partial class Form1 : Form {
        List<Weapon> senjataku;
        List<Weapon> clonan;
        List<Monsters> musuhku;
        List<perpustakaanKelas.Items> shop;
        List<perpustakaanKelas.Items> bought;
        Weapon dipake;
        int uang, maxPHealth, currHealth;
        Monsters dilawan;
        int ctrDD, ctrAs, currDef;
        public Form1() {
            InitializeComponent();
            uang = 5000;
            maxPHealth = 1000;
            currHealth = 1000;
        }

        private void Form1_Load(object sender, EventArgs e) {
            goldLabel.Text = uang + "";
            rawLabel.Text = "";
            elminfo.Text = "";
            element.Text = "";
            groupBox3.Enabled = false;
            senjataku = new List<Weapon>();
            musuhku = new List<Monsters>();
            bought = new List<Items>();
            shop = new List<Items>();
            for (int i = 0; i < 3; i++) {
                senjataku.Add(new Weapon(i));
                musuhku.Add(new Monsters(i));
            }
            shop.Add(new Potion());
            shop.Add(new MegaPotion());
            shop.Add(new MaxPotion());
            shop.Add(new DemonDrug());
            shop.Add(new Armorskin());
            senjataku.Add(new Weapon(3));
            itemshop.DataSource = null;
            itemshop.DataSource = shop;
            pilihMonster.DataSource = null;
            pilihMonster.DataSource = musuhku;
            wpCBox.DataSource = null;
            wpCBox.DataSource = senjataku;
            clonan = senjataku.ToList();
            comboBox1.DataSource = clonan;
            currDef = 25;
        }
        
        private void button2_Click(object sender, EventArgs e) {
            if (wpCBox.SelectedIndex != -1)
            {
                if (uang >= 500) {
                    senjataku[wpCBox.SelectedIndex].upgrade();
                    uang -= 500;
                } else {
                    MessageBox.Show("Uang tidak mencukupi");
                }
            }
            refresh();
        }

        private void wpCBox_SelectedIndexChanged(object sender, EventArgs e) {
            if (wpCBox.SelectedIndex != -1) {
                Weapon item = senjataku[wpCBox.SelectedIndex];
                rawLabel.Text = item.Raw_Attack + "";
                elminfo.Text = item.Element_Attack + "";
                element.Text = item.Element;
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            if (itemshop.SelectedIndex != -1) {
                int i = itemshop.SelectedIndex;
                switch (i) {
                    case 0:
                        if (uang >= 100) {
                            uang -= 100;
                            bought.Add(shop[i]);
                        } else {
                            MessageBox.Show("Uang tidak mencukupi");
                        }
                        break;
                    case 1:
                        if (uang >= 200) {
                            uang -= 200;
                            bought.Add(shop[i]);
                        } else {
                            MessageBox.Show("Uang tidak mencukupi");
                        }
                        break;
                    case 2:
                        if (uang >= 500) {
                            uang -= 500;
                            bought.Add(shop[i]);
                        } else {
                            MessageBox.Show("Uang tidak mencukupi");
                        }
                        break;
                    case 3:
                    case 4:
                        if (uang >= 1000) {
                            uang -= 1000;
                            bought.Add(shop[i]);
                        } else {
                            MessageBox.Show("Uang tidak mencukupi");
                        }
                        break;
                }
                refresh();
            }
        }

        private void changestate() {
            groupBox3.Enabled = !groupBox3.Enabled;
            groupBox1.Enabled = !groupBox1.Enabled;
            groupBox2.Enabled = !groupBox2.Enabled;
            pilihMonster.Enabled = !pilihMonster.Enabled;
            comboBox1.Enabled = !comboBox1.Enabled;
        }

        private void button6_Click(object sender, EventArgs e) {
            if (listBox1.SelectedIndex != -1) {
                int i = listBox1.SelectedIndex;
                textBox1.Text = "Player use "+bought[i].Name+"\r\n" + textBox1.Text;
                if (bought[i].Name.Contains("Potion")) {
                    if (bought[i].Name.Contains("Max"))
                        currHealth = maxPHealth;
                    else if (bought[i].Name.Contains("Mega"))
                        currHealth = currHealth + (int)(0.5 * maxPHealth);
                    else
                        currHealth = currHealth + (int)(0.2 * maxPHealth);
                    if (currHealth > maxPHealth) currHealth = maxPHealth;
                    playerHealth.Text = currHealth + "/" + maxPHealth;
                }
                else if (bought[i].Name.Equals("Demon Drug")) {
                    ctrDD = 3; }
                else if (bought[i].Name.Equals("Armorskin")) {
                    ctrAs = 3; }
                bought.RemoveAt(i);
                listBox1.DataSource = null;
                listBox1.DataSource = bought;
                double dmg;
                if (ctrAs > 0) {
                    ctrAs--;
                    dmg = dilawan.Attack - (1.2 * currDef);
                    if (ctrAs == 0) {
                        textBox1.Text = "The effect of Armorskin has expired\r\n" + textBox1.Text;
                    }
                } else {
                    dmg = dilawan.Attack - currDef;
                }
                textBox1.Text = dilawan.Name + " hit player with " + dmg + " damage\r\n" + textBox1.Text;
                currHealth -= (int)dmg;
                if (currHealth < 0) {
                    currHealth = 0;
                    changestate();
                    textBox1.Text = "Player lost!\r\n" + textBox1.Text;
                }
                playerHealth.Text = currHealth + "/" + maxPHealth;
            }
        }

        private void button5_Click(object sender, EventArgs e) {
            double dmg; string s = "";
            if (dipake.Element.Equals(dilawan.Weakness)) {
                dmg = dipake.Raw_Attack + (0.5 * dipake.Element_Attack);
            } else if (dipake.Element.Equals(dilawan.Resistance)) {
                dmg = dipake.Raw_Attack;
            } else {
                dmg = dipake.Raw_Attack + (0.25 * dipake.Element_Attack);
            }
            if (ctrDD > 0) {
                dmg = 1.2 * dmg;
                ctrDD--;
                if (ctrDD == 0) {
                    s = "The effect of Demon Drug has expired\r\n";
                }
            }
            dmg -= dilawan.Defense;
            dilawan.CurrHP = dilawan.CurrHP - (int)dmg;
            textBox1.Text = "Player hit " + dilawan.ToString() + " with " + (int)dmg + " damage\r\n"+textBox1.Text;
            hpMusuh.Text = dilawan.CurrHP + "/" + dilawan.MaxHP;
            textBox1.Text = s + textBox1.Text;
            if(dilawan.CurrHP == 0) {
                textBox1.Text = "The battle has ended, Player get " + dilawan.Reward + " gold\r\n" + textBox1.Text;
                changestate();
                uang += dilawan.Reward;
                refresh();
            } else {
                if (ctrAs > 0) {
                    ctrAs--;
                    dmg = dilawan.Attack - (1.2 * currDef);
                    if (ctrAs == 0) {
                        textBox1.Text = "The effect of Armorskin has expired\r\n" + textBox1.Text;
                    }
                } else {
                    dmg = dilawan.Attack - currDef;
                }
                textBox1.Text = dilawan.Name + " hit player with " + dmg + " damage\r\n"+textBox1.Text ;
                currHealth -= (int)dmg;
                playerHealth.Text = currHealth + "/" + maxPHealth;
            }
        }

        private void battleButton_Click(object sender, EventArgs e) {
            if (comboBox1.SelectedIndex!=-1) {
                currHealth = maxPHealth;
                textBox1.Clear();
                ctrAs = 0; ctrDD = 0;
                changestate();
                dipake = senjataku[comboBox1.SelectedIndex];
                playerDef.Text = currDef+"";
                playerHealth.Text = currHealth + "/" + maxPHealth;
                weaponUse.Text = dipake.ToString();
                attack.Text = dipake.Raw_Attack + "";
                elmAttack.Text = dipake.Element_Attack + "";
                dilawan = musuhku[pilihMonster.SelectedIndex];
                dilawan.CurrHP = dilawan.MaxHP;
                currHealth = maxPHealth;
                namaMonster.Text = dilawan.ToString();
                hpMusuh.Text = dilawan.CurrHP + "/" + dilawan.MaxHP;
                atkMusuh.Text = dilawan.Attack+"";
                defMusuh.Text = dilawan.Defense + "";
            } else {
                MessageBox.Show("No selected weapon");
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            if (wpCBox.SelectedIndex != -1) {
                if (uang >= 500) {
                    senjataku[wpCBox.SelectedIndex].Raw_Attack += 10;
                    uang -= 500;
                } else {
                    MessageBox.Show("Uang tidak mencukupi");
                }
            }
            refresh();
        }

        private void refresh() {
            goldLabel.Text = uang + "";
            clonan = senjataku.ToList();
            comboBox1.DataSource = clonan;
            if (wpCBox.SelectedIndex != -1) {
                wpCBox.DataSource = null;
                wpCBox.DataSource = senjataku;
                clonan = senjataku.ToList();
                comboBox1.DataSource = clonan;
            }
            listBox1.DataSource = null;
            listBox1.DataSource = bought;
        }
    }
}