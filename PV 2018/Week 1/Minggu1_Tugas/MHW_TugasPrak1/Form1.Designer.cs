﻿namespace MHW_TugasPrak1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.goldLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.element = new System.Windows.Forms.Label();
            this.elminfo = new System.Windows.Forms.Label();
            this.rawLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.wpCBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.itemshop = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pilihMonster = new System.Windows.Forms.ComboBox();
            this.battleButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.playerDef = new System.Windows.Forms.Label();
            this.playerHealth = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label18 = new System.Windows.Forms.Label();
            this.defMusuh = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.atkMusuh = new System.Windows.Forms.Label();
            this.hpMusuh = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.namaMonster = new System.Windows.Forms.Label();
            this.elmAttack = new System.Windows.Forms.Label();
            this.attack = new System.Windows.Forms.Label();
            this.weaponUse = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Gold: ";
            // 
            // goldLabel
            // 
            this.goldLabel.AutoSize = true;
            this.goldLabel.Location = new System.Drawing.Point(54, 13);
            this.goldLabel.Name = "goldLabel";
            this.goldLabel.Size = new System.Drawing.Size(35, 13);
            this.goldLabel.TabIndex = 1;
            this.goldLabel.Text = "label2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.element);
            this.groupBox1.Controls.Add(this.elminfo);
            this.groupBox1.Controls.Add(this.rawLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.wpCBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(16, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 136);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Weapon Upgrade";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(112, 94);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "ELM UP";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "RAW UP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // element
            // 
            this.element.AutoSize = true;
            this.element.Location = new System.Drawing.Point(270, 77);
            this.element.Name = "element";
            this.element.Size = new System.Drawing.Size(35, 13);
            this.element.TabIndex = 7;
            this.element.Text = "label6";
            // 
            // elminfo
            // 
            this.elminfo.AutoSize = true;
            this.elminfo.Location = new System.Drawing.Point(267, 49);
            this.elminfo.Name = "elminfo";
            this.elminfo.Size = new System.Drawing.Size(35, 13);
            this.elminfo.TabIndex = 6;
            this.elminfo.Text = "label6";
            // 
            // rawLabel
            // 
            this.rawLabel.AutoSize = true;
            this.rawLabel.Location = new System.Drawing.Point(267, 22);
            this.rawLabel.Name = "rawLabel";
            this.rawLabel.Size = new System.Drawing.Size(35, 13);
            this.rawLabel.TabIndex = 5;
            this.rawLabel.Text = "label6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Element";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(206, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Elm Atk: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Raw Atk: ";
            // 
            // wpCBox
            // 
            this.wpCBox.FormattingEnabled = true;
            this.wpCBox.Location = new System.Drawing.Point(68, 20);
            this.wpCBox.Name = "wpCBox";
            this.wpCBox.Size = new System.Drawing.Size(121, 21);
            this.wpCBox.TabIndex = 1;
            this.wpCBox.SelectedIndexChanged += new System.EventHandler(this.wpCBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Weapon: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.itemshop);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(373, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(278, 136);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Item Store";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(106, 94);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "BUY";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // itemshop
            // 
            this.itemshop.FormattingEnabled = true;
            this.itemshop.Location = new System.Drawing.Point(84, 33);
            this.itemshop.Name = "itemshop";
            this.itemshop.Size = new System.Drawing.Size(121, 21);
            this.itemshop.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Item: ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(209, 217);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(148, 217);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Weapon: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(351, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Monster: ";
            // 
            // pilihMonster
            // 
            this.pilihMonster.FormattingEnabled = true;
            this.pilihMonster.Location = new System.Drawing.Point(408, 217);
            this.pilihMonster.Name = "pilihMonster";
            this.pilihMonster.Size = new System.Drawing.Size(121, 21);
            this.pilihMonster.TabIndex = 13;
            // 
            // battleButton
            // 
            this.battleButton.Location = new System.Drawing.Point(151, 244);
            this.battleButton.Name = "battleButton";
            this.battleButton.Size = new System.Drawing.Size(378, 23);
            this.battleButton.TabIndex = 10;
            this.battleButton.Text = "BATTLE";
            this.battleButton.UseVisualStyleBackColor = true;
            this.battleButton.Click += new System.EventHandler(this.battleButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.playerDef);
            this.groupBox3.Controls.Add(this.playerHealth);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.defMusuh);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.atkMusuh);
            this.groupBox3.Controls.Add(this.hpMusuh);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.namaMonster);
            this.groupBox3.Controls.Add(this.elmAttack);
            this.groupBox3.Controls.Add(this.attack);
            this.groupBox3.Controls.Add(this.weaponUse);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(16, 287);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(635, 472);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // playerDef
            // 
            this.playerDef.AutoSize = true;
            this.playerDef.Location = new System.Drawing.Point(41, 69);
            this.playerDef.Name = "playerDef";
            this.playerDef.Size = new System.Drawing.Size(35, 13);
            this.playerDef.TabIndex = 22;
            this.playerDef.Text = "label6";
            // 
            // playerHealth
            // 
            this.playerHealth.AutoSize = true;
            this.playerHealth.Location = new System.Drawing.Point(41, 42);
            this.playerHealth.Name = "playerHealth";
            this.playerHealth.Size = new System.Drawing.Size(35, 13);
            this.playerHealth.TabIndex = 21;
            this.playerHealth.Text = "label6";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Location = new System.Drawing.Point(10, 262);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(619, 204);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "History";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 19);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(607, 179);
            this.textBox1.TabIndex = 0;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(146, 160);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 19;
            this.button6.Text = "Use Item";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 160);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 18;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 135);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Inventory";
            // 
            // defMusuh
            // 
            this.defMusuh.AutoSize = true;
            this.defMusuh.Location = new System.Drawing.Point(506, 96);
            this.defMusuh.Name = "defMusuh";
            this.defMusuh.Size = new System.Drawing.Size(35, 13);
            this.defMusuh.TabIndex = 12;
            this.defMusuh.Text = "label6";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(459, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Atk: ";
            // 
            // atkMusuh
            // 
            this.atkMusuh.AutoSize = true;
            this.atkMusuh.Location = new System.Drawing.Point(503, 68);
            this.atkMusuh.Name = "atkMusuh";
            this.atkMusuh.Size = new System.Drawing.Size(35, 13);
            this.atkMusuh.TabIndex = 11;
            this.atkMusuh.Text = "label6";
            // 
            // hpMusuh
            // 
            this.hpMusuh.AutoSize = true;
            this.hpMusuh.Location = new System.Drawing.Point(503, 41);
            this.hpMusuh.Name = "hpMusuh";
            this.hpMusuh.Size = new System.Drawing.Size(35, 13);
            this.hpMusuh.TabIndex = 10;
            this.hpMusuh.Text = "label6";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(458, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Def: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(460, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "HP: ";
            // 
            // namaMonster
            // 
            this.namaMonster.AutoSize = true;
            this.namaMonster.Location = new System.Drawing.Point(460, 16);
            this.namaMonster.Name = "namaMonster";
            this.namaMonster.Size = new System.Drawing.Size(62, 13);
            this.namaMonster.TabIndex = 14;
            this.namaMonster.Text = "placeholder";
            // 
            // elmAttack
            // 
            this.elmAttack.AutoSize = true;
            this.elmAttack.Location = new System.Drawing.Point(220, 68);
            this.elmAttack.Name = "elmAttack";
            this.elmAttack.Size = new System.Drawing.Size(35, 13);
            this.elmAttack.TabIndex = 13;
            this.elmAttack.Text = "label6";
            // 
            // attack
            // 
            this.attack.AutoSize = true;
            this.attack.Location = new System.Drawing.Point(220, 41);
            this.attack.Name = "attack";
            this.attack.Size = new System.Drawing.Size(35, 13);
            this.attack.TabIndex = 12;
            this.attack.Text = "label6";
            // 
            // weaponUse
            // 
            this.weaponUse.AutoSize = true;
            this.weaponUse.Location = new System.Drawing.Point(158, 15);
            this.weaponUse.Name = "weaponUse";
            this.weaponUse.Size = new System.Drawing.Size(54, 13);
            this.weaponUse.TabIndex = 4;
            this.weaponUse.Text = "Weapon: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(159, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 11;
            this.label15.Text = "Elm Atk: ";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(7, 94);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 3;
            this.button5.Text = "Attack";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(159, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Raw Atk: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 68);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Def: ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "HP: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Player";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 771);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.battleButton);
            this.Controls.Add(this.pilihMonster);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.goldLabel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "MHW";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label goldLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox wpCBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label element;
        private System.Windows.Forms.Label elminfo;
        private System.Windows.Forms.Label rawLabel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox itemshop;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox pilihMonster;
        private System.Windows.Forms.Button battleButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label elmAttack;
        private System.Windows.Forms.Label attack;
        private System.Windows.Forms.Label weaponUse;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label namaMonster;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label defMusuh;
        private System.Windows.Forms.Label atkMusuh;
        private System.Windows.Forms.Label hpMusuh;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label playerDef;
        private System.Windows.Forms.Label playerHealth;
    }
}

