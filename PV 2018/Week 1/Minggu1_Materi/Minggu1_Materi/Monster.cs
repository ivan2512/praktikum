﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minggu1_Materi {
    class Monster {
        public Monster Parent1 { get; set; }
        public Monster Parent2 { get; set; }
        public string Nama { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        Random r;

        public Monster(string nama, string type) {
            this.Nama = nama;
            this.Type1 = type;
            Type2 = string.Empty;
            Parent1 = null;
            Parent2 = null;
        }
        public Monster(string nama, Monster p1, Monster p2) {
            this.Nama = nama;
            Parent1 = p1;
            Parent2 = p2;

            //KODINGAN NENTUIN TYPE GA PUENTING BANGET DAN SEK BUGGY
            if (p1.Type2==string.Empty && p2.Type2 == string.Empty) {
                if (p1.Type1 == p2.Type2) {
                    Type1 = p1.Type1;
                } else {
                    Type1 = p1.Type1;
                    Type2 = p2.Type1;
                }
            } else {
                if (p1.Type2 == string.Empty) {
                    if (p1.Type1 == p2.Type1 || p1.Type1 == p2.Type2) {
                        Type1 = p1.Type1;
                        Type2 = (p1.Type1 == p2.Type1) ? p2.Type2 : p2.Type1;
                    } else {
                        Type1 = p1.Type1;
                        r = new Random();
                        int randoman = r.Next(1, 3);
                        if (randoman == 1) {
                            Type2 = p2.Type2;
                        } else {
                            Type2 = p2.Type1;
                        }
                    }
                } else if (p2.Type2 == string.Empty) {
                    if (p2.Type1 == p1.Type1 || p2.Type1 == p1.Type2) {
                        Type1 = p2.Type1;
                        Type2 = (p2.Type1 == p1.Type1) ? p1.Type2 : p1.Type1;
                    } else {
                        Type1 = p2.Type1;
                        r = new Random();
                        int randoman = r.Next(1, 3);
                        if (randoman == 1) {
                            Type2 = p1.Type2;
                        } else {
                            Type2 = p1.Type1;
                        }
                    }
                } else {
                    r = new Random();
                    int r1, r2;
                    r1 = r.Next(1, 3);
                    r2 = r.Next(1, 3);
                    Type1 = (r1 == 1) ? p1.Type1 : p1.Type2;
                    Type2 = r2 == 1 ? p2.Type1 : p2.Type2;
                    if (Type1 == Type2) {
                        Type2 = string.Empty;
                    }
                }
            }
        }

        public override string ToString() {
            return this.Nama;
        }
    }
}
