﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Minggu1_Materi {
    public partial class Form1 : Form {
        List<Monster> monsters;
        int uang;

        public Form1() {
            InitializeComponent();
            uang = 1000;
            monsters = new List<Monster>();
            typemonsterbeli.Items.Add("Fire");
            typemonsterbeli.Items.Add("Air");
            typemonsterbeli.Items.Add("Water");
            Perbarui();
        }

        void Perbarui() {
            money.Text = uang.ToString();
            jumlahmonster.Text = monsters.Count.ToString();
            namamonsterjual.DataSource = null;
            namamonsterjual.DataSource = monsters;
            detailmonster.DataSource = null;
            detailmonster.DataSource = monsters.ToArray();
            monster1.DataSource = null;
            monster1.DataSource = monsters.ToArray();
        }

        private void buy_Click(object sender, EventArgs e) {
            if(namamonsterbeli.Text!=string.Empty && uang >= 500 && typemonsterbeli.SelectedIndex!=-1) {
                string nama = namamonsterbeli.Text;
                string type = typemonsterbeli.Items[typemonsterbeli.SelectedIndex].ToString();
                monsters.Add(new Monster(nama, type));
                uang -= 500;
                Perbarui();
            } else {
                if (namamonsterbeli.Text == string.Empty) {
                    MessageBox.Show("Nama ga boleh kosong");
                } else if (uang < 500) {
                    MessageBox.Show("Uang ga cukup");
                } else {
                    MessageBox.Show("Pilih dari 3 type yang ada");
                }
            }
        }

        private void sell_Click(object sender, EventArgs e) {
            if (namamonsterjual.SelectedIndex > -1) {
                int index = namamonsterjual.SelectedIndex;
                Monster temp = (Monster)namamonsterjual.Items[index];
                if (temp.Type2 == string.Empty) {
                    uang += 200;
                } else {
                    uang += 450;
                }
                monsters.Remove(temp);
                Perbarui();
            }
        }

        private void breed_Click(object sender, EventArgs e) {
            if (namamonsterbreeding.Text != string.Empty) {
                if (monster1.SelectedIndex > -1) {
                    if (monster2.SelectedIndex > -1) {
                        Monster baru;
                        Monster parent1, parent2;
                        parent1 = (Monster)monster1.SelectedItem;
                        parent2 = (Monster)monster2.SelectedItem;
                        baru = new Monster(namamonsterbreeding.Text, parent1, parent2);
                        monsters.Add(baru);
                        Perbarui();
                    } else {
                        MessageBox.Show("Pilih parent kedua");
                    }
                } else {
                    MessageBox.Show("Pilih parent pertama");
                }
            } else {
                MessageBox.Show("Isi nama monster baru");
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e) {
            int index = detailmonster.SelectedIndex;
            if (index > -1) {
                Monster temp = (Monster)detailmonster.Items[index];
                type.Text = (temp.Type2 == string.Empty) ? (temp.Type1) : (temp.Type1 + " - " + temp.Type2);
                parent.Text = (temp.Parent1 != null) ? (temp.Parent1.Nama + " - " + temp.Parent2.Nama) : "-";
            } else {
                type.Text = "-";
                parent.Text = "";
            }
        }

        private void monster1_SelectedIndexChanged(object sender, EventArgs e) {
            int index = monster1.SelectedIndex;
            if (index > -1) {
                monster2.DataSource = null;
                List<Monster> t = monsters.ToList();
                t.Remove((Monster)monster1.Items[index]);
                monster2.DataSource = t;
            }
        }
    }
}
