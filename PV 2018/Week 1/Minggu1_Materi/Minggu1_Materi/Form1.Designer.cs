﻿namespace Minggu1_Materi {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.namamonsterbeli = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.typemonsterbeli = new System.Windows.Forms.ComboBox();
            this.namamonsterjual = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.money = new System.Windows.Forms.Label();
            this.jumlahmonster = new System.Windows.Forms.Label();
            this.detailmonster = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.Label();
            this.parent = new System.Windows.Forms.Label();
            this.monster1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.monster2 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.namamonsterbreeding = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.breed = new System.Windows.Forms.Button();
            this.buy = new System.Windows.Forms.Button();
            this.sell = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buy);
            this.groupBox1.Controls.Add(this.typemonsterbeli);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.namamonsterbeli);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 127);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Buy";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sell);
            this.groupBox2.Controls.Add(this.namamonsterjual);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(218, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 127);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sell";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.parent);
            this.groupBox3.Controls.Add(this.type);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.detailmonster);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(463, 203);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 235);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Details";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.breed);
            this.groupBox4.Controls.Add(this.namamonsterbreeding);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.monster2);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.monster1);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Location = new System.Drawing.Point(12, 203);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(381, 235);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Breeding";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // namamonsterbeli
            // 
            this.namamonsterbeli.Location = new System.Drawing.Point(68, 25);
            this.namamonsterbeli.Name = "namamonsterbeli";
            this.namamonsterbeli.Size = new System.Drawing.Size(121, 20);
            this.namamonsterbeli.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Type";
            // 
            // typemonsterbeli
            // 
            this.typemonsterbeli.FormattingEnabled = true;
            this.typemonsterbeli.Location = new System.Drawing.Point(68, 57);
            this.typemonsterbeli.Name = "typemonsterbeli";
            this.typemonsterbeli.Size = new System.Drawing.Size(121, 21);
            this.typemonsterbeli.TabIndex = 3;
            // 
            // namamonsterjual
            // 
            this.namamonsterjual.FormattingEnabled = true;
            this.namamonsterjual.Location = new System.Drawing.Point(60, 25);
            this.namamonsterjual.Name = "namamonsterjual";
            this.namamonsterjual.Size = new System.Drawing.Size(121, 21);
            this.namamonsterjual.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(445, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Money : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(445, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "No. of Monster : ";
            // 
            // money
            // 
            this.money.AutoSize = true;
            this.money.Location = new System.Drawing.Point(582, 40);
            this.money.Name = "money";
            this.money.Size = new System.Drawing.Size(31, 13);
            this.money.TabIndex = 6;
            this.money.Text = "1000";
            this.money.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // jumlahmonster
            // 
            this.jumlahmonster.AutoSize = true;
            this.jumlahmonster.Location = new System.Drawing.Point(582, 69);
            this.jumlahmonster.Name = "jumlahmonster";
            this.jumlahmonster.Size = new System.Drawing.Size(13, 13);
            this.jumlahmonster.TabIndex = 7;
            this.jumlahmonster.Text = "0";
            this.jumlahmonster.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // detailmonster
            // 
            this.detailmonster.FormattingEnabled = true;
            this.detailmonster.Location = new System.Drawing.Point(62, 29);
            this.detailmonster.Name = "detailmonster";
            this.detailmonster.Size = new System.Drawing.Size(121, 21);
            this.detailmonster.TabIndex = 6;
            this.detailmonster.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Parent";
            // 
            // type
            // 
            this.type.AutoSize = true;
            this.type.Location = new System.Drawing.Point(62, 74);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(13, 13);
            this.type.TabIndex = 8;
            this.type.Text = "0";
            this.type.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // parent
            // 
            this.parent.AutoSize = true;
            this.parent.Location = new System.Drawing.Point(62, 113);
            this.parent.Name = "parent";
            this.parent.Size = new System.Drawing.Size(13, 13);
            this.parent.TabIndex = 9;
            this.parent.Text = "0";
            this.parent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // monster1
            // 
            this.monster1.FormattingEnabled = true;
            this.monster1.Location = new System.Drawing.Point(23, 56);
            this.monster1.Name = "monster1";
            this.monster1.Size = new System.Drawing.Size(121, 21);
            this.monster1.TabIndex = 11;
            this.monster1.SelectedIndexChanged += new System.EventHandler(this.monster1_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(63, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Monster 1";
            // 
            // monster2
            // 
            this.monster2.FormattingEnabled = true;
            this.monster2.Location = new System.Drawing.Point(241, 56);
            this.monster2.Name = "monster2";
            this.monster2.Size = new System.Drawing.Size(121, 21);
            this.monster2.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(273, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Monster 2";
            // 
            // namamonsterbreeding
            // 
            this.namamonsterbreeding.Location = new System.Drawing.Point(153, 106);
            this.namamonsterbreeding.Name = "namamonsterbreeding";
            this.namamonsterbreeding.Size = new System.Drawing.Size(121, 20);
            this.namamonsterbreeding.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(101, 109);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Name";
            // 
            // breed
            // 
            this.breed.Location = new System.Drawing.Point(164, 162);
            this.breed.Name = "breed";
            this.breed.Size = new System.Drawing.Size(75, 23);
            this.breed.TabIndex = 14;
            this.breed.Text = "Breed";
            this.breed.UseVisualStyleBackColor = true;
            this.breed.Click += new System.EventHandler(this.breed_Click);
            // 
            // buy
            // 
            this.buy.Location = new System.Drawing.Point(69, 84);
            this.buy.Name = "buy";
            this.buy.Size = new System.Drawing.Size(75, 23);
            this.buy.TabIndex = 15;
            this.buy.Text = "Buy";
            this.buy.UseVisualStyleBackColor = true;
            this.buy.Click += new System.EventHandler(this.buy_Click);
            // 
            // sell
            // 
            this.sell.Location = new System.Drawing.Point(70, 84);
            this.sell.Name = "sell";
            this.sell.Size = new System.Drawing.Size(75, 23);
            this.sell.TabIndex = 16;
            this.sell.Text = "Sell";
            this.sell.UseVisualStyleBackColor = true;
            this.sell.Click += new System.EventHandler(this.sell_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 450);
            this.Controls.Add(this.jumlahmonster);
            this.Controls.Add(this.money);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox typemonsterbeli;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox namamonsterbeli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox namamonsterjual;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label parent;
        private System.Windows.Forms.Label type;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox detailmonster;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox monster2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox monster1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label money;
        private System.Windows.Forms.Label jumlahmonster;
        private System.Windows.Forms.Button breed;
        private System.Windows.Forms.TextBox namamonsterbreeding;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buy;
        private System.Windows.Forms.Button sell;
    }
}

