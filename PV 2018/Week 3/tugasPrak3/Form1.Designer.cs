﻿namespace tugasPrak3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.refreshScreen = new System.Windows.Forms.Timer(this.components);
            this.panelPlayer = new System.Windows.Forms.Panel();
            this.musuh3 = new System.Windows.Forms.Panel();
            this.musuh2 = new System.Windows.Forms.Panel();
            this.musuh1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.spawnMusuh = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // refreshScreen
            // 
            this.refreshScreen.Enabled = true;
            this.refreshScreen.Tick += new System.EventHandler(this.refreshScreen_Tick);
            // 
            // panelPlayer
            // 
            this.panelPlayer.AutoSize = true;
            this.panelPlayer.BackColor = System.Drawing.Color.Transparent;
            this.panelPlayer.BackgroundImage = global::tugasPrak3.Properties.Resources.Shoot__5_;
            this.panelPlayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panelPlayer.Location = new System.Drawing.Point(1082, 305);
            this.panelPlayer.Name = "panelPlayer";
            this.panelPlayer.Size = new System.Drawing.Size(152, 100);
            this.panelPlayer.TabIndex = 0;
            // 
            // musuh3
            // 
            this.musuh3.BackColor = System.Drawing.Color.Transparent;
            this.musuh3.BackgroundImage = global::tugasPrak3.Properties.Resources.musuh1;
            this.musuh3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.musuh3.Location = new System.Drawing.Point(152, 277);
            this.musuh3.Name = "musuh3";
            this.musuh3.Size = new System.Drawing.Size(150, 100);
            this.musuh3.TabIndex = 1;
            this.musuh3.Visible = false;
            // 
            // musuh2
            // 
            this.musuh2.BackColor = System.Drawing.Color.Transparent;
            this.musuh2.BackgroundImage = global::tugasPrak3.Properties.Resources.musuh2;
            this.musuh2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.musuh2.Location = new System.Drawing.Point(94, 406);
            this.musuh2.Name = "musuh2";
            this.musuh2.Size = new System.Drawing.Size(150, 100);
            this.musuh2.TabIndex = 2;
            this.musuh2.Visible = false;
            // 
            // musuh1
            // 
            this.musuh1.BackColor = System.Drawing.Color.Transparent;
            this.musuh1.BackgroundImage = global::tugasPrak3.Properties.Resources.musuh3;
            this.musuh1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.musuh1.Location = new System.Drawing.Point(107, 139);
            this.musuh1.Name = "musuh1";
            this.musuh1.Size = new System.Drawing.Size(150, 100);
            this.musuh1.TabIndex = 3;
            this.musuh1.Visible = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::tugasPrak3.Properties.Resources.ProjectilePea;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel4.Location = new System.Drawing.Point(740, 224);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(30, 30);
            this.panel4.TabIndex = 4;
            this.panel4.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.BackgroundImage = global::tugasPrak3.Properties.Resources.Bullet__1_;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel5.Location = new System.Drawing.Point(1335, 194);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(78, 37);
            this.panel5.TabIndex = 5;
            this.panel5.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 39);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // spawnMusuh
            // 
            this.spawnMusuh.Enabled = true;
            this.spawnMusuh.Interval = 3000;
            this.spawnMusuh.Tick += new System.EventHandler(this.spawnMusuh_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 39);
            this.label2.TabIndex = 6;
            this.label2.Text = "Life : ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(1221, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(276, 144);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 39);
            this.label3.TabIndex = 7;
            this.label3.Text = "Misil : ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::tugasPrak3.Properties.Resources.BG;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1556, 614);
            //this.Controls.Add(this.panel5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panelPlayer);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer refreshScreen;
        private System.Windows.Forms.Panel panelPlayer;
        private System.Windows.Forms.Panel musuh3;
        private System.Windows.Forms.Panel musuh2;
        private System.Windows.Forms.Panel musuh1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer spawnMusuh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
    }
}

