﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using perpustakaanKelas;

namespace tugasPrak3
{
    public partial class Form1 : Form
    {
        List<Musuh> arrMusuh;
        List<Sprite> peluru;
        List<Sprite> peluruMusuh;
        Random r;
        int ctr = 0; int skor = 0;
        int duaDetik = 0;
        int speedPeluru = 20; int life = 5;
        int missile = 5;
        int sepuluhDetik = 10000;
        bool proteksi = false;
        Sprite player;
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            r = new Random();
            arrMusuh = new List<Musuh>();
            peluru = new List<Sprite>();
            label1.Text = skor+"";
            label2.Text = "Life: " + life;
            label3.Text = "Misil: " + missile;
            player = new Sprite(panelPlayer);
            player.panel = panelPlayer;
            peluruMusuh = new List<Sprite>();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e) {
            int x, y;
            x = panelPlayer.Left;
            y = panelPlayer.Top;
            if (e.KeyCode == Keys.W) y -= 5;
            else if (e.KeyCode == Keys.A) x -= 5;
            else if (e.KeyCode == Keys.S) y += 5;
            else if (e.KeyCode == Keys.D) x += 5;
            if (x > 0 && (x + panelPlayer.Width + 10) < this.Width) panelPlayer.Left = x;
            if (y > 0 && (y + panelPlayer.Height + 40) < this.Height) panelPlayer.Top = y;
        }

        private void refreshScreen_Tick(object sender, EventArgs e) {
            if (ctr % 2 == 0) { ctr = 0; }
            if (proteksi) {
                sepuluhDetik -= 100;
                panelPlayer.BorderStyle = BorderStyle.Fixed3D;
                panelPlayer.BackColor = System.Drawing.Color.Red;
            }
            else {
                panelPlayer.BorderStyle = BorderStyle.None;
                panelPlayer.BackColor = System.Drawing.Color.Transparent;
            }
            for (int i = arrMusuh.Count - 1; i >= 0; i--)
            {
                if (arrMusuh[i].Index < 2)
                {
                    if (ctr == 0)
                        arrMusuh[i].sprite.panel.Left += 6;
                }
                else arrMusuh[i].sprite.panel.Left += 6;
                Panel musuhsaiki = arrMusuh[i].sprite.panel;
                if (arrMusuh[i].sprite.nubruk(player))
                {
                    if (!proteksi) life--;
                    this.Controls.Remove(arrMusuh[i].sprite.panel);
                    arrMusuh.RemoveAt(i);
                }
                if (musuhsaiki.Left + musuhsaiki.Width > this.Width)
                {
                    if (!proteksi) life--;
                    this.Controls.Remove(arrMusuh[i].sprite.panel);
                    arrMusuh.RemoveAt(i);
                }
                if (duaDetik >= 2000)
                {
                    if (r.Next(2) == 0)
                    {
                        if (arrMusuh[i].Index == 0)
                        {
                            Peluru a = new Peluru(panel4, musuhsaiki);
                            peluruMusuh.Add(a);
                            this.Controls.Add(a.panel);
                            a.panel.BringToFront();
                            a.setPos();
                        }
                        else if (arrMusuh[i].Index == 1)
                        {
                            Misil a = new Misil(panel5, musuhsaiki);
                            peluruMusuh.Add(a);
                            this.Controls.Add(a.panel);
                            a.panel.BringToFront();
                            a.setPos();
                        }
                    }
                }
            }
            for (int i = peluruMusuh.Count-1; i >=0; i--)
            {
                Sprite item = peluruMusuh[i];
                Panel temp = item.panel;
                item.panel.Left += speedPeluru;
                if (item.nubruk(player))
                {
                    if (item is Misil)
                    {
                        life -= 2;
                        this.Controls.Remove(temp);
                        peluruMusuh.RemoveAt(i);
                    }
                    else
                    {
                        life -= 1;
                        this.Controls.Remove(temp);
                        peluruMusuh.RemoveAt(i);
                    }
                }

            }
            for (int i = peluru.Count - 1; i >= 0; i--) {
                Sprite item = peluru[i];
                Panel temp = item.panel;
                if (item is Peluru) {
                    Peluru j = (Peluru)item;
                    if (j.arahGerak == -1) {
                        j.panel.Left -= speedPeluru;
                        if (j.panel.Left < 0) {
                            this.Controls.Remove(temp);
                            peluru.Remove(item);
                        }
                        for (int jumMusuh = arrMusuh.Count - 1; jumMusuh >= 0; jumMusuh--) {
                            if (j.nubruk(arrMusuh[jumMusuh].sprite)) {
                                if (arrMusuh[jumMusuh].Index < 2) {
                                    arrMusuh[jumMusuh].Nyawa--;
                                    this.Controls.Remove(temp);
                                    peluru.Remove(item);
                                }
                                if (arrMusuh[jumMusuh].Nyawa <= 0) {
                                    this.Controls.Remove(arrMusuh[jumMusuh].sprite.panel);
                                    int hadiah = r.Next(100);
                                    if (hadiah < 20) life++;
                                    else if (hadiah < 60)  missile++;
                                    else if (hadiah < 80) {
                                        proteksi = true;
                                        sepuluhDetik = 10000;
                                    } else  arrMusuh[jumMusuh].Reward *= 2;  
                                    skor += arrMusuh[jumMusuh].Reward;
                                    arrMusuh.RemoveAt(jumMusuh);
                                }
                            }
                        }
                    }
                } else if (item is Misil) {
                    Misil k = (Misil)item;
                    if (k.arahGerak == -1) {
                        k.panel.Left -= speedPeluru;
                        if (k.panel.Left < 0) {
                            this.Controls.Remove(temp);
                            peluru.Remove(item);
                        }
                        for (int jumMusuh = arrMusuh.Count - 1; jumMusuh >= 0; jumMusuh--) {
                            if (k.nubruk(arrMusuh[jumMusuh].sprite)) {
                                arrMusuh[jumMusuh].Nyawa--;
                                this.Controls.Remove(temp);
                                peluru.Remove(item);
                                if (arrMusuh[jumMusuh].Nyawa <= 0) {
                                    this.Controls.Remove(arrMusuh[jumMusuh].sprite.panel);
                                    int hadiah = r.Next(100);
                                    if (hadiah < 20) life++;
                                    else if (hadiah < 60) missile++;
                                    else if (hadiah < 80) {
                                        proteksi = true;
                                        sepuluhDetik = 10000;
                                    } else arrMusuh[jumMusuh].Reward *= 2;
                                    skor += arrMusuh[jumMusuh].Reward;
                                    arrMusuh.RemoveAt(jumMusuh);
                                }
                            }
                        }
                    }
                }
            }
            label1.Text = skor + "";
            label2.Text = "Life: " + life;
            label3.Text = "Misil: " + missile;
            ctr++;
            if (duaDetik >= 2000)
            {
                duaDetik -= 2000;
            }
            duaDetik += 100;
            if (sepuluhDetik <= 0) {
                proteksi = false; sepuluhDetik = 10000;
            }
            this.Invalidate();
            if (life <= 0)
            {
                refreshScreen.Enabled = false;
                spawnMusuh.Enabled = false;
                MessageBox.Show("Game Over! Skor: "+skor);
            }
        }

        private void spawnMusuh_Tick(object sender, EventArgs e) {
            if (arrMusuh.Count < 8) {
                int banyakMusuh = r.Next(2, 5);
                for (int i = 0; i < banyakMusuh; i++) {
                    int temp = r.Next(3);
                    Musuh tampung = new Musuh(temp);
                    if (temp == 0) { tampung.sprite.panel.BackgroundImage = musuh1.BackgroundImage; }
                    else if (temp == 1) { tampung.sprite.panel.BackgroundImage = musuh2.BackgroundImage; }
                    else { tampung.sprite.panel.BackgroundImage = musuh3.BackgroundImage; }
                    this.Controls.Add(tampung.sprite.panel);
                    tampung.sprite.panel.Top = r.Next(this.Height - tampung.sprite.panel.Height-50);
                    arrMusuh.Add(tampung);
                }
                this.Invalidate();
            //    spawnMusuh.Enabled = false;
            }
        }
        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space) {
                Peluru a = new Peluru(panel4, panelPlayer);
                peluru.Add(a);
                this.Controls.Add(a.panel);
                a.setPos();
            } else if (e.KeyCode == Keys.M) {
                Misil a = new Misil(panel5, panelPlayer);
                peluru.Add(a);
                this.Controls.Add(a.panel);
                a.setPos();
                missile--;
            }
        }
    }
}
