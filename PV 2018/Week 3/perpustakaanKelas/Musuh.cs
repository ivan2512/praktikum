﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perpustakaanKelas
{
    public class Musuh
    {
        public Sprite sprite { get; set; }
        public int Index { get; private set; }
        public int Reward { get; set; }
        public int Nyawa { get; set; }
        public Musuh(int randoman)
        {
            sprite = new Sprite(new System.Windows.Forms.Panel());
            this.Index = randoman;
            if (randoman == 0)
            {
                sprite.panel.BackColor = System.Drawing.Color.Transparent;
                sprite.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                sprite.panel.Size = new System.Drawing.Size(150, 100);
                Reward = 50;
                Nyawa = 1;
            } else if (randoman == 1)
            {
                sprite.panel.BackColor = System.Drawing.Color.Transparent;
                sprite.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                sprite.panel.Size = new System.Drawing.Size(150, 100);
                Reward = 100;
                Nyawa = 2;
            } else
            {
                sprite.panel.BackColor = System.Drawing.Color.Transparent;
                sprite.panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                sprite.panel.Size = new System.Drawing.Size(150, 100);
                Reward = 150;
                Nyawa = 1;
            }
        }
    }
}
