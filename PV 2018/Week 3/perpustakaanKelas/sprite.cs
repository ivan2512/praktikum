﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace perpustakaanKelas
{
    public class Sprite
    {
        public Panel panel { get; set; }
        public Sprite(Panel pass)
        {
            panel = new Panel();
            panel.BackColor = System.Drawing.Color.Transparent;
            panel.Size = pass.Size;
            panel.BackgroundImage = pass.BackgroundImage;
            panel.BackgroundImageLayout = ImageLayout.Zoom;
            panel.Location = pass.Location;
        }

        public bool nubruk(Sprite lain)
        {
         //   MessageBox.Show("CEK");
            return !(((panel.Top + panel.Height) < lain.panel.Top) || (panel.Top > (lain.panel.Top + lain.panel.Height))
                || ((panel.Left + panel.Width) < lain.panel.Left) || (panel.Left > (lain.panel.Left + lain.panel.Height)));
        }
    }

    public class Peluru:Sprite {
        public int posX { get; set; }
        public int arahGerak { get; set; }
        public int posY { get; set; }
        public Peluru(Panel peluru, Panel yangManggil) : base(peluru) {
            if (yangManggil.Name == "panelPlayer") {
                posX = yangManggil.Left - panel.Width;
                posY = yangManggil.Top + yangManggil.Height - panel.Height;
                arahGerak = -1;
            } else
            {
                posX = yangManggil.Left +yangManggil.Width;
                posY = yangManggil.Top + yangManggil.Height - panel.Height;
                arahGerak = 1;
            }
        }

        public void setPos() {
            panel.Top = posY;
            panel.Left = posX;
        }
    }

    public class Misil:Sprite
    {
        public int posX { get; set; }
        public int arahGerak { get; set; }
        public int posY { get; set; }
        public Misil(Panel peluru, Panel yangManggil) : base(peluru) {
            if (yangManggil.Name == "panelPlayer") {
                posX = yangManggil.Left - panel.Width;
                posY = yangManggil.Top + yangManggil.Height - panel.Height;
                arahGerak = -1;
            } else
            {
                posX = yangManggil.Left + yangManggil.Width;
                posY = yangManggil.Top + yangManggil.Height - panel.Height;
                arahGerak = 1;
            }
        }

        public void setPos() {
            panel.Top = posY;
            panel.Left = posX;
        }
    }
}
