﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorWeek5 {
    public partial class Form2 : Form {
        string nama;
        Form1 parent;
        public Form2() {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e) {
            parent = (Form1)MdiParent;
            nama = Text;
        }

        private void button1_Click(object sender, EventArgs e) {
            int width = listBox1.Width;
            string pesan = textBox1.Text;
            if (pesan.ToLower() == "ping") {
                parent.pingPesan(this);
                textBox1.Text = "";
            } else {
                listBox1.Items.Add(this.nama + " : " + pesan);
                parent.kirimPesan(this, pesan);
                textBox1.Text = "";
            }
        }
    }
}
