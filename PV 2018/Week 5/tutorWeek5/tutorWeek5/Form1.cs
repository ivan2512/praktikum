﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorWeek5 {
    public partial class Form1 : Form {
        public List<Form2> arr;
        Form2 yangNgePing;
        public Form1() {
            InitializeComponent();
            LayoutMdi(MdiLayout.TileHorizontal);
            arr = new List<Form2>();
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void vertikalToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e) {
            Form2 temp = new Form2();
            temp.MdiParent = this;
            temp.Text = (arr.Count+1)+"";
            temp.Show();
            arr.Add(temp);
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
            if (MdiChildren.Count() > 0) {
                MdiChildren[0].Close();
            }
        }

        public void pingPesan(Form2 asal) {
            yangNgePing = asal;
            timer1.Start();
        }

        public void kirimPesan(Form2 asal, string pesan) {
            foreach (Form2 item in arr) {
                if (item != asal) {
                    item.listBox1.Items.Add(asal.Text + " : " + pesan);
                }
            }
        }
        int ctr = 0;
        private void timer1_Tick(object sender, EventArgs e) {
            Random r = new Random();
            
            ctr += 1;
            if (ctr == 20) {
                timer1.Stop();
            } else {
                foreach (Form2 item in arr) {
                    if (item != yangNgePing) {
                        item.Left += r.Next(20);
                        item.Top += r.Next(20);
                        item.Left -= r.Next(20);
                        item.Top -= r.Next(20);
                    }
                }
            }
        }
    }
}
