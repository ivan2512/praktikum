﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Player
    {
        public String Nama { get; set; }
        public int skor { get; set; }
        public int life { get; set; }
        public Player(String nama) {
            this.Nama = nama;
            skor = 0;
            life = 5;
        }
    }
}
