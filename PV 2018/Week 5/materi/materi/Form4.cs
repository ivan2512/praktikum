﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace materi {
    public partial class Form4 : Form {
        public Player ku;
        public List<string> ini;
        Form1 parent;
        string soal;
        public Form4() {
            InitializeComponent();
        }
        public void lagi() {
            label1.Text = ku.Nama;
            label2.Text = "Skor: " + ku.skor;
            label3.Text = "Life: " + ku.life;
            listBox1.DataSource = null;
            listBox1.DataSource = ini;
        }

        private void button1_Click(object sender, EventArgs e) {
            parent = (Form1)MdiParent;
            soal = label5.Text;
            if (textBox1.Text.Length == 1) {
                string s = "";
                if (soal.Contains(textBox1.Text)) {
                    ku.skor += 10;
                    for (int i = 0; i < soal.Length;  i++) {
                        if (soal[i] == textBox1.Text[0]) {
                            s += soal[i] + " ";
                        } else {
                            s += "_ ";
                        }
                    }
                } else {
                    ku.life--;
                    if (ku.life == 0) {
                        this.Close();
                        MessageBox.Show(ku.Nama+" game over");
                    }
                }
                string sebelum = parent.history[parent.history.Count-1];
                char[] temp = s.ToArray();
                for (int i = 0; i < temp.Count(); i++) {
                    if (sebelum[i] != '_') {
                        temp[i] = sebelum[i];
                    } else {
                        temp[i] = s[i];
                    }
                }
                s = "";
                for (int i = 0; i < temp.Count(); i++) {
                    s += temp[i];
                }
                parent.history.Add(s);
            }

            foreach (Form4 item in parent.MdiChildren) {
                item.lagi();
            }
            parent.giliran += 1;
            parent.giliran %= 4;
            for (int i = 0; i < parent.MdiChildren.Count(); i++) {
                if (i != parent.giliran) {
                    ((Form4)(parent.MdiChildren[i])).button1.Enabled = false;
                } else {
                    ((Form4)(parent.MdiChildren[i])).button1.Enabled = true;

                }
            }
        }
    }
}
