﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace materi {
    public partial class Form1 : Form {
        public Form2 addPlayer;
        public Form3 formSoal;
        public List<Form4> players;
        Player[] array;
        List<String> soal;
        String soalSekarang;
        public List<string> history;
        Random r;
        public int giliran = 0;
        public Form1() {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e) {
            addPlayer = new Form2();
            addPlayer.MdiParent = this;
            r = new Random();
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.Cascade);

        }

        private void tileHorizontalToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileHorizontal);

        }

        private void tileVerticalToolStripMenuItem_Click(object sender, EventArgs e) {
            this.LayoutMdi(MdiLayout.TileVertical);

        }

        private void addPlayerToolStripMenuItem_Click(object sender, EventArgs e) {
            addPlayer.Show();
        }

        public void terima(Player[] arr) {
            this.array = arr;
            addPlayer.Close();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e) {
            formSoal = new Form3();
            formSoal.MdiParent = this;
            formSoal.Show();
            history = new List<string>();
        }
        public void terimaSoal(List<String> soal) {
            this.soal = soal;
            formSoal.Close();
            players = new List<Form4>();
            int j = r.Next(soal.Count);
            soalSekarang = soal[j];
            soal.RemoveAt(j);
            string s="";
            for (int i = 0; i < soalSekarang.Length; i++) {
                s += "_ ";
            }
            history.Add(s);
            for (int i = 0; i < array.Count(); i++) {
                Form4 temp = new Form4();
                temp.Text = "(Player "+(i+1)+")"+ array[i].Nama;
                temp.ku = array[i];
                temp.ini = history;
                players.Add(temp);
                temp.lagi();
                temp.MdiParent = this;
                temp.Show();
                temp.label5.Text = soalSekarang;
            }
            for (int i = 0; i < players.Count; i++) {
                if (i != giliran) {
                    players[i].button1.Enabled = false;
                } else {
                    players[i].button1.Enabled = true;
                }
            }
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e) {
            Player pemenang=((Form4)MdiChildren[0]).ku;
            foreach (var item in MdiChildren) {
                if (((Form4)item).ku.life > 0) {
                    if (pemenang.skor< ((Form4)item).ku.skor) {
                        pemenang = ((Form4)item).ku;
                    }
                }
                ((Form4)item).Close();
            }
            MessageBox.Show("Pemenangnya: "+pemenang.Nama+" dengan skor "+pemenang.skor);

        }
    }
}
