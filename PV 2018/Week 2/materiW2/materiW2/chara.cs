﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace materiW2 {
    public class chara {
        public string Name { get; set; }
        public bool Evolved { get; set; }
        public string Job { get; set; }
        public int Exp { get; set; }
        private int _hp;
        public int CurrHP {
            get { return _hp; } 
            set{
                if (value >= 0 && value <= MaxHP)
                    _hp = value;
                else if (value <0) _hp = value;
                else _hp = MaxHP;
            }
        }
        public int MaxHP { get; set; }
        public int Money { get; set; }
        public int Level { get; set; }
        public chara(string nama, string job) {
            Evolved = false;
            Job = job;
            Name = nama;
            Exp = 0; Level = 1;
            MaxHP = 80; CurrHP = 80;
            Money = 200;
        }
        public override string ToString() {
            return Name;
        }
    }
}
