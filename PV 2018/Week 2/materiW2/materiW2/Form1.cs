﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace materiW2 {
    public partial class Form1 : Form {
        Player now;
        chara aktif;
        public Form1() {
            InitializeComponent();
            groupBox4.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e) {
            if (2018 - int.Parse(dateTimePicker1.Value.ToString("yyyy")) > 18) {
                panel1.Visible = false;
                now = new Player();
            } else {
                MessageBox.Show("Umur anda kurang dari 18 tahun");
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            now.arr.Add(new chara(textBox1.Text, comboBox1.SelectedItem.ToString()));
            comboBox2.DataSource = null;
            comboBox2.Items.Clear();
            comboBox2.DataSource = now.arr;

        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e) {
            now = null;
            panel1.Visible = true;
        }

        private void label5_Click(object sender, EventArgs e) {

        }

        private void button3_Click(object sender, EventArgs e) {
            if (comboBox2.SelectedIndex > -1) {
                label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
                string s;
                if (now.arr[comboBox2.SelectedIndex].Evolved) {
                    s = now.arr[comboBox2.SelectedIndex].Job;
                }
                else {
                    s = "Novice (" + now.arr[comboBox2.SelectedIndex].Job + ")";
                }
                label6.Text = "Job  : " + s;
                Level.Text = "Level: " + now.arr[comboBox2.SelectedIndex].Level;
                label8.Text = "Money: " + now.arr[comboBox2.SelectedIndex].Money;
                label9.Text = "Exp: " + now.arr[comboBox2.SelectedIndex].Exp + "/100";
                progressBar1.Value = now.arr[comboBox2.SelectedIndex].Exp;
                label10.Text = "HP: " + now.arr[comboBox2.SelectedIndex].CurrHP + "/" + now.arr[comboBox2.SelectedIndex].MaxHP;
                progressBar2.Maximum = now.arr[comboBox2.SelectedIndex].MaxHP;
                progressBar2.Value = now.arr[comboBox2.SelectedIndex].CurrHP;
                aktif = now.arr[comboBox2.SelectedIndex];
            }
        }

        private void button4_Click(object sender, EventArgs e) {
            if (comboBox2.SelectedIndex > -1) {
                now.arr.RemoveAt(comboBox2.SelectedIndex);
                comboBox2.DataSource = null;
                comboBox2.DataSource = now.arr;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e) {
            string s;
            if (comboBox2.SelectedIndex > -1) {
                if (now.arr[comboBox2.SelectedIndex].Evolved) {
                    s = now.arr[comboBox2.SelectedIndex].Job;
                } else {
                    s = "Novice (" + now.arr[comboBox2.SelectedIndex].Job + ")";
                }
            }
            else s = "";

            label4.Text = "Job: " + s;
        }

        private void button6_Click(object sender, EventArgs e) {
            if (aktif.Money >= 100) {
                if (aktif.CurrHP < aktif.MaxHP) {
                    aktif.CurrHP = aktif.MaxHP;
                    aktif.Money -= 100;
                    label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
                    string s;
                    if (aktif.Evolved) {
                        s = aktif.Job;
                    }
                    else {
                        s = "Novice (" + aktif.Job + ")";
                    }
                    label6.Text = "Job  : " + s;
                    Level.Text = "Level: " + aktif.Level;
                    label8.Text = "Money: " + aktif.Money;
                    label9.Text = "Exp: " + aktif.Exp + "/100";
                    progressBar1.Value = aktif.Exp;
                    label10.Text = "HP: " + aktif.CurrHP + "/" + aktif.MaxHP;
                    progressBar2.Maximum = aktif.MaxHP;
                    progressBar2.Value = aktif.CurrHP;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e) {
            if (aktif.Money >= 300) {
                if (aktif.Level >= 5) {
                    aktif.Money -= 300;
                    aktif.Evolved = true;
                    if (aktif.Job == "Knight") {
                        aktif.MaxHP += 100;
                    } else if (aktif.Job == "Magician") {
                        aktif.MaxHP += 50;
                    } else if (aktif.Job == "Archer") {
                        aktif.MaxHP += 75;
                    }
                    label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
                    string s;
                    if (aktif.Evolved) {
                        s = aktif.Job;
                    }
                    else {
                        s = "Novice (" + aktif.Job + ")";
                    }
                    label6.Text = "Job  : " + s;
                    Level.Text = "Level: " + aktif.Level;
                    label8.Text = "Money: " + aktif.Money;
                    label9.Text = "Exp: " + aktif.Exp + "/100";
                    progressBar1.Value = aktif.Exp;
                    label10.Text = "HP: " + aktif.CurrHP + "/" + aktif.MaxHP;
                    progressBar2.Maximum = aktif.MaxHP;
                    progressBar2.Value = aktif.CurrHP;
                }
            }
        }

        private void button8_Click(object sender, EventArgs e) {
            aktif.CurrHP -= 10;
            label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
            string s;
            if (aktif.Evolved) {
                s = aktif.Job;
            }
            else {
                s = "Novice (" + aktif.Job + ")";
            }
            label6.Text = "Job  : " + s;
            Level.Text = "Level: " + aktif.Level;
            label8.Text = "Money: " + aktif.Money;
            label9.Text = "Exp: " + aktif.Exp + "/100";
            progressBar1.Value = aktif.Exp;
            label10.Text = "HP: " + aktif.CurrHP + "/" + aktif.MaxHP;
            progressBar2.Maximum = aktif.MaxHP;
            progressBar2.Value = aktif.CurrHP;
        }

        private void button9_Click(object sender, EventArgs e) {
            aktif.Exp += 20;
            if (aktif.Exp >= 100) {
                aktif.Exp -= 100;
                aktif.Level++;
            }
            label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
            string s;
            if (aktif.Evolved) {
                s = aktif.Job;
            }
            else {
                s = "Novice (" + aktif.Job + ")";
            }
            label6.Text = "Job  : " + s;
            Level.Text = "Level: " + aktif.Level;
            label8.Text = "Money: " + aktif.Money;
            label9.Text = "Exp: " + aktif.Exp + "/100";
            progressBar1.Value = aktif.Exp;
            label10.Text = "HP: " + aktif.CurrHP + "/" + aktif.MaxHP;
            progressBar2.Maximum = aktif.MaxHP;
            progressBar2.Value = aktif.CurrHP;
        }

        private void button10_Click(object sender, EventArgs e) {
            aktif.Money += 100;
            label7.Text = "Nama : " + now.arr[comboBox2.SelectedIndex].Name;
            string s;
            if (aktif.Evolved) {
                s = aktif.Job;
            }
            else {
                s = "Novice (" + aktif.Job + ")";
            }
            label6.Text = "Job  : " + s;
            Level.Text = "Level: " + aktif.Level;
            label8.Text = "Money: " + aktif.Money;
            label9.Text = "Exp: " + aktif.Exp + "/100";
            progressBar1.Value = aktif.Exp;
            label10.Text = "HP: " + aktif.CurrHP + "/" + aktif.MaxHP;
            progressBar2.Maximum = aktif.MaxHP;
            progressBar2.Value = aktif.CurrHP;
        }

        private void button5_Click(object sender, EventArgs e) {
            groupBox4.Enabled = true;
        }

        private void button11_Click(object sender, EventArgs e) {
            groupBox4.Enabled = false;
        }
    }
}
