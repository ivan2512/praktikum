﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerpustakaanKelas {
    public class Musuh {
        private string _nama;
        public string Nama {
            get { return _nama; }
            set { _nama = value; }
        }
        private int _hp;
        public int HP {
            get { return _hp; }
            set { _hp = value; }
        }
        private int _atk;
        public int Attack {
            get { return _atk; }
            set { _atk = value; }
        }
        private int _def;
        public int Defense {
            get { return _def; }
            set { _def = value; }
        }
        private int _mag;
        public int Magic {
            get { return _mag; }
            set { _mag = value; }
        }
        private int _mp;
        public int MP {
            get { return _mp; }
            set { _mp = value; }
        }
        protected int index;
        public int RewardGold { get; private set; }
        public int RewardXP { get; private set; }
        public int Level { get; protected set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        Random r = new Random(DateTime.Now.Millisecond);
        public Musuh() { }
        public Musuh(Pet musuh) {
            double mult; int hasil;
            hasil = r.Next(5);
            if (hasil == 0) {
                mult = 1.05; Nama = "KingCrab";
                RewardGold = 1065; RewardXP = 21;
            } else if (hasil == 1) {
                mult = 1.06; Nama = "Scorpion";
                RewardGold = 1320; RewardXP = 25;
            } else if (hasil == 2) {
                mult = 1.07; Nama = "Leo";
                RewardGold = 1520; RewardXP = 30;
            } else if (hasil == 3) {
                mult = 1.08; Nama = "Libra";
                RewardGold = 1750; RewardXP = 50;
            } else {
                mult = 1.10; Nama = "Virgo";
                RewardGold = 2000; RewardXP = 100;
            }
            
            HP = (int)Math.Round(musuh.HP * mult);
            Attack = (int)Math.Round(musuh.Attack * mult);
            Magic = (int)Math.Round(musuh.Magic * mult);
            MP = (int)Math.Round(musuh.MP * mult);
            Defense = (int)Math.Round(musuh.Defense * mult);
        }
    }
}
