﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerpustakaanKelas {
    public class Pet:Musuh {
        private int _exp;
        public int Price { get; private set; }
        public List<string> Actions { get; set; }
        public int Experience {
            get { return _exp; }
            set {
                while (value >= 100) {
                    levelUp(); value -= 100;
                }
                _exp = value;
            }
        }
        public Pet(int i) {
            Actions = new List<string>();
            Actions.Add("Attack");
            Actions.Add("Move");
            Actions.Add("Retreat");
            index = i; Level = 0;
            if (i == 0) {
                Nama = "Mildeep"; HP = 60;
                Attack = 66; Magic = 55;
                MP = 66; Defense = 59;
                Price = 1470;
            } else if (i == 1) {
                Nama = "Feilla"; HP = 52;
                Attack = 46; Magic = 72;
                MP = 72; Defense = 37;
                Price = 2250;
            } else if (i == 2) {
                Nama = "Cleo"; HP = 49;
                Attack = 30; Magic = 75;
                MP = 75; Defense = 48;
                Price = 3310;
            } else if (i == 3) {
                Nama = "Snaka"; HP = 56;
                Attack = 60; Magic = 71;
                MP = 56; Defense = 58;
                Price = 4980;
            } else if (i == 4) {
                Nama = "Urd"; HP = 61;
                Attack = 89; Magic = 42;
                MP = 89; Defense = 67;
                Price = 10000;
                Actions.Add("World's End");
            }
        }

        public string cetak() {
            return "Pet: " + Nama + "\r\nLevel: " + Level + "  ATT:" + Attack + 
                "\r\nHP: " + HP + "  MAG: " + Magic + 
                "\r\nMP: " + MP + "  DEF: " + Defense;
        }

        public void levelUp() {
            Level++;
            Random r = new Random();
            int a = r.Next(5, 11);
            a += 100;
            HP = HP * a / 100;
            Attack = Attack * a / 100;
            Defense = Defense * a / 100;
            MP = MP * a / 100;
            Magic = Magic * a / 100;
            if (Level % 10 == 0) {
                if (Level / 10 == 1) {
                    if (index == 0) {
                        Nama = "Unicorn";
                    	Actions.Add("Heal");
                    }
                    else if (index == 1){
                        Nama = "Fignar";
                        Actions.Add("Magma Burst");
                    }
                    else if (index == 2){
                        Nama = "Cloudar";
                        Actions.Add("Lightning Blast");
                    }
                    else if (index == 3){
                        Nama = "Drake";
                        Actions.Add("Scratch");
                    }
                    else if (index == 4){
                        Nama = "Valkyrie";
                        Actions.Add("Eternal Light");
                    }
                    a = 120;
                    HP = HP * a / 100;
                    Attack = Attack * a / 100;
                    Defense = Defense * a / 100;
                    MP = MP * a / 100;
                    Magic = Magic * a / 100;
                }
                else if (Level / 10 == 2) {
                    if (index == 0){
                        Nama = "Pegasus";
                        Actions.Add("Heaven's Light");
                    } else if (index == 1){
                        Nama = "Sandra";
                        Actions.Add("Roar");
                    } else if (index == 2){
                    	Actions.Add("Thunder Storm");
                        Nama = "Thunderfury";
                    } else if (index == 3){
                    	Actions.Add("Spin Scratch");
                        Nama = "Uniak";
                    }
                    if (index != 4) {
                        a = 120;
                        HP = HP * a / 100;
                        Attack = Attack * a / 100;
                        Defense = Defense * a / 100;
                        MP = MP * a / 100;
                        Magic = Magic * a / 100;
                    }
                }
            }
        }

        public override string ToString() {
            return Nama;
        }
    }
}
