﻿namespace PetWars
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelMPmu = new System.Windows.Forms.Label();
            this.labelHPmu = new System.Windows.Forms.Label();
            this.labelMPku = new System.Windows.Forms.Label();
            this.labelHPku = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.namamu = new System.Windows.Forms.Label();
            this.namaku = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.HPmu = new System.Windows.Forms.ProgressBar();
            this.MPmu = new System.Windows.Forms.ProgressBar();
            this.HPku = new System.Windows.Forms.ProgressBar();
            this.MPku = new System.Windows.Forms.ProgressBar();
            this.label4 = new System.Windows.Forms.Label();
            this.pilihAction = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buyPet = new System.Windows.Forms.Button();
            this.beliPet = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.citGold = new System.Windows.Forms.TextBox();
            this.citLevel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rangkaiStatsDisini = new System.Windows.Forms.Label();
            this.cetak = new System.Windows.Forms.Button();
            this.mine = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.labelGapenting = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.history = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelMPmu);
            this.groupBox1.Controls.Add(this.labelHPmu);
            this.groupBox1.Controls.Add(this.labelMPku);
            this.groupBox1.Controls.Add(this.labelHPku);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Controls.Add(this.namamu);
            this.groupBox1.Controls.Add(this.namaku);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.HPmu);
            this.groupBox1.Controls.Add(this.MPmu);
            this.groupBox1.Controls.Add(this.HPku);
            this.groupBox1.Controls.Add(this.MPku);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.pilihAction);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.richTextBox3);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Location = new System.Drawing.Point(15, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 519);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Battle";
            // 
            // labelMPmu
            // 
            this.labelMPmu.AutoSize = true;
            this.labelMPmu.Location = new System.Drawing.Point(371, 314);
            this.labelMPmu.Name = "labelMPmu";
            this.labelMPmu.Size = new System.Drawing.Size(41, 13);
            this.labelMPmu.TabIndex = 22;
            this.labelMPmu.Text = "label15";
            // 
            // labelHPmu
            // 
            this.labelHPmu.AutoSize = true;
            this.labelHPmu.Location = new System.Drawing.Point(371, 279);
            this.labelHPmu.Name = "labelHPmu";
            this.labelHPmu.Size = new System.Drawing.Size(41, 13);
            this.labelHPmu.TabIndex = 21;
            this.labelHPmu.Text = "label14";
            // 
            // labelMPku
            // 
            this.labelMPku.AutoSize = true;
            this.labelMPku.Location = new System.Drawing.Point(144, 307);
            this.labelMPku.Name = "labelMPku";
            this.labelMPku.Size = new System.Drawing.Size(41, 13);
            this.labelMPku.TabIndex = 20;
            this.labelMPku.Text = "label14";
            // 
            // labelHPku
            // 
            this.labelHPku.AutoSize = true;
            this.labelHPku.Location = new System.Drawing.Point(144, 279);
            this.labelHPku.Name = "labelHPku";
            this.labelHPku.Size = new System.Drawing.Size(41, 13);
            this.labelHPku.TabIndex = 19;
            this.labelHPku.Text = "label14";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(30, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 37);
            this.button2.TabIndex = 18;
            this.button2.Text = "Force Retreat";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 372);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Launch";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(204, 338);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(208, 173);
            this.listBox1.TabIndex = 16;
            // 
            // namamu
            // 
            this.namamu.AutoSize = true;
            this.namamu.Location = new System.Drawing.Point(229, 253);
            this.namamu.Name = "namamu";
            this.namamu.Size = new System.Drawing.Size(41, 13);
            this.namamu.TabIndex = 15;
            this.namamu.Text = "label10";
            // 
            // namaku
            // 
            this.namaku.AutoSize = true;
            this.namaku.Location = new System.Drawing.Point(13, 252);
            this.namaku.Name = "namaku";
            this.namaku.Size = new System.Drawing.Size(35, 13);
            this.namaku.TabIndex = 14;
            this.namaku.Text = "label9";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "MP";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(229, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "HP";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 307);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "MP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "HP";
            // 
            // HPmu
            // 
            this.HPmu.Location = new System.Drawing.Point(262, 275);
            this.HPmu.Name = "HPmu";
            this.HPmu.Size = new System.Drawing.Size(100, 23);
            this.HPmu.TabIndex = 9;
            // 
            // MPmu
            // 
            this.MPmu.Location = new System.Drawing.Point(262, 304);
            this.MPmu.Name = "MPmu";
            this.MPmu.Size = new System.Drawing.Size(100, 23);
            this.MPmu.TabIndex = 8;
            // 
            // HPku
            // 
            this.HPku.Location = new System.Drawing.Point(38, 274);
            this.HPku.Name = "HPku";
            this.HPku.Size = new System.Drawing.Size(100, 23);
            this.HPku.TabIndex = 7;
            // 
            // MPku
            // 
            this.MPku.Location = new System.Drawing.Point(38, 303);
            this.MPku.Name = "MPku";
            this.MPku.Size = new System.Drawing.Size(100, 23);
            this.MPku.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 338);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Action :";
            // 
            // pilihAction
            // 
            this.pilihAction.FormattingEnabled = true;
            this.pilihAction.Location = new System.Drawing.Point(52, 335);
            this.pilihAction.Name = "pilihAction";
            this.pilihAction.Size = new System.Drawing.Size(121, 21);
            this.pilihAction.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Arena";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "World Map";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(215, 38);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(197, 211);
            this.richTextBox3.TabIndex = 2;
            this.richTextBox3.Text = "";
            this.richTextBox3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBox3_KeyUp);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(6, 38);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(190, 211);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.richTextBox1_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gold: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buyPet);
            this.groupBox2.Controls.Add(this.beliPet);
            this.groupBox2.Location = new System.Drawing.Point(8, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(174, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buy Pet";
            // 
            // buyPet
            // 
            this.buyPet.Location = new System.Drawing.Point(42, 61);
            this.buyPet.Name = "buyPet";
            this.buyPet.Size = new System.Drawing.Size(75, 23);
            this.buyPet.TabIndex = 4;
            this.buyPet.Text = "Buy";
            this.buyPet.UseVisualStyleBackColor = true;
            this.buyPet.Click += new System.EventHandler(this.buyPet_Click);
            // 
            // beliPet
            // 
            this.beliPet.FormattingEnabled = true;
            this.beliPet.Location = new System.Drawing.Point(22, 34);
            this.beliPet.Name = "beliPet";
            this.beliPet.Size = new System.Drawing.Size(121, 21);
            this.beliPet.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.citGold);
            this.groupBox3.Controls.Add(this.citLevel);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.comboBox4);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Location = new System.Drawing.Point(221, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 162);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cheat";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(58, 118);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 15;
            this.button6.Text = "Cheat";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Cheat);
            // 
            // citGold
            // 
            this.citGold.Location = new System.Drawing.Point(58, 21);
            this.citGold.Name = "citGold";
            this.citGold.Size = new System.Drawing.Size(121, 20);
            this.citGold.TabIndex = 14;
            // 
            // citLevel
            // 
            this.citLevel.Location = new System.Drawing.Point(58, 82);
            this.citLevel.Name = "citLevel";
            this.citLevel.Size = new System.Drawing.Size(121, 20);
            this.citLevel.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Level";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(58, 47);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Pet";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(24, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Gold: ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rangkaiStatsDisini);
            this.groupBox4.Controls.Add(this.cetak);
            this.groupBox4.Controls.Add(this.mine);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Location = new System.Drawing.Point(8, 131);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 196);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Detail";
            // 
            // rangkaiStatsDisini
            // 
            this.rangkaiStatsDisini.AutoSize = true;
            this.rangkaiStatsDisini.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rangkaiStatsDisini.Location = new System.Drawing.Point(19, 88);
            this.rangkaiStatsDisini.Name = "rangkaiStatsDisini";
            this.rangkaiStatsDisini.Size = new System.Drawing.Size(60, 20);
            this.rangkaiStatsDisini.TabIndex = 7;
            this.rangkaiStatsDisini.Text = "label10";
            this.rangkaiStatsDisini.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cetak
            // 
            this.cetak.Location = new System.Drawing.Point(53, 50);
            this.cetak.Name = "cetak";
            this.cetak.Size = new System.Drawing.Size(75, 23);
            this.cetak.TabIndex = 6;
            this.cetak.Text = "Show Stats";
            this.cetak.UseVisualStyleBackColor = true;
            this.cetak.Click += new System.EventHandler(this.cetak_Click);
            // 
            // mine
            // 
            this.mine.FormattingEnabled = true;
            this.mine.Location = new System.Drawing.Point(53, 23);
            this.mine.Name = "mine";
            this.mine.Size = new System.Drawing.Size(121, 21);
            this.mine.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Pet";
            // 
            // labelGapenting
            // 
            this.labelGapenting.AutoSize = true;
            this.labelGapenting.Location = new System.Drawing.Point(11, 330);
            this.labelGapenting.Name = "labelGapenting";
            this.labelGapenting.Size = new System.Drawing.Size(39, 13);
            this.labelGapenting.TabIndex = 8;
            this.labelGapenting.Text = "History";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.comboBox2);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Location = new System.Drawing.Point(221, 210);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 117);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Battle Preparation";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(58, 58);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 10;
            this.button5.Text = "Go Battle!";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(58, 31);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Pet";
            // 
            // history
            // 
            this.history.FormattingEnabled = true;
            this.history.Location = new System.Drawing.Point(7, 344);
            this.history.Name = "history";
            this.history.ScrollAlwaysVisible = true;
            this.history.Size = new System.Drawing.Size(416, 160);
            this.history.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.labelGapenting);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.history);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Location = new System.Drawing.Point(472, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 519);
            this.panel1.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 550);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label namamu;
        private System.Windows.Forms.Label namaku;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar HPmu;
        private System.Windows.Forms.ProgressBar MPmu;
        private System.Windows.Forms.ProgressBar HPku;
        private System.Windows.Forms.ProgressBar MPku;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox pilihAction;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buyPet;
        private System.Windows.Forms.ComboBox beliPet;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label rangkaiStatsDisini;
        private System.Windows.Forms.Button cetak;
        private System.Windows.Forms.ComboBox mine;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelGapenting;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox citGold;
        private System.Windows.Forms.TextBox citLevel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ListBox history;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelMPmu;
        private System.Windows.Forms.Label labelHPmu;
        private System.Windows.Forms.Label labelMPku;
        private System.Windows.Forms.Label labelHPku;
    }
}

