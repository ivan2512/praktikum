﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PerpustakaanKelas;

namespace PetWars {
    public partial class Form1 : Form {
        int Uang, ctrGerak = 0; Random rand;
        List<Pet> dataShop, myPets;
        Musuh musuhKu;
        Pet inbattle;
        string[,] wm, arena;
        public Form1() {
            InitializeComponent();
            rand = new Random();
        }
        private void Form1_Load(object sender, EventArgs e) {
            rangkaiStatsDisini.Text = ""; Uang = 3000;
            dataShop = new List<Pet>(); myPets = new List<Pet>();
            for (int i =0; i<5; ++i) {
                dataShop.Add(new Pet(i));
            }
            wm = init(wm); arena = init(arena);
            beliPet.DataSource = dataShop;
            Perbaharui();
            history.ResetText();
            groupBox1.Enabled = false;
        }
        string[,] init(string [,] arr) {
            arr = new string[10, 10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    arr[i, j] = "X";
                }
            }
            return arr;
        }
        private void Perbaharui() {
            label1.Text = "Gold:" + Uang;
            mine.DataSource = null;
            mine.DataSource = myPets;
            comboBox4.DataSource = null;
            comboBox4.DataSource = myPets.ToArray();
            comboBox2.DataSource = null;
            comboBox2.DataSource = myPets.ToArray();
        }
        private void button5_Click(object sender, EventArgs e) {
            wm = init(wm);
            if (comboBox2.SelectedIndex > -1) {
                listBox1.Items.Clear();
                inbattle = myPets[comboBox2.SelectedIndex];
                richTextBox1.Enabled = true; richTextBox3.Enabled = false;
                pilihAction.Enabled = false; button1.Enabled = false;
                toggle();
                namaku.Text = inbattle.ToString();
                HPku.Maximum = inbattle.HP;
                MPku.Maximum = inbattle.MP;
                HPku.Value = inbattle.HP;
                MPku.Value = inbattle.MP;
                pilihAction.DataSource = null;
                pilihAction.DataSource = inbattle.Actions;
                richTextBox1.Text = "";
                wm[rand.Next(10), rand.Next(10)] = "P";
                int musuhCount = rand.Next(3)+1;
                labelHPku.Text = HPku.Value + " / " + HPku.Maximum;
                labelMPku.Text = MPku.Value + " / " + MPku.Maximum;
                do {
                    int tX = rand.Next(10), tY=rand.Next(10);
                    if (wm[tX, tY] != "P" && wm[tX,tY]!="M") {
                        musuhCount--;
                        wm[tX, tY] = "M";
                    }
                } while (musuhCount > 0);
                for (int i = 0; i < 10; i++) {
                    for (int j = 0; j < 10; j++) {
                        richTextBox1.Text = richTextBox1.Text + wm[j, i];
                    }
                    richTextBox1.Text = richTextBox1.Text + "\r\n";
                }
            }
        }
        private void richTextBox3_KeyUp(object sender, KeyEventArgs e) {
            int x = -1, y = -1, xBaru, yBaru;
            x = inbattle.PosX; y = inbattle.PosY;
            xBaru = x; yBaru = y;
            if (ctrGerak > 0) {
                if (e.KeyCode == Keys.W) {
                    yBaru = y - 1;
                }
                else if (e.KeyCode == Keys.A) {
                    xBaru = x - 1;
                }
                else if (e.KeyCode == Keys.S) {
                    yBaru = y + 1;
                }
                else if (e.KeyCode == Keys.D) {
                    xBaru = x + 1;
                }
                if (xBaru < 10 && yBaru < 10 && xBaru >= 0 && yBaru >= 0) {
                    if (arena[xBaru, yBaru] != "X") { }
                    else {
                        arena[xBaru, yBaru] = arena[x, y];
                        arena[x, y] = "X";
                        inbattle.PosX = xBaru; inbattle.PosY = yBaru;
                        ctrGerak--;
                        listBox1.Items.Add(inbattle.Nama + " has moved");
                        refreshArena();
                    }
                }
            }
            if (ctrGerak == 0)
            {
                gerakMusuk();
                refreshArena();
            }
        }

        private void gerakMusuk(int x=0, int y=0)
        {
            rand = new Random();
            int gerak = rand.Next(4);
            x = musuhKu.PosX; y = musuhKu.PosY;
            bool bisa_att = false;
            if (x == inbattle.PosX) {
                if (y - 1 == inbattle.PosY || y + 1 == inbattle.PosY)
                    bisa_att = true;
            } else if (y == inbattle.PosY) {
                if (x - 1 == inbattle.PosX || x + 1 == inbattle.PosX)
                    bisa_att = true;
            }
            if (!bisa_att) {
                if (gerak == 0) --x;
                else if (gerak == 1) ++x;
                else if (gerak == 2) --y;
                else if (gerak == 3) ++y;
                if (0 <= x && x < 10 && 0 <= y && y < 10 && (x != musuhKu.PosX || y != musuhKu.PosY)) {
                    if (arena[x, y] == "X") {
                        string t = arena[x, y];
                        arena[x, y] = arena[musuhKu.PosX, musuhKu.PosY];
                        arena[musuhKu.PosX, musuhKu.PosY] = t;
                        musuhKu.PosX = x; musuhKu.PosY = y;
                        listBox1.Items.Add(musuhKu.Nama + " has moved");
                    }
                }
            }
            else  {
                double damage = 0, block;
                rand = new Random();
                block = (rand.Next(45, 51) * inbattle.Defense / 100);
                damage = musuhKu.Attack - block;
                int HPmusuh = HPku.Value;
                if (HPmusuh < damage) HPmusuh = 0;
                else HPmusuh -= (int)Math.Round(damage);
                HPku.Value = HPmusuh;
                listBox1.Items.Add(musuhKu.Nama + " attack " + inbattle.Nama + " and ");
                listBox1.Items.Add(inbattle.Nama + " block " + block + "(Total damage: " + damage + ")");
                refreshArena();
            }
        }

        private void Cheat(object sender, EventArgs e) {
            int parseUang, parseLevel, i, selisih;
            string namaLama, mid;
            i = comboBox4.SelectedIndex;
            if (i > -1) {
                if (int.TryParse(citLevel.Text, out parseLevel) && int.TryParse(citGold.Text, out parseUang)) {
                    Uang += parseUang;
                    selisih = parseLevel - myPets[i].Level;
                    namaLama = myPets[i].Nama;
                    myPets[i].Experience = (parseLevel * 100) - (myPets[i].Level * 100);
                    Perbaharui();
                    history.Items.Insert(0, "Success cheat! Gold +" + parseUang);
                    if (namaLama != myPets[i].Nama){
                        if (namaLama == "Mildeep" && myPets[i].Nama!="Unicorn") mid = "Unicorn";
                        else if (namaLama == "Feilla" && myPets[i].Nama != "Fignar") mid = "Fignar";
                        else if (namaLama == "Cleo" && myPets[i].Nama != "Cloudar") mid = "Cloudar";
                        else if (namaLama == "Snaka" && myPets[i].Nama != "Drake") mid = "Drake";
                        else {
                            mid = namaLama;
                        }
                        if (mid!=namaLama) history.Items.Insert(0, namaLama + " has evolved to " + mid);
                        history.Items.Insert(0, mid + " has evolved to " + myPets[i].Nama);
                    }
                    history.Items.Insert(0, "Success cheat! Level " + myPets[i].Nama + " sekarang:" + myPets[i].Level);
                } else {
                    MessageBox.Show("Gagal cheat");
                }
            }
        }
        private void richTextBox1_KeyUp(object sender, KeyEventArgs e) {
            int x = -1, y = -1, xBaru, yBaru;
            findPlayer(ref x, ref y);
            xBaru = x; yBaru = y;
            if (e.KeyCode == Keys.W) {
                yBaru = y - 1;
            } else if (e.KeyCode == Keys.A) {
                xBaru = x - 1;
            } else if (e.KeyCode == Keys.S) {
                yBaru = y + 1;
            } else if (e.KeyCode == Keys.D) {
                xBaru = x + 1;
            }
            if (xBaru < 10 && yBaru < 10 && xBaru >= 0 && yBaru >= 0) {
                if (wm[xBaru, yBaru] == "X") {
                    wm[xBaru, yBaru] = "P";
                    wm[x, y] = "X";
                    richTextBox1.Text = "";
                    for (int i = 0; i < 10; i++) {
                        for (int j = 0; j < 10; j++) {
                            richTextBox1.Text = richTextBox1.Text + wm[j, i];
                        }
                        richTextBox1.Text = richTextBox1.Text + "\r\n";
                    }
                }
                else if (wm[xBaru, yBaru] == "M") { 
                    MPmu.Minimum = 0; HPmu.Minimum = 0;
                    MPmu.Value = 0; HPmu.Value = 0;
                    musuhKu = new Musuh(inbattle);
                    namamu.Text = musuhKu.Nama;
                    HPmu.Maximum = musuhKu.HP;
                    HPmu.Value = musuhKu.HP;
                    MPmu.Maximum = musuhKu.MP;
                    MPmu.Value = musuhKu.MP;
                    labelHPmu.Text = HPmu.Value + " / " + HPmu.Maximum;
                    labelMPmu.Text = MPmu.Value + " / " + MPmu.Maximum;
                    pindahKeArena();
                    pilihAction.SelectedIndex = -1;
                    pilihAction.Enabled = true;
                    pilihAction.Focus();
                }
            }
        }
        private void cetak_Click(object sender, EventArgs e) {
            int i = mine.SelectedIndex;
            if (i > -1) {
                rangkaiStatsDisini.Text = myPets[i].cetak();
            }
        }
        private void button1_Click(object sender, EventArgs e) {
            int i = pilihAction.SelectedIndex;
            if (i > -1) {
                if (pilihAction.Items[i].ToString() == "Retreat") {
                    richTextBox1.Enabled = true; pilihAction.Enabled = false;
                    richTextBox3.Enabled = false; button1.Enabled = false;
                    listBox1.Items.Add(inbattle.Nama + " retreating");
                } else if (pilihAction.Items[i].ToString() == "Attack") {
                    double damage = 0, block;
                    rand = new Random();
                    block = (rand.Next(45, 51) * musuhKu.Defense / 100);
                    damage = inbattle.Attack - block;
                    int HPmusuh = HPmu.Value;
                    if (HPmusuh < damage) HPmusuh = 0;
                    else HPmusuh -= (int)Math.Round(damage);
                    HPmu.Value = HPmusuh;
                    listBox1.Items.Add(inbattle.Nama + " attack " + musuhKu.Nama + " and ");
                    listBox1.Items.Add(musuhKu.Nama + " block " + block + "(Total damage: " + damage + ")");
                    refreshArena();
                    if (HPmu.Value == 0) {
                        string s = inbattle.Nama;
                        inbattle.Experience += musuhKu.RewardXP;
                        if (inbattle.Nama != s) history.Items.Insert(0, s + " has evolved into " + inbattle.Nama);
                        Uang += musuhKu.RewardGold;
                        history.Items.Insert(0, "Get " + musuhKu.RewardGold + " from battle");
                        listBox1.Items.Add(inbattle.Nama + " get " + musuhKu.RewardXP + " XP");
                        richTextBox3.Enabled = false; richTextBox1.Enabled = true;
                        pilihAction.Enabled = false; button1.Enabled = false;
                        Perbaharui();
                    }
                    else gerakMusuk();
                } else if (pilihAction.Items[i].ToString() == "Move") {
                    ctrGerak = 2;
                    richTextBox3.Focus();
                } else if (pilihAction.Items[i].ToString() == "Heal") {
                    int temp = MPku.Value;
                    temp -= 5;
                    if (temp >= 0) {
                        MPku.Value = temp;
                        refreshArena();
                        HPku.Value = HPku.Maximum;
                        gerakMusuk();
                    } else MessageBox.Show("MP Tidak cukup");
                } else {
                    string s = pilihAction.Items[i].ToString();
                    int temp = MPku.Value;
                    if (s == "World's End") temp -= 20;
                    else if (s == "Magma Burst") temp -= 12;
                    else if (s == "Lightning Blast") temp -= 16;
                    else if (s == "Scratch") temp -= 6;
                    else if (s == "Heaven's Light") temp -= 16;
                    else if (s == "Roar") temp -= 16;
                    else if (s == "Eternal Light") temp -= 28;
                    else if (s == "Thunder Storm") temp -= 12;
                    else if (s == "Spin Scratch") temp -= 16;
                    if (temp >= 0) {
                        double damage = 0, block;
                        rand = new Random();
                        block = (rand.Next(35, 41) * musuhKu.Defense / 100);
                        damage = inbattle.Magic - block;
                        int HPmusuh = HPmu.Value;
                        if (HPmusuh < damage) HPmusuh = 0;
                        else HPmusuh -= (int)Math.Round(damage);
                        HPmu.Value = HPmusuh;
                        listBox1.Items.Add(inbattle.Nama + " use " + pilihAction.Items[i] + " on " + musuhKu.Nama + " and ");
                        listBox1.Items.Add(musuhKu.Nama + " block " + block + "(Total damage: " + damage + ")");
                        MPku.Value = temp;
                        if (HPmu.Value == 0) {
                            string soo = inbattle.Nama;
                            inbattle.Experience += musuhKu.RewardXP;
                            if (inbattle.Nama != s) history.Items.Insert(0, soo + " has evolved into " + inbattle.Nama);
                            Uang += musuhKu.RewardGold;
                            history.Items.Insert(0, "Get " + musuhKu.RewardGold + " from battle");
                            listBox1.Items.Add(inbattle.Nama + " get " + musuhKu.RewardXP + " XP");
                            richTextBox3.Enabled = false;
                            button1.Enabled = false;
                            richTextBox1.Enabled = true;
                            pilihAction.Enabled = false;
                            Perbaharui();
                        } else gerakMusuk();
                    } else MessageBox.Show("MP Tidak Cukup");
                    refreshArena();
                }
            }
        }
        private void pindahKeArena() {
            Random r = new Random(DateTime.Now.Millisecond);
            bool ini; richTextBox3.Enabled = true;
            button1.Enabled = true; richTextBox1.Enabled = false;
            inbattle.PosX = r.Next(10); inbattle.PosY = r.Next(10);
            do {
                musuhKu.PosX = r.Next(10);
                musuhKu.PosY = r.Next(10);
                ini = (musuhKu.PosX == inbattle.PosX) && (musuhKu.PosY == inbattle.PosY);
            } while (ini);
            arena = init(arena);
            if (inbattle.Nama.Substring(0, 1) == musuhKu.Nama.Substring(0, 1)) {
                arena[inbattle.PosX, inbattle.PosY] = inbattle.Nama.Substring(0, 1)+"P";
                arena[musuhKu.PosX, musuhKu.PosY] = musuhKu.Nama.Substring(0, 1)+"M";
            } else {
                arena[inbattle.PosX, inbattle.PosY] = inbattle.Nama.Substring(0, 1);
                arena[musuhKu.PosX, musuhKu.PosY] = musuhKu.Nama.Substring(0, 1);
            }
            refreshArena();
        }
        void refreshArena() {
            richTextBox3.Text = "";
            for (int y = 0; y < 10; y++) {
                for (int x = 0; x < 10; x++) {
                    richTextBox3.Text =richTextBox3.Text+ arena[x, y];
                }
                richTextBox3.Text = richTextBox3.Text + "\r\n";
            }
            labelHPku.Text = HPku.Value + " / " + HPku.Maximum;
            labelMPku.Text = MPku.Value + " / " + MPku.Maximum;
            labelHPmu.Text = HPmu.Value + " / " + HPmu.Maximum;
            labelMPmu.Text = MPmu.Value + " / " + MPmu.Maximum;
        }
        private void button2_Click(object sender, EventArgs e) {
            toggle();
            listBox1.Items.Add("Player retreat from Battle");
        }
        private void buyPet_Click(object sender, EventArgs e) {
            int i = beliPet.SelectedIndex;
            if (i >= 0) {
                if (Uang >= dataShop[i].Price) {
                    myPets.Add(dataShop[i]);
                    Uang -= dataShop[i].Price;
                    Perbaharui();
                    mine.SelectedIndex = myPets.Count - 1;
                    history.Items.Insert(0, "Success Buy " + myPets[myPets.Count - 1].ToString());
                    dataShop.RemoveAt(i);
                    beliPet.DataSource = null;
                    beliPet.DataSource = dataShop;
                } else {
                    MessageBox.Show("Uang tidak cukup");
                }
            }
        }
        void toggle() {
            groupBox1.Enabled = !groupBox1.Enabled;
            panel1.Enabled = !panel1.Enabled;
        }
        void findPlayer(ref int x, ref int y) {
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    if (wm[j, i] == "P") {
                        x = j; y = i;
                        break;
                    }
                }
            }
        }
    }
}
